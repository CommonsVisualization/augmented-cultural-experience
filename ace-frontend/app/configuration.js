var locProtocol = location.protocol;
var backendPort = location.protocol==="https:"?'8080':'8079';
window.restServer = location.protocol+'//' + window.location.hostname + ':' + backendPort;

window.configuration = {
    'mediaServer' : '/audio/',
    'restSearchService' : restServer + '/features/search',
    'restNearService' : restServer + '/features/near',
    'restBBoxService' : restServer + '/features/bbox',
    'restWikipediaService' : restServer + '/features/wikipedia',

    'restAuthService' : restServer + '/auth',
    'restUserService' : restServer + '/user',
    'restIdentityService' : restServer + '/identity',
    'restImage' : restServer + '/img',

    'restItineraryService' : restServer + '/itineraries/{code}',


};
