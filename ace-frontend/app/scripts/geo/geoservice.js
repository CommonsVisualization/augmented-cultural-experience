function Coordinates(latitude, longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
}

function Position(coords, date) {
    this.coords = coords;
    this.date = (date == undefined) ? new Date() : date;
}


var geolocationProviders = {
    
    "W3C" : {
        _watchId : -1,
        
        watchPosition : function(successCallback, errorCallback, options) {
            this.clearWatch();
            this._watchId = navigator.geolocation.watchPosition(
                successCallback, errorCallback, options);
        },
        clearWatch : function() {
            if (this._watchId != -1) {
                navigator.geolocation.clearWatch(this._watchId);
                this._watchId = -1;
            }
        }
    },
    
    "SIMULATOR" : {
        _TIMEOUT  : 1*1000,
        _watchId  : -1,
        _counter  : 0,
        _delta    : 0,
        _currentCallback : function() {},
        
        _updatePosition : function(self) {
            var lat = seeds[this._counter][1];                
            var lon = seeds[this._counter][0];
            var coords = {"latitude"  : lat, 
                          "longitude" : lon,
                          "accuracy"  : parseInt(Math.random()*5+5)};
            var position = {"coords"    : coords, 
                            "timestamp" : new Date().getTime()}
            self._currentCallback(position);
            if (((self._delta == -1) && (self._counter == 0)) || 
                ((self._delta == +1) && (self._counter == seeds.length-1))){
                self._delta = self._delta * -1;
            }
            self._counter = self._counter + self._delta;
            self._watchId = setTimeout(
                function(self) {
                    self._updatePosition(self);
                }, self._TIMEOUT, self);
        },
        watchPosition : function(successCallback, errorCallback, options) {
            if (false || Math.random() > 0.9) {
                errorCallback({"code" : 1});
            } else {
                this._delta = +1;
                this._currentCallback = successCallback;
                this._watchId = setTimeout(
                    function(self) {
                        self._updatePosition(self)
                    }, this._TIMEOUT, this);
            }
        },
        clearWatch : function() {
            if (this._watchId != -1) {
                clearTimeout(this._watchId);
                this._watchId = -1;
            }
        }
    }
}

var geolocationService = {
     PROVIDER_NAME : "SIMULATOR",
     LECTURE_IS_OBSOLETE_TIME : 10 * 1000,
     
     options : {enableHighAccuracy : true, 
                maximumAge : 10*000, 
                timeout : 60*1000},
     
     log : function(position) {
         console.log(
                  " tim: " + ((position.timestamp % 10000) / 1000)
                + " lat: " + Math.round(position.coords.latitude* 1E5) / 1E5
                + " lon: " + Math.round(position.coords.longitude* 1E5) / 1E5
                + " acc: " + position.coords.accuracy
                + " bea: " + (position.bearing ? Math.round(position.bearing) : "UNK")
                + " dis: " + (position.distance ? Math.round(position.distance* 1E4) / 1E4 : "UNK")
                + " hea: " + Math.round(position.coords.heading));
         
     },
     
     processPosition : function(position) {
         position.latLon = LatLon.fromGPSPosition(position);
         var oldLectureLapse = this.lastPosition ?
                              position.timestamp - this.lastPosition.timestamp :
                              this.LECTURE_IS_OBSOLETE_TIME;
         if (oldLectureLapse < this.LECTURE_IS_OBSOLETE_TIME) {
            var lastLatLon = LatLon.fromGPSPosition(this.lastPosition);
            position.bearing = position.latLon.bearingTo(lastLatLon);
            position.distance = parseFloat(position.latLon.distanceTo(lastLatLon, 8))
         } 
         this.lastPosition = position;
         return (position.distance == undefined) || 
                ((position.distance > 0.001) && (position.coords.accuracy < 10));
     },
     
     watchPosition : function() {
         var self = this;

         this.clearWatch();

         this.currentWatchId = geolocationProviders[this.PROVIDER_NAME].watchPosition(
            function(position) {
                if (self.processPosition(position) == true) {
                    self.fireNewPositionEvent(new self.NewPositionEvent(position));
                }
            }, 
            function(errorCode) {
                self.fireErrorEvent(new self.ErrorEvent(errorCode));
            },
            this.options);
     },            
     clearWatch : function() {
         geolocationProviders[this.PROVIDER_NAME].clearWatch();
     },
     isGeolocationActive : function() {
         return geolocationProviders[this.PROVIDER_NAME]._watchId != -1;
     },
     
     NewPositionEvent : function(position) {
         this.position = position;
     },
     
     ErrorEvent : function(errorCode) {
         this.errorCode = errorCode;
     }
}
window.applyListenerDecorator("newPosition", geolocationService);
window.applyListenerDecorator("error", geolocationService);
