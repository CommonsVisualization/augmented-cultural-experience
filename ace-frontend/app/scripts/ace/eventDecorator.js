
(function($){
    $.fn.addEventSupport = function(eventName)  {
        for (var idx=0; idx < this.length; idx++) {
            var currentObject = this[idx];
            if (currentObject.events == undefined) {
                currentObject.events = {};
            }
            currentObject.events[eventName] = {};
            var listeners = currentObject.events[eventName];

            var eventNameSuffix = eventName.charAt(0).toUpperCase() + eventName.substring(1);
            currentObject["add" + eventNameSuffix + "Listener"] = function(listener, key) {
                if (key == undefined) {
                    key = listener;
                }
                if (listeners[key] != undefined) {
                    throw "assert: listener already added.";
                }
                listeners[key] = listener;
            }

            currentObject["remove" + eventNameSuffix + "Listener"] = function(listenerOrKey) {
                var key = null;
                for (var currentListenerKey in listeners) {
                    if ((listenerOrKey == currentListenerKey)
                        || (listeners[currentListenerKey] == listenerOrKey)) {
                        key = currentListenerKey;
                        break;
                    }
                }
                if (key == null) {
                    throw "assert: Removing unknown listener (" + listenerOrKey + ").";
                }
                delete listeners[key];
            }
            
            currentObject["has" + eventNameSuffix + "Listener"] = function(listenerOrKey) {
                var key = null;
                for (var currentListenerKey in listeners) {
                    if ((listenerOrKey == currentListenerKey)
                        || (listeners[currentListenerKey] == listenerOrKey)) {
                        key = currentListenerKey;
                        break;
                    }
                }
                return key != null;
            }
            
            

            currentObject["fire" + eventNameSuffix + "Event"] = function(evt) {
                for (var currentListenerKey in listeners) {
                    listeners[currentListenerKey](evt);
                }
            }
        }
    }

})(jQuery);

