function ItineraryService() {
}


ItineraryService._ITINERARY_MODIFIED_EVENT_NAME = "onChangeFeatures";
ItineraryService._FEATURE_ACTIVATED_EVENT_NAME = "onActivatedFeature";

ItineraryService._WEB_SERVICE_URL =  window.configuration.restItineraryService;
// ItineraryService._USER_SERVICE_URL =  configuration.restUserService;

ItineraryService.prototype._itinerary = null;
ItineraryService.prototype._currentFeatureIndex = 0;

// Call the backend to request itinerary info.
ItineraryService.prototype._invokeWebService = function(itineraryCode, fn) {
    var self = this;
    var url = ItineraryService._WEB_SERVICE_URL.replace("{code}", itineraryCode)+"?locale="+getCurrentLocale();

    return $.ajax({
              url: url,
              type: "GET",
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              headers: createAuthorizationTokenHeader(),
              success: function (itinerary) {
                  // ADAPTATION: Handle the route for default itinerary
                  if (itinerary.code == "itiaud"){
                    itinerary.pathRouteString = "41.362081943230116 2.1575736451123255, 41.36205220066788 2.157652250455374, 41.361994840012144 2.157669246205222, 41.361894989981785 2.157662872799029, 41.36184400273224 2.157274095021251, 41.361695289921066 2.1569787938676384, 41.36164855160899 2.1568895661809355, 41.36161243564056 2.156698363995143, 41.36149983879781 2.1566367544019434, 41.36135749939283 2.1565963894960536, 41.36128314298725 2.1564774192471163, 41.361206662112934 2.156428556466303, 41.361074945051605 2.1565050373406196, 41.36096022374013 2.1564859171220405, 41.36083700455373 2.1564922905282335, 41.36065854918033 2.1564922905282335, 41.36046734699454 2.1565071618093508, 41.360416359744995 2.156513535215544, 41.36032500758956 2.156602762902247, 41.360318634183365 2.1566452522768675, 41.360441853369764 2.15671323527626, 41.36051408530662 2.156766346994536, 41.36057781936855 2.156872570431087, 41.3607116608986 2.1568874417122044, 41.360771146023076 2.1568768193685495, 41.360873120522164 2.1568428278688527, 41.36076477261688 2.1566579990892536, 41.360673420461445 2.156530530965392, 41.36070316302368 2.156519908621737, 41.360773270491805 2.1565623979963573, 41.360985717364905 2.156674994839102, 41.361062198239225 2.156772720400729, 41.361261898299944 2.1568152097753495, 41.36143185579842 2.1570531502732244, 41.3615805686096 2.1573038375834854, 41.36165067607772 2.157567271706133, 41.36175477504554 2.157633130236795, 41.36178876654524 2.1578137100789316, 41.36184187826351 2.158077144201579, 41.36189074104433 2.1583681964177295, 41.36188011870067 2.158582767759563, 41.361729281420764 2.1584574241044328, 41.36172078354584 2.1585891411657565, 41.36171865907711 2.158635879477839, 41.36176964632666 2.158801588038859, 41.36175477504554 2.1588716955069827, 41.36183125591986 2.1590628976927753, 41.36205432513661 2.1590990136612027, 41.362050076199154 2.1590628976927753, 41.36204157832423 2.1590097859744994, 41.36208406769885 2.1590841423800855, 41.36214355282332 2.159220108378871, 41.362213660291445 2.159324207346691, 41.362283767759564 2.159489915907711, 41.36233900394657 2.159521782938677, 41.36250683697632 2.1595748946569526, 41.36254295294475 2.159517534001215, 41.36282338281725 2.1593943148148154, 41.362925357316335 2.1593433275652707, 41.362946602003646 2.1592349796599883, 41.36300608712811 2.1591032625986646, 41.3630528254402 2.158999163630844, 41.362991215846996 2.1589375540376445, 41.36276602216151 2.1590990136612027, 41.36248771675774 2.1591032625986646, 41.362392115664846 2.1589290561627204, 41.36237936885246 2.158888691256831, 41.36239848907104 2.1588483263509413, 41.36240911141469 2.1588313306010933, 41.36248346782028 2.1588610731633278, 41.36258544231937 2.1588589486945966, 41.36270016363085 2.1587994635701278, 41.36268741681846 2.158731480570735, 41.362649176381304 2.1586528752276872, 41.36256207316333 2.158567896478446, 41.3624983391014 2.158497789010322, 41.36250046357013 2.1585360294474807, 41.36234537735277 2.158538153916212, 41.36232413266546 2.158480793260474, 41.362281643290835 2.1583427027929574, 41.36223278051002 2.1582173591378266, 41.36216692197936 2.1580261569520345, 41.3621286815422 2.157839203703704";
                  }
                  // Check if the request has also the features
                  if (itinerary.features) {
                    self._normalizeFeatures(itinerary.features);
                  }
                  if (fn) fn(itinerary); // Callback

              },
              error: function (jqXHR, textStatus, errorThrown) {
                  defaultAjaxError (jqXHR, textStatus, errorThrown);
              }
          });

};

// Get itinerary info by code
ItineraryService.prototype.getItineraryByName = function(itineraryCode, fn) {
    var self = this;
    this._invokeWebService(itineraryCode, function(itinerary) {
        if (fn) fn(itinerary);
    });
};

// Load an itinerary by code and call the function to activate it on the map
ItineraryService.prototype.loadItinerary = function(itineraryCode, fn) {
    var self = this;
    this._invokeWebService(itineraryCode, function(itinerary) {
        // Set the active itinerary
        self.setActiveItinerary(itinerary);
        if (fn) fn(itinerary);
    });
};

ItineraryService.prototype.setActiveFeatureByIndex = function(featureIndex) {
    this._currentFeatureIndex = featureIndex;
    $(this).trigger(ItineraryService._FEATURE_ACTIVATED_EVENT_NAME);
};

ItineraryService.prototype.setActiveFeature = function(feature) {
    var idx;
    for (idx=0; idx < this._itinerary.features.length; idx++) {
        if (this._itinerary.features[idx] == feature) {
            break;
        }
    }
    this.setActiveFeatureByIndex(idx);
};

ItineraryService.prototype.getItineraryFeatures = function() {
    return this._itinerary.features;
};

ItineraryService.prototype.getActiveFeature = function() {
    return (this._itinerary) ? this._itinerary.features[this._currentFeatureIndex] : null;
};

ItineraryService.prototype.getActiveFeatureOrdinal = function() {
    return this._currentFeatureIndex;
};

ItineraryService.prototype.getActiveItinerary = function() {
    return this._itinerary;
};

// Set the active itinerary and trigger the action to activate it
ItineraryService.prototype.setActiveItinerary = function(itinerary) {
    this._itinerary = itinerary;
    $(this).trigger(ItineraryService._ITINERARY_MODIFIED_EVENT_NAME);
    this.setActiveFeatureByIndex(0);
};

// Erase the itinerary info stored
ItineraryService.prototype.clearItinerary = function() {
    this._itinerary = null;
    $(this).trigger(ItineraryService._ITINERARY_MODIFIED_EVENT_NAME);
};

ItineraryService.prototype._normalizeFeatures = function(features) {
    var orderedFeatures = [];
    for (var idx=0; idx < features.length; idx++) {
        var feature = features[idx];
        feature.label = feature.name;
        feature.audioResource = feature.audioFile;
        feature.detailsAvalaible = true;
        orderedFeatures[feature.featureOrder] = feature;
    }
    return orderedFeatures;
};

ItineraryService.prototype.createItinerary = function (itinerary){

  var url = ItineraryService._WEB_SERVICE_URL.replace("{code}", '');

  return $.ajax({
            url: url,
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(itinerary),
            headers: createAuthorizationTokenHeader(),
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
  });

}

ItineraryService.prototype.getUserItierary = function (username){

  var url = ItineraryService._WEB_SERVICE_URL.replace("{code}", 'user/'+username);

  return $.ajax({
            url: url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
  });

}

ItineraryService.prototype.getPublicItieraries = function (){

  var url = ItineraryService._WEB_SERVICE_URL.replace("{code}", 'public/');

  return $.ajax({
            url: url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
  });

}

ItineraryService.prototype.deleteItinerary = function (itineraryId){

  var url = ItineraryService._WEB_SERVICE_URL.replace("{code}", itineraryId);

  return $.ajax({
            url: url,
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
  });

}
