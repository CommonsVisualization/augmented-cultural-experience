var $scripts = $("script");
var scriptFolder = null;
for (var idx=0; idx < $scripts.length; idx++) {
    var position = $scripts[idx].src.indexOf("compass.js");
    if (position != -1) {
        scriptFolder = $scripts[idx].src.substring(0, position);
    }
}


function Compass(imageElement, distanceElement, destinationLatLon, currentLatLon) {
    this.destinationLatLon = destinationLatLon;
    this.imageElement = imageElement;
    this.imageElement.src = scriptFolder + "/images/50px-High-contrast-go-up.svg.png";
    this.distanceElement = distanceElement;
    this.bearing = 0;
    this.distance = 0;
    this.updateDirection(currentLatLon);
} 

Compass.prototype.updateDirection = function(newLatLon) {
        this.bearing = (180 + this.destinationLatLon.bearingTo(newLatLon)) % 360;
        this.distance = this.destinationLatLon.distanceTo(newLatLon);
        $compassImg = $(this.imageElement);
        if (this.distance < 0.008) {
            $compassImg.attr("src", scriptFolder + "/images/50px-High-contrast-view-restore.svg.png");
            $compassImg.css('transform', 'rotate(0deg)');
        } else {
            $compassImg.attr("src", scriptFolder + "/images/50px-High-contrast-go-up.svg.png");
            $compassImg.css('transform', 'rotate('+this.bearing+'deg)');
        }

        $(this.distanceElement).text(Math.round(this.distance*1000)+ "m");
}

