/** Loading page berfore map **/
var LoadingControl = L.Control.extend({
    _$container : null,

    options: {
            position: "bottomleft",
            text    : "",
            localeKey: "" // Used for translation
    },

    onAdd: function (map) {
            this._map = map;
            var container = this._createComponents();
            return container;
    },


    _createComponents: function () {
            var self = this;
            this._$container = $("<div/>")
                               .css("opacity", "0.8")
                               .text(this.options.text)
                               .attr('data-i18n', this.options.localeKey)
                               .hide();

            $(document).ajaxStart(function() {
                self._$container.show();
            });
            $(document).ajaxStop(function() {
               self._$container.hide();
            });

            return this._$container[0];
    }


});
