// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
// var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
// btn.onclick = function() {
//     modal.style.display = "block";
// }

var setHeight = function (){

  var body = document.body,
      html = document.documentElement;

  var height = Math.max( body.scrollHeight, body.offsetHeight,
                       html.clientHeight, html.scrollHeight, html.offsetHeight );

  modal.style.height = height;
}

var openModal = function( headerText, bodyText) {
    modal.style.display = "block";
    // Display modal content because modal image hid it
    $("#modalContent").show();
    $('#modalImage').remove();
    $('#caption').remove();

    setHeight();
    
    // Add text to header
    var hText = document.createTextNode(headerText);
    var header = $("#modalHeaderText");

    // var header = document.getElementById("modalHeaderText");
    header.innerHTML = '';
    header.html(headerText);
    // header.appendChild(hText);

    // Add text tobody
    var bText = document.createTextNode(bodyText);
    var body = $("#modalBody");
    // var body = document.getElementsByClassName("modal-body")[0];
    body.innerHTML = '';
    body.html(bodyText);
    // body.appendChild(bText);

}

// Open modal with an ok or cancel buttons
var openConfirmModal = function( headerText, cb) {

  var butOk = $("<div />").addClass("btn").append('<i class="fa fa-check" aria-hidden="true"></i>').click(function (){
      var result = true;
      cb( );
      closeModal();
  });

  var butCancel = $("<div />").addClass("btn").append('<i class="fa fa-times" aria-hidden="true"></i>').click(function (){
      var result = false;
      closeModal();
  });
  var container = $("<div />").append (butOk, butCancel).addClass("btnConfirmContainer");
  openModal ( headerText, container);

}


var openModalImage = function (imageSrc, imageDescription){
    $('#caption').remove();
    $('#modalImage').remove();
    $("#modalContent").hide();
    setHeight();

    modal.style.display = "block";



    var img = $('<img >');
    img.attr('src', imageSrc);
    // img.appendTo(modal);
    var close = $('<span />')
      .click(function () { closeModal() })
      .addClass('closeImage')
      .append("&times;");

    var imgContainer = $('<div />')
      .attr('id', 'modalImage')
      .append(close , img)
      .appendTo(modal);


    if (imageDescription) {
      var imgCaption = $("<div />")
        .attr('id', 'caption')
        .append (imageDescription)
        .appendTo(modal);
    }
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  closeModal ();
}

var closeModal = function (){
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
      closeModal ();
    }
}
