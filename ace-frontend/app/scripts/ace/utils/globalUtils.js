// Handle the default ajax error messages
var defaultAjaxError = function (jqXHR, textStatus, errorThrown){
  if (jqXHR.status === 401 &&
    jqXHR.responseJSON.exception == "org.springframework.security.authentication.BadCredentialsException"){
    showToast("Authentication needed!");
  } else if (jqXHR.status === 401 &&
    jqXHR.responseJSON.exception == "org.springframework.security.core.userdetails.UsernameNotFoundException") {
    showToast("Bad username or password");
  }
  else if (jqXHR.status === 401) {
      openModal ("Spring exception:" , jqXHR.responseJSON.exception);
      throw new Error("Err 401: " + errorThrown);
  } else if (jqXHR.readyState == 0 || jqXHR.readyState == 4) {
    showToast($.i18n("ajax-default-error"));
  } else {
    errorHandler(jqXHR.status + " " + textStatus);
  }
}
// Function that provides default error message
var errorHandler = function (data){
  openModal (
  '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Error! ' ,
  data);
  throw new Error("An unexpected error occured: " , data);

}

var showToast = function (msg) {

    // Get the snackbar DIV
    var x = document.getElementById("snackbar")

    // Add the "show" class to DIV
    x.className = "show";

    x.textContent = msg;

    x.onclick = function() {
      unshow();
    };

    var unshow = function (){
      x.className = x.className.replace("show", "");
    }

    // After 3 seconds, remove the show class from DIV
    setTimeout(unshow, 3000);
}

// Returns date on dd/MM/yyyy
var formatDate = function (date){

  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [ day, month, year ].join('-');
}

// Create a progressbar and append it to element
var appendProgresBar = function (element){
  var progressContainer = $("<div />")
    .attr('id', 'myProgress')
    .appendTo(element);


  var progress = $("<div />")
    .attr('id', 'myBar')
    .appendTo(progressContainer);

}

var progresBarWidth = 0;
var updateProgresBar = function (minVal, maxVal){

  var progress = $("#myBar");
  progresBarWidth = (minVal/maxVal)*100;
  var id = setInterval(frame, 10);

  function frame() {
    if (progresBarWidth > 100) {
      clearInterval(id);
    } else {
      // progresBarWidth++;
      progress.width (progresBarWidth + '%');
      progress.html (progresBarWidth * 1  + '%');
      if (progresBarWidth >= 100) {
        clearInterval(id);

      }

    }
  }
}


// This is used to add into an element this radio butttons to togle between private or public options in a form
var appendPublicPrivateRadioButtons = function (element, cb){

  var name=element.selector.substring(1); //Get the id of the radio buttons based on the container element name

  var rad =
  '<input type="radio" name="public" value="true" id="publicTrue'+name+'" required>'+
  '<label for="publicTrue'+name+'" class="btn"><i class="fa fa-globe" aria-hidden="true"></i></label>'+
  '<input type="radio" name="public" value="false" id="publicFalse'+name+'" >'+
  '<label for="publicFalse'+name+'" class="btn"><i class="fa fa-lock" aria-hidden="true"></i></label>'+
  '<br><div id="radioValue'+name+'"></div>'
  ;



  element.html(rad);

  var $radio = $('[name="public"]').on("change", function(){
    var val = $('[name="public"]:checked').val();
    if (val==="true") {
      $("#radioValue"+name).html("<span data-i18n='label-public'>"+$.i18n("label-public")+"</span>");
    } else {
      $("#radioValue"+name).html("<span data-i18n='label-private'>"+$.i18n("label-private")+"</span>");
    }
    if (cb) {cb (val);}
  });

}


// Used to bind a jQuery object with a dom element
function DataBinder( object_id ) {
  // Use a jQuery object as simple PubSub
  var pubSub = jQuery({});

  // We expect a `data` element specifying the binding
  // in the form: data-bind-<object_id>="<property_name>"
  var data_attr = "bind-" + object_id,
      message = object_id + ":change";

  // Listen to change events on elements with the data-binding attribute and proxy
  // them to the PubSub, so that the change is "broadcasted" to all connected objects
  jQuery( document ).on( "change", "[data-" + data_attr + "]", function( evt ) {
    var $input = jQuery( this );
    pubSub.trigger( message, [ $input.data( data_attr ), $input.val() ] );
  });

  // PubSub propagates changes to all bound elements, setting value of
  // input tags or HTML content of other tags
  pubSub.on( message, function( evt, prop_name, new_val ) {
    jQuery( "[data-" + data_attr + "=" + prop_name + "]" ).each( function() {
      var $bound = jQuery( this );

      if ( $bound.is("input, textarea, select") ) {
        $bound.val( new_val );
      } else {
        $bound.html( new_val );
      }
    });
  });

  return pubSub;
}

// Logic for hide the copyright bar after few seconds of program startup
function hideCopyrightBar (){

  // Get toolbar and copiryght and move it to his position
  var copyright = $('.copyright');
  var toolbar = $('#toolbar');
  copyright.animate({top: '-40px'});
  toolbar.animate({top: '0'});

  // If a page that is not the map is selected, move it also
  var page = $( ".page[selected='true'] " );
  if (page.attr('id') != 'map'){
    page.animate({top: '45px'});
  }

  // And change the css rule for all remaining pages
  $('.page:not(#map)').css('top', '45px');

  // After that, put the map buttons on his position.
  var mapbuttons = $('#map').find('.leaflet-top');
  mapbuttons.css('top', '0');

}

// Randomly generated uid for each feature to bind it to html elements
function makeid()
{
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz";

    for( var i=0; i < 3; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
