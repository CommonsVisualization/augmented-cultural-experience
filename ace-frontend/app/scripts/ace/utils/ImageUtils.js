// This JS is for provide utils for image upload

// Used to transform canvas image to multipart file to send it to the backend
function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}


function resizeImage (cb){

  var $input = $('#imgInput');

  // Show preview and Preprocess the image using canvas
  // var canvas = document.getElementById('canvas');
  var canvas = document.createElement('canvas');
  var ctx = canvas.getContext('2d');
  var img = new Image;
  img.src = URL.createObjectURL($input[0].files[0]);
  // img.src = URL.createObjectURL(e.target.files[0]);
  img.onload = function() {
      ctx.drawImage(img, 0,0);

      // Setting max width and height for the image
      var MAX_WIDTH = 1600;
      var MAX_HEIGHT = 1067;
      var width = img.width;
      var height = img.height;

      if (width > height) {
        if (width > MAX_WIDTH) {
          height *= MAX_WIDTH / width;
          width = MAX_WIDTH;
        }
      } else {
        if (height > MAX_HEIGHT) {
          width *= MAX_HEIGHT / height;
          height = MAX_HEIGHT;
        }
      }

      canvas.width = width;
      canvas.height = height;

      ctx.fillStyle = '#fff';  /// set white fill style because jpg doesn't have transparent layer support
      ctx.fillRect(0, 0, width, height);

      ctx.drawImage(img, 0, 0, width, height);

      var dataUrl = canvas.toDataURL("image/jpeg",0.7);

      cb ( dataUrl );
      // return canvas.toDataURL("image/jpeg",0.7);

    }
}
