// I don't know what its used for
$.fn.animateHighlight = function(times, duration) {
    times = times|| 3;
    duration = duration || 200;
    this.stop(true,true);
    for (var idx=0; idx < times; idx++) {
        this.fadeTo(duration, 0.3).fadeTo(duration, 1.0);
    }
}
