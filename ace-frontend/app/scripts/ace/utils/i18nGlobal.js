var supoortedLanguages = {
  'en' : {
    'path' : 'i18n/en.json',
    'name' : 'English',
    'default' : 'true'
  },
  'ca' : {
    'path' : 'i18n/ca.json',
    'name' : 'Català'
  },
  'es' : {
    'path' : 'i18n/es.json',
    'name' : 'Castellano',
  }
}

var loadedLanguagesCache = {}; //Cache used to not load previously download lang files

var localeButton = {
  initialized : false,

  initialize : function (){
    var localesLenght = 0;
    // Generate a button for each supported lang
    $.each(supoortedLanguages, function (k, v){
      localesLenght ++ ;
      var langElement = '<a href="#" data-locale="'+k+'"><li>'+v.name+'</li></a>';
      $('#switch-locale').append(langElement);
    })

    // Calc the height dinamically when clic
    var defaultHeight = $('#switch-locale').css("height");
    var toggleMenu = function (){
       var container = $("#switch-locale");
        if (!container.hasClass('locale-opened')) {
          deployMenu(container);
        } else {
          hideMenu(container)
        }
     }
     var deployMenu = function (container) {
       container.addClass('locale-opened')
       var increment = 40;  // Lineheight increment
       container.css("height", (increment*localesLenght)+ // Number of languajes
                                localesLenght+ // Add the line width of 1px
                                increment // And add the first line (the icon line)
                    );
     }

     var hideMenu = function (container) {
       container.css ('height' , defaultHeight);
       container.removeClass('locale-opened');
     }

     // Set the click events
     $('.locale-container').on('click', 'a', function(e) {
        e.preventDefault();

        if ( $(this).is(':first-child') ) {
         toggleMenu ();
        } else {

          var newLocale = $(this).data('locale');
          changeLocale (newLocale);
          toggleMenu();
         }
      });

      localeButton.initialized = true;
  }
}



// Initialization function for i18n internationalization
var loadLocales = function (){

  var localeNamePath = {};
  var browserLocaleName = navigator.language;
  browserLocaleName = browserLocaleName.substring(0, browserLocaleName.indexOf("-"));

  changeLocale (browserLocaleName, function (){
     _toolbarUpdate($('#pageTitle')); //Force to change loading bar name if iui loads faster than trasnlations
     i18nIuiIntegration();
  });

}


// Used to find the default locale if no supported locale are select.
// Search on supoortedLanguages the locale marked as default. Return object { 'locale_name' : 'locale_path'}
var getDefaultLocale = function (){
  var defLocale = {};
  $.each(supoortedLanguages, function (k, v){
    if (v.default == 'true'){
      defLocale [k] = v.path
      return false;
    }
  });
  return defLocale;
}

var getLocalWord = function (varName){
  return $.i18n(varName)
}
var getCurrentLocale = function (){
  return $.i18n().locale;
}

var changeLocale = function (lng, cb){

  var set = function (seti18n){
    if (seti18n) $.i18n().locale = seti18n;
    if (!localeButton.initialized) localeButton.initialize();
    if (cb) cb();
    i18nUpdate();
  }

  var localeNamePath = {};
  var i18new = null;

  // If locale is in cache don't load it from server
  if (loadedLanguagesCache.hasOwnProperty(lng)) {
    set (lng);
    return;
  }
  // If actual locale supported
  else if(supoortedLanguages.hasOwnProperty(lng)) {
    localeNamePath [lng] = supoortedLanguages[lng].path;
    i18new = lng;
  }
  // Else get default locale name and path and change it to i18n actual locale
  else {
    localeNamePath = getDefaultLocale();
    i18new = Object.keys(localeNamePath)[0];
  }

  $.i18n().load(
    localeNamePath
  ).done (function(){
    loadedLanguagesCache [lng] = localeNamePath[lng];
    set(i18new);
  });

}

// Global i18n update for all sensible elements
var i18nUpdate = function (){
  i18nElementUpdate();
  i18nDataArgsUpdate();
  infoCardUpdate();
}

// i18n update for single elements
var i18nElementUpdate = function (element) {
  if (!element) element = 'body';
  $(element).i18n();
}

// This piece of code is used for refresh elements that changes his data-args attribute dinamically
// For example the toolbar.
// This solve a bug where dinamic change of the data-i18n attr doesn't reflect changes.
var i18nDataArgsUpdate = function (){

  var getDestinationAttribute = function (argument){
    var end = argument.indexOf("]");
    if (end && end > 0) {
      var attri = argument.substring(1,end);
      return attri;
    } else {
      return argument;
    }
  };

  $('.translate').each(function() {
     var args = [], $this = $(this);
     if ($this.attr("data-args")){
      args = $this.attr("data-args").split(',');
      if (args[0].indexOf("[") == 0 && args[0].indexOf("]") > 0) { // If the translation is oriented to an attribute
        var attribute = getDestinationAttribute (args[0]);
        args[0] = args[0].substring(args[0].indexOf("]")+1);
        $this.attr(attribute, $.i18n.apply(null, args));
      } else {
        $this.html( $.i18n.apply(null, args) );
      }
     }
  })
}

// If info card is displayed try to update the elements
var infoCardUpdate = function (){
  if ($("#infoCard").is(":visible") && globalCardCtrl){
    globalCardCtrl.translate();
  }
}


// Add data-args attribute to an element and then refresh it.
// The element should be of class translate
var i18nAddDataArgsToElement = function (element, localeNameAndArgs ){
  element.empty().attr('data-args', localeNameAndArgs );
  i18nDataArgsUpdate ();
}

// Specific iui integration
// Get the text from the data-title iui attribute to use it to get the translation
var _toolbarUpdate = function (element) {
  // var title = $('#pageTitle');
  var localeName = element.text();
  try{
    if ( localeName.length > 0){
      if ( $.i18n( localeName ) != localeName){
        i18nAddDataArgsToElement(element, localeName);
      }
    }

  } catch (e){
  }
}

// Integration with iui framework
var i18nIuiIntegration = function (){
  //Bind toolbar title changes
  $('#pageTitle').bind("DOMSubtreeModified",function(){
    _toolbarUpdate($('#pageTitle'));
  });
  $('#backButton').bind("DOMSubtreeModified",function(){
    _toolbarUpdate($('#backButton'));
  });
  $('#rightButton').bind("DOMSubtreeModified",function(){
    _toolbarUpdate($('#rightButton'));
  });
}
