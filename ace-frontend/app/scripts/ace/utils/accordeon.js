// logic for accordeos
var generateAccordeon = function (){
  var acc = document.getElementsByClassName("accordion");
  var i;

  var toggleActiveAccordeon = function (element){

    if (element.getElementsByTagName("i")[0].className == "fa fa-chevron-right") {
      element.getElementsByTagName("i")[0].className = "fa fa-chevron-down";
    } else{
       element.getElementsByTagName("i")[0].className = "fa fa-chevron-right";
    }

    var panel = element.nextElementSibling;
    if (panel.style.maxHeight){
      // Used on the started as active accordions
      if ($(element).next().css('overflow') == 'visible') {
        $(element).next().css({
          overflow: 'hidden'
        })
      }
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  }

  for (i = 0; i < acc.length; i++) {

    // Add a chevron arrow if doesn't exists
    if (!$(acc[i]).find(".fa-chevron-right")[0] && !$(acc[i]).find(".fa-chevron-down")[0]){
      $(acc[i]).prepend('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
    }

    acc[i].onclick = function() {
      var e = acc[i];

      this.classList.toggle("active");

      toggleActiveAccordeon(this);
    }

    // Used to start accordion as active
    if ($(acc[i]).hasClass("active")) {
      toggleActiveAccordeon(acc[i]);
      $(acc[i]).next().css({
        overflow: 'visible'
      })
    }
  }
}
