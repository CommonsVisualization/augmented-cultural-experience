function ImageService() {
}

ImageService._IMAGE =  configuration.restImage;


// Upload an image
ImageService.prototype.upload = function (data) {
  var self = this;

  return $.ajax({
            xhr: function() {  // custom xhr
              myXhr = $.ajaxSettings.xhr();
              if(myXhr.upload){ // check if upload property exists
                myXhr.upload.addEventListener('progress',function (evt){
                if(evt.lengthComputable){
                  updateProgresBar( evt.loaded , evt.total)
                  }
               }, false); // for handling the progress of the upload
              }
              return myXhr;
             },
            url: ImageService._IMAGE,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            processData: false,  // Important!
            contentType: false,
            cache: false,
            enctype: 'multipart/form-data',
            data: data,
            headers: createAuthorizationTokenHeader(),
            success: function (data){
            },
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
    });

};

// Get all public images
ImageService.prototype.getPublicImages = function () {
  var self = this;
  return $.ajax({
            url: ImageService._IMAGE,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

            },
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
    });

};

// Get all private images
ImageService.prototype.getPrivateImages = function () {
  var self = this;
  return $.ajax({
            url: ImageService._IMAGE+"/private",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            success: function (response) {

            },
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
    });

};

// Delete image
ImageService.prototype.delete = function (img) {
  var self = this;

  return $.ajax({
            url: ImageService._IMAGE,
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(img),
            headers: createAuthorizationTokenHeader(),
            success: function (response) {

            },
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
    });
};


// Return jquery img object with data on them
ImageService.prototype.loadImage = function (path, data) {
  var finalPath = ImageService._IMAGE+"/"+getJwtToken()+"/"+path;
  var img = new Image ();
  img.src = finalPath;
  img.data =  data;
  img.onclick =  function (){
    openModalImage(this.src, this.data.title +"<br>"+ this.data.description) //When click the image open a modal with it and description
  };
  return img;
}


// Associate an image to a especific feature
ImageService.prototype.asociateToFeature = function (featureType, featureId, img){

  var url = ImageService._IMAGE+"/"+featureType+"/"+featureId;

  return $.ajax({
            url: url,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(img),
            headers: createAuthorizationTokenHeader(),
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
  });
}


// Get all images from feaure
ImageService.prototype.getImagesFromFeature = function (featureType, featureId){

  var url = ImageService._IMAGE+"/"+featureType+"/"+featureId;

  return $.ajax({
            url: url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
  });
}
