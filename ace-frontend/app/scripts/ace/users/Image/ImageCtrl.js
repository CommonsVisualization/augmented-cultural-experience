function ImageCtrl ( imageService ){
  var self = this;
  this._imageService = imageService;

}

ImageCtrl.prototype._imageService = null;    // Image service to make calls (upload image, get gallery...)
ImageCtrl.prototype._publicGallery = null;   // Data for public image gallery
ImageCtrl.prototype._privateGallery = null;  // Data for private image gallery
ImageCtrl.prototype._imageLonLat = null;      // Store the lon lat for imge upload

// *********** IMAGE UPLOAD FORM ***********
// Append the "image upload form" to a html element
ImageCtrl.prototype.appendImageUploadForm = function (element, cb) {
  var self = this;
  element.append(this.imgUploadForm);

  // Add the callback for the upload image form
  $("#imgUploadForm").submit(function (event) {
      event.preventDefault();
      self._imageFormAction(cb);
  });

  // Image input change logic
  $("#imgInput").change( function(e){ self._onInputImgChange(e) });
  // Add logic for radio buttons for show what is the option choosed
  appendPublicPrivateRadioButtons($("#pubprivradbuttons"));

  i18nElementUpdate();
  i18nDataArgsUpdate();
}

// Action when submit the image upload form
ImageCtrl.prototype._imageFormAction = function (cb) {
  var self = this;
  var $form = $('#imgUploadForm');

  // Process the image coordenades if we have it
  var lonlat = {};
  if (self._imageLonLat) {
    lonlat.lon = self._imageLonLat.coords.longitude;
    lonlat.lat = self._imageLonLat.coords.latitude;
  }

  // Create an FormData object
  var formData = new FormData()

  // Add an extra field for the FormData
  formData.append('image', new Blob([JSON.stringify({
              "title": $form.find('input[name="title"]').val(),
              "description": $form.find('input[name="description"]').val(),
              "isPublic": $form.find('input[name="public"]:checked').val(),
              "lat": lonlat.lat,
              "lon": lonlat.lon
            })], {
                type: "application/json"
            }));

  // Get preprocessed image from canvas
  resizeImage( function (dataUrl){

    var blob = dataURItoBlob(dataUrl);
    formData.append('file', blob);

    // Progress bar, remove if exist and then append new one
    $('#myProgress').remove();
    appendProgresBar($form);

    self._imageService.upload (formData)
      .success (function (data){
        openModal ('<i class="fa fa-check" aria-hidden="true"></i><span data-i18n="imageCtrl-uploadForm-completed-title">'+$.i18n("imageCtrl-uploadForm-completed-title")+'</span>  ' ,
          '<span data-i18n="imageCtrl-uploadForm-completed-body">'+$.i18n("imageCtrl-uploadForm-completed-body")+'</span>  ');
        if ($('#imgUploadForm')[0]) {
          $('#imgUploadForm')[0].reset();
        }
        $('#preview').attr('src', '');
        if (cb) cb(data);
      //$('#preview').hide();
      });

  });

}

// Behaviour for input file button
ImageCtrl.prototype._onInputImgChange = function (e) {
  var $input = $('#imgInput');
  var $preview = $('#preview').show();
  var self = this;

  // Check if is an image
  if (~$input[0].files[0].type.indexOf("image")) {

    // Show a preview of the image
    var reader = new FileReader();
    reader.onload = function (e) {
        $preview.attr('src', e.target.result);
    }
    reader.readAsDataURL($input[0].files[0]);

    // Get lat and lon for send it to backend
    var position = null;
    if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (pos){
            self._imageLonLat =  pos;
          });
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
  }
  else {
    $input.val('');
    openModal (
      '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i><span data-i18n="imageCtrl-uploadForm-fileError-body">'+$.i18n("imageCtrl-uploadForm-fileError-body")+'</span>' ,
      '<span data-i18n="imageCtrl-uploadForm-fileError-body">'+$.i18n("imageCtrl-uploadForm-fileError-body")+'</span>  '
    );
  }

}

// *********** GALLERIES ***********
// Append gallery container to an element where the galleries will be filled
// The username is used to put CRUD options on photo, for exemple delete
ImageCtrl.prototype.appendGalleryContainer = function (element){
  element.append(this._imageGallery);
}
// Get private images and creates a buton that opens image upload form and button that open private gallery
ImageCtrl.prototype.gallerySelectorCreation = function (){
  var self = this;

  // Crete button that oppen upload image form
  if ( $('#galleryCards').find('.openUploadImageForm').length == 0 ){
    this.appendButtonOpenUploadPhotoModal( $('#galleryCards'), function (data){
      // Get all private images list
      self._imageService.getPrivateImages().success(function (data) {
        self._privateGallery = data;
        // Update gallery button to add new info
        self.createGalleryButton ($("#galleryCardPrivate"), data);
      });
      // Append the image to the gallery div
      var figure = self._createPhotoFigure(data);
      figure.prependTo($("#imageGallery"));


    });
  }

  // Load the private images and append its to private gallery
  this._imageService.getPrivateImages()
    .success (function (data) {
      self._privateGallery = data;
      self.createGalleryButton ($("#galleryCardPrivate"), data);
      $("#galleryCardPrivate").on("click", function(){ self._privateGalleryLoad() }  );
      self._privateGalleryLoad();
  });

}

// Function for create the gallery selector button
ImageCtrl.prototype.createGalleryButton = function ($galleryButon, data){
  // Reset button
  try {
    $galleryButon.find('.gallery_card_count')[0].remove();
    $galleryButon.find('img')[0].remove();
  } catch (err) {}
  // Create button for select public gallery
  var img = null;
  if (typeof data[0] == 'undefined' ){
    showToast($.i18n("imageCtrl-gallery-empty"));
  }
  else {
    img = this._imageService.loadImage( data[0].path, data[0] ); //Get the image that fill the button
  }
  var count = $("<div />")
    .addClass ("gallery_card_count")
    .append(" <span class='translate' data-args='imageCtrl-gallery-photo,"+data.length+"'>"+$.i18n('imageCtrl-gallery-photo',data.length)+"</span>");

  $galleryButon
    .prepend(img)
    .append(count);
}

// Load private gallery
ImageCtrl.prototype._privateGalleryLoad = function() {
  var self = this;
  if (!self._privateGallery){
    this._imageService.getPrivateImages()
      .success (function (data) {
        self._privateGallery = data;
        self._fillGallery (self._privateGallery);
      });
  } else self._fillGallery (self._privateGallery);
}

// Fill gallery div with all images and info
ImageCtrl.prototype._fillGallery = function (data){
  var self = this;

  var gallery = $("#imageGallery");
  gallery.empty();

  for (var i = 0 ; i < data.length; i++) {

    var figure = this._createPhotoFigure(data[i]);
    gallery.append(figure);

  }
}

// Creates the <figure> for each photo in a gallery
ImageCtrl.prototype._createPhotoFigure = function (data){
  var self = this;
  // Get the image
  var img = self._imageService.loadImage(data.path, data);
  // Create the caption of the image with photo title and creation date
  var isPublic = (data.isPublic) ? '<i class="fa fa-globe" aria-hidden="true"></i> ' : '<i class="fa fa-lock" aria-hidden="true"></i> ';
  var caption = $("<figcaption />").append(
    isPublic+data.title +"<br>",
    '<div class="explanation">'+formatDate (data.creationDate)+'</div>'
  );

  // Append all together
  var figure = $("<figure />").append (img, caption);

  // If the user is the owner of the image put CRUD icons
  // if (data.user.username == USERINFO.username ) {
      // var img = $(figure).find('img')[0].data;
      self.CRUDicons (figure, img.data, figure);
  // }

  figure.on('remove', function () {
    self._imageService.getPrivateImages()
      .success (function (data) {
        self.createGalleryButton ($("#galleryCardPrivate"), data);
      });
  })


  return figure;

}


// Crud icons div, for add to somewhere to add it
// Element: is the container element where to append the buttons
// Img: the image that will recive the CRUD actions
// Container: the image container that will be updated
// deleteCb: callback when delete
ImageCtrl.prototype.CRUDicons = function (element, img, container, deleteCb ){

  var self = this;
  // If the user is the owner of the image put CRUD icons
  if ( USERDATACOMPLETED() && img.user.username == USERINFO.username ) {
    var deleteBtn = $('<i />').addClass("fa fa-trash fa-lg").css("color", "#e75454")
                      .on('click' , function (){
                        openConfirmModal (
                          '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <span data-i18n="imageCtrl-gallery-delete">'+$.i18n("imageCtrl-gallery-delete")+'</span>'  ,
                          function (){
                            self._deleteImage (img, container);
                            if (deleteCb) deleteCb ();
                          }
                        )
                      } );

    return $("<div />").addClass("crud-icons").appendTo(element).append(deleteBtn);
  }
}

// Deletes and image and removes the html element that contain it
ImageCtrl.prototype._deleteImage = function (img, container){
  var self = this;
  self._imageService.delete(img)
    .success(function (data){
      $(container).trigger('remove').remove();
    });
}

// *********** PHOTO MODAL ***********
// Show a modal with the upload photo form
ImageCtrl.prototype.uploadPhotoModal = function ( cb ){
  var htmlForm = $('<div/>');
  openModal('<span data-i18n="imageCtrl-uploadForm-formTitle">'+$.i18n("imageCtrl-uploadForm-formTitle")+'</span>', htmlForm);
  this.appendImageUploadForm(htmlForm, cb);
}

// Add the  button to an element that opens image upload modal.
// cb if upload success
ImageCtrl.prototype.appendButtonOpenUploadPhotoModal = function ( element, cb ){
  if (!USERDATACOMPLETED()) return;
  var self = this;
  element.append(this._buttonOpenImageUploadModal);
  var uploadBtn = element.find('.openUploadImageForm');
  uploadBtn.click(function (){
    self.uploadPhotoModal ( function (data) { //Set the cb on upload success
      if (cb) cb(data);
    });
  })
}

// *********** MINI GALLERY ***********
// Mini gallery object to manage mini galleries on each object
// Callbacks when image is added and when image is delted
ImageCtrl.prototype.MiniGallery = function (imageCtrl, id, header, cbAdd, cbDel){
  if (!id || !imageCtrl) {return;}

  this.id = id; // The id of the gallery
  this._header = header; // The header (title) of the gallery
  this._element = null;
  this._imageCtrl = imageCtrl; //Image ctrl necessary to call upload form modal
  this._photos = []; // The stack of photos
  this.cbAdd = cbAdd; // Callaback when an image is appended
  this.cbDel = cbDel; // Callaback when an image is deleted


  this.uploadBtn = this.id+'Upl'; // Upload button id

  // Append a mini gallery to an element
  this.appendMiniGallery = function (element){
    var self = this;
    element.append(this._html._miniGallery);
    this._element = $('#'+this.id);
    // If has title add it
    if (this._header) this._element.append (this._html._galleryTitle(this._header))
    // Add the  button that open image upload modal
    self._imageCtrl.appendButtonOpenUploadPhotoModal(this._element, function(data){
      self._appendPhoto(data);
    });
  }

  // Simply load a photo and add it to the gallery
  this._loadPhoto = function (img){
    var img = this._imageCtrl._imageService.loadImage(img.path, img);
    this._element.append( this._photoContainer (img) );
  }
  // Append a recently uploaded image to the minigallery
  this._appendPhoto = function (img){
    if (this.cbAdd) this.cbAdd(this._photos, img ); // Send the complete list and the last appended image
    this._loadPhoto(img);
    this._photos [this._photos.length] = img;
  }

  // Returns a Div container with a photo and CRUD buttons
  this._photoContainer = function (img){
    var container = $('<div />').append ( img ).attr('id' , this.id);
    var self = this;

    this._imageCtrl.CRUDicons (container, img.data, container,
                                function (){
                                  // Delete the photo from array
                                  self._photos = self._photos.filter(function(i) {
                                    return i.path !== img.data.path;
                                  })
                                  if (self.cbDel) cbDel(self._photos);
                                }
                              );
    return container;
  }

  // Append to the gallery a list of images
  this.appendImageList = function (imgList){
    for (i = 0 ; i < imgList.length ; i++){
      this._loadPhoto(imgList[i]);
    }
  }
  // This show all the fotos associated to a feature or itinerary ect...
  this._html = {};
  // This mini gallery has also the posibility to open a modal with an upload image form to add data.
  this._html._miniGallery =
  `
  <div class="miniGallery" id="`+this.id+`">
  </div>
  `
  ;
  this._html._galleryTitle = function (text){
    return $('<div />').addClass('explanation').append(text).css('display' , 'block');
  }

}


// *********** HTML'S ***********
// The button that will open an upload image form modal
ImageCtrl.prototype._buttonOpenImageUploadModal =
`
  <button type="button"  class="btn btn-default openUploadImageForm" style="padding:0 !important;" >
    <span class="fa-stack fa-lg">
      <i class="fa fa-camera fa-stack-1x" ></i>
      <i class="fa fa-plus-circle fa-badge fa-outline-inverse" ></i>
    </span>
  </button>
`;

// Html form for upload images
ImageCtrl.prototype.imgUploadForm =
`
<section id="contentPhoto" class="tab-content">
  <div class="formContainer">
    <form method="POST" enctype="multipart/form-data" id="imgUploadForm">
      <h3 data-i18n="imageCtrl-uploadForm-title"></h3>

      <input type="file" accept="image/*" name="imgInput" required id="imgInput" class="inputfile " />
      <label for="imgInput" class="btn"><i class="fa fa-camera" aria-hidden="true"></i></label>
      <br>
      <img id="preview" src="#"  alt="" style="max-height:200px; max-width:200px;"/>
      <br>

      <input type="text" class="translate" required name="title" data-args="[placeholder]input-title">
      <br>

      <input type="text" class="translate" name="description" data-args="[placeholder]input-description">
      <br>
      <div class="sectionTitle" data-i18n="imageCtrl-uploadForm-visibility-title"></div>
      <br>

      <div id="pubprivradbuttons"></div>

      <div class="explanation translate" data-args="imageCtrl-uploadForm-visibility-explanation"></div>
      <br>
      <input type="submit" value="Upload" name="uploadBtn" id="uploadBtn" class="inputfile "/>
      <label  for="uploadBtn" class="btn" ><i class="fa fa-cloud-upload" aria-hidden="true"></i></label>
    </form>
  </div>
</section>
`;


// Image gallery and buttons to select between public and private galls
ImageCtrl.prototype._imageGallery =
`
			<section id="contentGallery" class="tab-content">
        <div id="galleryCards">
          <div id="galleryCardPrivate" class="gallery_card" >
            <div class="gallery_card_title"><i class="fa fa-lock" aria-hidden="true"></i> <span data-i18n="imageCtrl-gallery-private"></span></div>
          </div>
        </div>

				<div id="imageGallery"></div>
			</section>
`;
