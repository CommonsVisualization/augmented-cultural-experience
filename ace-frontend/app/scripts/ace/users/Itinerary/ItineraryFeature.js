// Object to create an itinerary feature and bind it to an html element
function ItineraryFeature( uid ) {
  var binder = new DataBinder( uid ),

      itineraryFeature = {
        attributes: {},

        // The attribute setter publish changes using the DataBinder PubSub
        set: function( attr_name, val ) {
          this.attributes[ attr_name ] = val;
          binder.trigger( uid + ":change", [ attr_name, val, this ] );
        },

        get: function( attr_name ) {
          return this.attributes[ attr_name ];
        },

        _binder: binder
      };

  // Subscribe to the PubSub
  binder.on( uid + ":change", function( evt, attr_name, new_val, initiator ) {
    if ( initiator !== itineraryFeature ) {
      itineraryFeature.set( attr_name, new_val );
    }
  });

  return itineraryFeature;
}
