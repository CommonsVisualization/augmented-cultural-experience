function ItineraryCtrl ( itineraryService, imageController ){
  this._itineraryService = itineraryService;
  // this._mapCtrl = mapCtrl || ItineraryCtrl._mapCtrl;
  this._imageController = imageController;


}

ItineraryCtrl.prototype._itineraryService = null;           // Image service to make calls (upload image, get gallery...)
ItineraryCtrl.prototype._imageController = null;            // Image controller for upload or see user photos
ItineraryCtrl.prototype._mapCtrl = null;

ItineraryCtrl.prototype._generalItineraryList = null;       // List of general itineraries
ItineraryCtrl.prototype._userItineraryList = null;          // List of user itineraries
ItineraryCtrl.prototype._publicItineraryList = null;        // List of public user itineraries

ItineraryCtrl.prototype._isItineraryListSelected = true;    // Used to toggle between itinerary list view and add itinerary
ItineraryCtrl.prototype._features = [];                     // New features for itinerary
ItineraryCtrl.prototype._formData = null;                   // To store the data of itinerary form and don't loose it
ItineraryCtrl.prototype._formDataId = "f1";                 // Used to bind form with an object to remember the content

ItineraryCtrl.prototype._$listContainer=null;               // div that conatins the list of itineraries
ItineraryCtrl.prototype._$itineraryForm=null;               // div that contains the itinerary creation form
ItineraryCtrl.prototype._$section=null;                     // div that stores all the section to restore it on page reload


// Set the principal map controler to open the itineraries
ItineraryCtrl.prototype.setMapCtrl  = function (mapCtrl){
  this._mapCtrl = mapCtrl;
}

// Append the controller html view to an element.
ItineraryCtrl.prototype.appendItinerariesContainer = function (element){
  if (!this._formData || !$('#contentItinerary').length ){ // First execution create all elements
    element.append(this._itinerariesContainer);
    var self = this;
    $("#itButtonTitle").click(function () {self._toggleItineraryPage()})

    this._$listContainer = $("#listContainer");
    this._$itineraryForm = $("#itineraryFormDiv");
    this._$section = $("#contentItinerary");
    this._showHideItineraryPageElements();

  }
}


// Show the previously selected itinerary page
ItineraryCtrl.prototype.showItineraryPage = function (){
  if (this._isItineraryListSelected){ // Select the view that show all itineraries
    $("#itPageTitle").html('<i class="fa fa-list-ul" aria-hidden="true"></i> '+"<span data-i18n='itineraryPage-itiList-title'></span> ");
    $("#itButtonTitle").html(this._addItineraryIcon); //Icon for the button to add itineraries
    this._getItineraries();
  } else { // Select the view that show itinerary creator
    $("#itPageTitle").html('<i class="fa fa-plus-square-o " aria-hidden="true"></i> '+"<span data-i18n='itineraryPage-itiCreation-title'></span> ");
    $("#itButtonTitle").html (this._listItineraryIcon); //Icon for the button to add itineraries
    this._showItineraryCreationForm();
  }
  i18nUpdate();
}


// Toggle itinerary list or itinerary creation dialog
ItineraryCtrl.prototype._toggleItineraryPage = function (){
  this._isItineraryListSelected = !this._isItineraryListSelected;
  this._showHideItineraryPageElements();
  this.showItineraryPage();
}

// Used to properly show and hide what page is selected
ItineraryCtrl.prototype._showHideItineraryPageElements = function (){
  if(!this._isItineraryListSelected) {
    this._$listContainer.hide();
    this._$itineraryForm.show();
  } else {
    this._$listContainer.show();
    this._$itineraryForm.hide();
  }
}

// ****** ITINERARY LIST ******
// Call the web service to get all the itineraries
ItineraryCtrl.prototype._getItineraries = function (){
  var self = this;
    this._itineraryService.getItineraryByName("", function (itineraries){
      self._generalItineraryList = itineraries;
      self._listGeneralItineraries();
    });

    this._getUserItierary();

    this._itineraryService.getPublicItieraries().success (function (itineraries){
      self._publicItineraryList = itineraries;
      self._listPublicItineraries();
    });
}

// Used to recharge the list when an itinerary is deleted
ItineraryCtrl.prototype._getUserItierary = function (){
  var self = this;
  this._itineraryService.getUserItierary(USERINFO.username).success (function (itineraries){
    self._userItineraryList = itineraries;
    self._listUserItineraries();
  });
}

// Creates an html a list of general itinerary elements
ItineraryCtrl.prototype._listGeneralItineraries = function (){

  var generalIti = $('#generalItineraries').addClass("accordion accordion-mini");
  this._generateListAccordeon(generalIti, this._generalItineraryList);

}
ItineraryCtrl.prototype._listUserItineraries = function (){

  var userIti = $('#userItineraries').addClass("accordion accordion-mini");
  this._generateListAccordeon(userIti, this._userItineraryList);

}
ItineraryCtrl.prototype._listPublicItineraries = function (){
  var publicIti = $('#publicItineraries').addClass("accordion accordion-mini");
  this._generateListAccordeon(publicIti, this._publicItineraryList);
}


// Used to centralize the creation of the accordeons that stores the html list
// After calls the list creation
ItineraryCtrl.prototype._generateListAccordeon = function (element, list){
  var content = element.next();
  if (list.length == 0 || !list){
    element.hide();
    content.hide();
    return;
  } else {
    content.show();
    element.show();
  }
  content.empty();
  generateAccordeon();
  this._generateItineraryList (list, content );
}


// Generate an html list of itineraries on a especific element
ItineraryCtrl.prototype._generateItineraryList = function (list, element){

  var self = this;

  // Check if is a public itinerary
  function isPublic (iti){
    if (iti.code == "itiaud" || iti.isPublic == true) return true;
    else return false;
  }

  // Add crud icons to an itinerary
  function CRUDIcons (iti){
    var deleteBtn = $('<i />').addClass("fa fa-trash fa-lg").css("color", "#e75454")
                      .on('click' , function (){
                        openConfirmModal (
                          "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> " +  '<span data-i18n="itineraryPage-modal-delete">'+$.i18n("itineraryPage-modal-delete")+'</span>' ,
                          function (){
                            self._itineraryService.deleteItinerary (iti.id).success(
                              function(){
                                self._getUserItierary();
                              }
                            );
                          }
                        )
                      });
    var container = $("<div />").addClass("crud-icons-inline").append(deleteBtn);
    return container;
  }

  // Iterate all itineraries and apend it to element
  for (var idx=0; idx < list.length; idx++) {

    var itinerary = list[idx];

    var isPublicIcon = (isPublic(itinerary)) ? '<i class="fa fa-globe" aria-hidden="true"></i> ' : '<i class="fa fa-lock" aria-hidden="true"></i> ';

    // Container element for itinerary list html objects
    var $itineraryFeatureElem =
      $('<div>')
        .addClass('list-card')
        .appendTo(element);

    // Element that contains info of itinerary and open it if click
    var itineraryInfo  =
      $('<span>')
        .html(isPublicIcon + itinerary.name + '<span> - '+itinerary.description+'</span>')
        .data('itData', itinerary)
        .click (function (){
          var iti = $(this).data('itData');
          // Look if has the features
          if (!iti.features){
            self._getItineraryFeatures(iti);
          } else {
            self._showItinerary(iti);
          }
        })
        .appendTo ($itineraryFeatureElem );

    // Add crud icons if the user is the propertary
    if (itinerary.user && itinerary.user.username == USERINFO.username){
      $itineraryFeatureElem.append(CRUDIcons(itinerary))
    }
  }
}

// Get the itinerary features of an itinerary code or id (depending if is general itinerary or user itinerary)
ItineraryCtrl.prototype._getItineraryFeatures = function (iti){

  // The general itineraries id is called "code", otherwhise, useritineraries id is called "id"
  var code;
  if (iti.code) {
    code = iti.code;
  }
  else {
    code = iti.id;
  }
  var self = this;
  self._itineraryService.getItineraryByName(code, function (itinerary){
    self._showItinerary (itinerary)
  });
}
// Show an itinerary on map
ItineraryCtrl.prototype._showItinerary = function (itinerary){
  this._mapCtrl.showItinerary(itinerary);
}



// ****** ITINERARY CREATION FORM ******
// Show the creation form
ItineraryCtrl.prototype._showItineraryCreationForm = function (){
  var self = this;
  var container = this._$itineraryForm;

  // If is the first execution creates the form and bind the content to this_formData
  if (!this._formData) {
    container.empty(); // Erase previous form

    this._formData = new Itinerary (this._formDataId); // Creates new itinerary object

    this._formData.set("inputItineraryName" , '' ); // Create the attribute that will store the NAME of the itinerary
    this._formData.set("inputItineraryDesc" , '' ); // Create the attribute that will store the DESCRIPTION of the itinerary
    this._formData.set("inputItineraryPublic" , '' ); // Create the attribute that will store the DESCRIPTION of the itinerary

    container.append(this._itineraryCreationForm () ); // Add the html for the first part of the form
    this._showItineraryCreationFormLogic();

    this._addItineraryFeatureForm();
    this._formData.set("features" ,this._features );

  } else if (container.children().length == 0) {
    container.append(this._itineraryCreationForm () );
    this._showItineraryCreationFormLogic();

    this._addItineraryFeatureForm();
  }

}

// Used to add the logic to the itinerary creation form. For example, after the first execution because is dinamically created
ItineraryCtrl.prototype._showItineraryCreationFormLogic = function (){
  var self = this;
  // Append radio buttons for public and private and bind the result to formData
  appendPublicPrivateRadioButtons($("#pubprivradbuttonsItin"),
              function (val){  self._formData.set("inputItineraryPublic" , val ); });

  $("#addFeature").click(function (){self._addItineraryFeatureForm()}) // Button for add new features
  $("#clearItineraryForm").click(function (){self._clearItineraryForm()}) // Button for clear itinerary form
  $("#itineraryFormForm").submit(function (event){
    event.preventDefault();
    self._createItinerary()
  }) // Button that calls itinerary creation


}

ItineraryCtrl.prototype._createItinerary = function (){
  var self = this;
  var normalizedItinerary = this._normalizeItinerary (this._formData)
  if (!normalizedItinerary){
    openModal (
      '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ' + $.i18n("itineraryPage-itiCreation-modal-error-onepoint-title"),
       $.i18n("itineraryPage-itiCreation-modal-error-onepoint-body")
      );
      return;
  }
  this._itineraryService.createItinerary (normalizedItinerary).success (
    function (){
      openModal (
        '<i class="fa fa-check" aria-hidden="true"></i> ' +$.i18n("itineraryPage-itiCreation-modal-success-title"),
        $.i18n("itineraryPage-itiCreation-modal-success-body"));
      self._clearItineraryForm();

  });
}

ItineraryCtrl.prototype._normalizeItinerary= function (formData){
  if (formData.get("features").length <= 0) {
    return false;
  }

  var iti = {
    "name" : formData.get("inputItineraryName"),
    "description" :  formData.get("inputItineraryDesc"),
    "isPublic" : formData.get("inputItineraryPublic"),
    "features" : []
  };


  formData.get("features").forEach(function (feature){

    var feat = {
      "title" : feature.get("title"),
      "featureOrder" : feature.get("featureOrder"),
      "description" : feature.get("description"),
      "lat" : feature.get("lat"),
      "lon" : feature.get("lon"),
      "images" : []
    }


    if (feature.get("images")){
      feature.get("images").forEach (function (image) {
        feat.images.push ({
          "path" : image.path
        })
      })
    }

    iti.features.push(feat);

  })

  return iti;

}


// Add a feature form to the itinerary form
ItineraryCtrl.prototype._addItineraryFeatureForm = function (){

  var self=this;

  var featureNumber = this._features.length;
  // var featureid="b"+featureNumber;
  var featureid=makeid();
  this._features [featureNumber] =  new ItineraryFeature (featureid); //Create a binded object with html elements
  this._features [featureNumber].set ('uid' , featureid);

  // Creates the accordeon
  var container = $("#itiFeaturesContainer");
  var featureForm = this._generateItineraryFeatureForm( this._features [featureNumber] );
  container.append(featureForm);
  generateAccordeon();


  // Create feature element
  this._features [featureNumber].set ('featureOrder' , featureNumber);
  this._features [featureNumber].set ('title' , $.i18n("itineraryPage-itiCreation-pointNme"));
  this._features [featureNumber].set ('description' , '');
  this._features [featureNumber].set ('lat' , '');
  this._features [featureNumber].set ('lon' , '');
  this._features [featureNumber].set ('images' , null);

  // Bind it to html elements
  // var panel = $('#panel'+featureid);
  var panel = $(featureForm.find('.accordeonPanel')[0]);
  panel.append('<input type="text" class="inputText translate" data-bind-'+featureid+'="title" data-args="[placeholder]input-name" '+
                ' placeholder='+$.i18n("input-name")+' required ></input>');
  panel.append('<br><input type="text" class="inputText translate" data-bind-'+featureid+'="description" '+
                'data-args="[placeholder]input-description"  placeholder='+$.i18n("input-description")+' ></input>');
  // Create the map
  var mapId='map'+featureid;
  panel.append('<div id="'+mapId+'" class="" data-title="Mapa" style="background-color:white; width:100%;height:300px; margin-top:5px;">');
  var mapCtrl = new MiniMapCtrl( $('#'+mapId),
        function (lat, lon){
            self._features [featureNumber].set ('lat' , lat);
            self._features [featureNumber].set ('lon' , lon);
          } );
  mapCtrl.show();
  // Input to store the lat and lon
  panel.append('<div style="width:100%;height:20px;text-align: center !important;" class="explanation">'+
                  '<input type="text" data-bind-'+featureid+'="lat" data-readonly  required />'+
                  // ' <span data-bind-'+featureid+'="lat" />  '+
                  // '<span data-bind-'+featureid+'="lon" />'+
                '</div>');

                $("input[data-readonly]").keydown(function(e){
                    e.preventDefault();
                });

  // Add mini gallery to upload fotos
  var galleryId = featureid+'gal';
  var minigal = new this._imageController.MiniGallery(
                                            this._imageController,
                                            galleryId,
                                            function (data){ //Callback when image is added
                                                self._features [featureNumber].set ('images' , data);
                                            },
                                            function (data){ //Callback when image is deleted
                                                self._features [featureNumber].set ('images' , data);
                                            }
                                          );
  minigal.appendMiniGallery(panel);

}

// Generates itinerary features form for add new features to form
ItineraryCtrl.prototype._generateItineraryFeatureForm = function (feature){
  var uid = feature.get("uid");
  var self = this;
  var accordeonDiv = $('<div />' );
  // Appends the accordeon with feature creation form
  accordeonDiv.append('<div class="accordion" > '+
                        '<span data-bind-'+ uid+'="featureOrder"> </span> - '+
                        '<span data-bind-'+ uid+'="title"></span>'+
                        '<span class="close close-div">&times;</span></div>');
  // Add the logic for close button of the accordeon
  accordeonDiv.find(".close")[0].onclick = function() {
    self._removeItineraryFeatureForm(accordeonDiv, feature);
  }
  // The form content
  var accordeonPanel = $('<div />' )
        .addClass ("accordeonPanel")
        .attr('id', 'panel'+uid);
  accordeonDiv.append(accordeonPanel);
  return accordeonDiv;
}

// Remove a feature and update the features array to set the correct order
ItineraryCtrl.prototype._removeItineraryFeatureForm = function (div, feature){
  div.remove();
  var uid = feature.get("featureOrder");
  this._features.splice($.inArray(feature, this._features ),1);
  for (i = uid; i < this._features.length ; i++){
    this._features[i].set("featureOrder" , i);
  }
}


// Clear itienerary form, setting his valor to null and calling the function to redraw it
ItineraryCtrl.prototype._clearItineraryForm = function (){
  this._formData = null;
  this._features = [];
  this.showItineraryPage();
}


// ***** HTML *****
// Itinerary page container
ItineraryCtrl.prototype._itinerariesContainer =
			'<section id="contentItinerary" class="tab-content">'+
        '<div class="itineraryTitle" >'+
          '<div class="btn  btn-right" style="padding:5px 10px;"  id="itButtonTitle" > </div>'+
          '<div id="itPageTitle"></div>'+
        '</div>'+
        '<div id="itineraryContainer">'+
          '<div id="listContainer">'+
            '<div id="generalItineraries"><span data-i18n="itineraryPage-itiList-globalItinearies"></span></div>'+
              '<div class="accordeonPanel"></div>'+
            '<div id="userItineraries"><span data-i18n="itineraryPage-itiList-ownItineraries"></span></div>'+
              '<div class="accordeonPanel"></div>'+
            '<div id="publicItineraries"><span data-i18n="itineraryPage-itiList-publicItineraries"></span></div>'+
              '<div class="accordeonPanel"></div>'+
          '</div>'+
          '<div id="itineraryFormDiv" ></div>'+
        '</div>'+
			'</section>'
;
// Icon fa stack for toggle between itinerary list and itinerary creation
ItineraryCtrl.prototype._addItineraryIcon=
`
<span class="fa-stack fa-1x">
  <i class="fa fa-map-marker fa-stack-2x" ></i>
  <i class="fa fa-plus-circle fa-badge fa-outline-inverse" ></i>
</span>
`;
ItineraryCtrl.prototype._listItineraryIcon=
`
<span class="fa-stack fa-1x">
  <i class="fa fa-list-alt  fa-stack-2x" ></i>
  <i class="fa fa-map-marker fa-badge fa-outline-inverse" ></i>
</span>
`;

ItineraryCtrl.prototype._itineraryCreationForm = function (){

  return '<form id="itineraryFormForm" class="normalForm">'+
  '<input type="submit" value="crea"  id="createItinerary" name="createItinerary" class="inputfile "/>'+
  '<label  for="createItinerary" class="btn btn-default" ><i class="fa fa-check" aria-hidden="true"></i><span data-i18n="itineraryPage-itiCreation-btn-create"></span></label>'+
  '<input type="button" value="crea"  id="clearItineraryForm" name="clearItineraryForm" class="inputfile "/>'+
  '<label  for="clearItineraryForm" class="btn btn-default" ><i class="fa fa-times" aria-hidden="true"></i><span data-i18n="itineraryPage-itiCreation-btn-clean"></span></label>'+
    '<div class="form-group">'+
        '<input type="text" class="inputText translate" id="inputItineraryName" data-args="[placeholder]input-name"'+
           'required name="itiName" data-bind-'+this._formDataId+'="inputItineraryName" > </input>'+
         '<br><input type="text" class="inputText translate" id="inputItineraryDesc"  data-args="[placeholder]input-description"'+
                  'name="itiDesc" data-bind-'+this._formDataId+'="inputItineraryDesc" >  </input>'+
         '<div id="pubprivradbuttonsItin"></div>'+
    '</div>'+
    '<div class="form-group" id="itiFeaturesContainer"></div>'+
    '<div class="btn btn-circular" id="addFeature"><i class="fa fa-plus" ></i> </div>'+
  '</form>'
  ;
}
