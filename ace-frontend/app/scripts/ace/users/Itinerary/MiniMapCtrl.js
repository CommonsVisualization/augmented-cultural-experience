/** Controlls the map creation (caracteristics, Behaviors...), buttons Behaviors..
- Buttons on the map
- Leaflet configuration
**/

// Create the buttons on the bottom of the map
var FeatureControlMiniMap = L.Control.extend({
    options: {
            position: 'bottomright',
            /** Map controller. */
            ctrl    : null
    },

    _cmdGeolocationButton : null,

    onAdd: function (map) {
            this._map = map;
            var container = this._createComponents();
            return container;
    },


    _createComponents: function () {
        var self = this;
        var ctrl = this.options.ctrl;
        var className = 'buttonPanel';
        var container = L.DomUtil.create('div', className);
        var $container = $(container);
        if (false) window.setInterval(function() {
            if ($container.is(':visible') === true) {
                var parentHeight = $('#map').height();
                var containerY = $container.offset().top;
                var containerHeight = $container.height();
                if (containerY + containerHeight + 10> parentHeight) {
                    $container.css({
                       top : parentHeight - containerHeight - 10
                    });
                }
            }
        }, 500);

        this._cmdGeolocationButton = new CommandButton(
                'gps', "", 'images/icons/android-locate.svg', ctrl, function() {
                    ctrl._setUserTracking(true);
                });

        this._cmdGeolocationButton.$element[0].type = 'button';

        $(container).append(this._cmdGeolocationButton.$element);




        return container;
    }


});

var MarkIcon = L.Icon.extend({
    options : {
        'iconUrl'    : 'images/markers/marker-blue.gif',
        'iconSize'   : new L.Point(17, 19),
        'iconAnchor' : new L.Point(0, 19),
        'popupAnchor': new L.Point(9, -15)
        //shadowUrl: '../docs/images/leaf-shadow.png',
        //shadowSize: new L.Point(68, 95),
    }
});

var TouchIcon = L.Icon.extend({
    options : {
        'iconUrl'    : 'images/touch.png',
        //'iconUrl'    : '../images/touch.png', !!!!!!!!!!!!!!!!!!!
        'iconSize'   : new L.Point(40, 40),
        'iconAnchor' : new L.Point(20, 20)
        //shadowUrl: '../docs/images/leaf-shadow.png',
        //shadowSize: new L.Point(68, 95),
    }
});


L.Popup.include({
    getSource : function() {
        return this._source;
    }
});


function MiniMapCtrl($mapElement, cb) {
    var self = this;

    this._$mapElement = $mapElement || this._$mapElement;

    this.cbLonLat = cb;


    // self._executeWhenMapIsVisible(self, function() {self._map.invalidateSize();}, [], true);


}

MiniMapCtrl._INITIAL_LATLON = new L.LatLng(41.3621, 2.15808);
MiniMapCtrl._MAP_BOUNDS = new L.LatLngBounds(
                        new L.LatLng(41.35951, 2.15510),
                        new L.LatLng(41.36510, 2.16140)
                      );
MiniMapCtrl._INITIAL_ZOOM = 17;
MiniMapCtrl._FEATURES_MIN_ZOOM = 19;
MiniMapCtrl._ITINERARY_FEATURE_AUTODISPLAY_DISTANCE = 10;

MiniMapCtrl.prototype._map = null;
MiniMapCtrl.prototype._$mapElement = null;
MiniMapCtrl.prototype._canvasLayer = null;

MiniMapCtrl.prototype._marker = null // Used to put the marker
MiniMapCtrl.prototype._markerGroup = null; // The layer that will contain the marker
MiniMapCtrl.prototype.cbLonLat = null; // used as callback function to store lat and lon data

MiniMapCtrl.prototype._watchingGpsIsActive = false;
MiniMapCtrl.prototype._userTrackingIsActive= false;
MiniMapCtrl.prototype._userMark = null;

MiniMapCtrl.prototype._lastAdquiredPositionTime = null;

MiniMapCtrl.prototype._touchMarker = new L.Marker(new L.LatLng(0,0),
                                        {'icon': new TouchIcon(),
                                         'clickable' : false});

MiniMapCtrl.prototype._featureControl = null;

MiniMapCtrl.prototype._loadingControl = null;

MiniMapCtrl.prototype.initialized = false;

MiniMapCtrl.prototype.initialize = function() {

    var self = this;
    // this._$mapElement.css('height', '100%');
    this._map = new L.Map(this._$mapElement[0],
                {crs : L.CRS.EPSG4326,
                    attributionControl :false,
                 zoomControl : true,
                 maxZoom : 22,
                 maxBounds : MiniMapCtrl._MAP_BOUNDS,
                 doubleClickZoom : false,
                 keyboard : true,
                //  keyboardPanOffset : 250
                }).on('click', function(evt) {
                    self._showTouch(evt.latlng, evt.layerPoint);
                    self._putMarker(evt.latlng, evt.layerPoint);
                    self._setUserTracking (false);
                    if (self.cbLonLat) self.cbLonLat(evt.latlng.lat, evt.latlng.lng);
                    if (evt.stopPropagation) evt.stopPropagation();
                }).on('dragend', function(evt) {
                    self._setUserTracking(false);
                }).on('popupopen', function(evt) {
                    var marker = evt.popup.getSource();
                    // var feature = marker.options.feature;
                    if (evt.stopPropagation) evt.stopPropagation();
                });
    // Using geojson implies drawing vectors and that pane is usually
    // over the tile pane with the canvas.
    this._map.getPanes()['tilePane'].style.zIndex = 5;
    var colorsPromise = $.getJSON('colors.json');
    var fitoPromise = $.getJSON('fitoepisodis.json');
    var furniturePromise = $.getJSON('baranes.json');
    $.when(colorsPromise, fitoPromise, furniturePromise)
     .then(function(colorsResponse, fitoepisodisResponse, furnitureResponse) {
        self._fitoColors = colorsResponse[0];
        L.geoJson(fitoepisodisResponse[0], {
            minZoom : MiniMapCtrl._INITIAL_ZOOM,
            maxZoom : 24,
            touchZoom : true,
            attributionControl : false,
            style: function (feature) {
                var color = self._fitoColors[feature.properties.ID];
                var style = {
                       "clickable": false,
                       "color": color ? color.stroke : 'darkgray',
                       "fillColor": color ? color.fill : 'lightgray',
                       "weight": 3.0,
                       "opacity": 0.8,
                       "fillOpacity": 0.8
                   };
                   return style;
            }
        }).addTo(self._map);
        L.geoJson(furnitureResponse[0], {
            minZoom : MiniMapCtrl._INITIAL_ZOOM,
            maxZoom : 24,
            touchZoom : true,
            attributionControl : false,
            style: function (feature) {
                var style = {
                       "clickable": false,
                       "color": 'darkgray',
                       "weight": 3.5,
                       "opacity": 0.7
                   };
                   return style;
            }
        }).addTo(self._map);
    });
    self._canvasLayer = L.tileLayer.canvas({
        minZoom : MiniMapCtrl._INITIAL_ZOOM,
        maxZoom : 22,
        async   : true
    });
    self._canvasLayer.drawTile = function(canvas, tilePoint, zoom) {
        self._drawTiles(canvas, tilePoint, zoom);
    };
    self._canvasLayer.addTo(self._map);

    // var zoomControl = new L.Control.Zoom({'position' : 'topleft'});
    this._featureControl = new FeatureControlMiniMap({'ctrl' : this});
    this._loadingControl = new LoadingControl({'text' : getLocalWord("leaflet-loadingControl"), 'localeKey' : "leaflet-loadingControl" });


    this._markerGroup = new L.LayerGroup();
    this._markerGroup.addTo(this._map);

    // this._map.addControl(zoomControl)
    this._map
        .addControl(this._featureControl)
        .addControl(this._loadingControl)
        .setView(MiniMapCtrl._INITIAL_LATLON, MiniMapCtrl._INITIAL_ZOOM);

    this._map.on('locationfound', function(evt) {
        self._onLocationFound(evt.latlng, evt.bounds, evt.accuracy, evt.heading);
    });
    this._map.on('locationerror', function(exc) {
        self._onLocationError(exc.message);
    });


    this._$mapElement.data('map', this._map);

    this._userMark = new UserMark(this._map);
    // this._setUserTracking(false);
    // this._startGeolocation();


    this.initialized = true;
};

MiniMapCtrl.prototype.show = function() {

    var self = this;
    if (self.initialized == false)
        self.initialize();

};


MiniMapCtrl.prototype._executeWhenMapIsVisible = function(context, fn, args, keepAlive) {
    if (this._$mapElement.is(':visible') == true) {
        fn.apply(context, args);
    } else {
    }
};



MiniMapCtrl.prototype._setUserTracking = function(value) {
    // if (value == true) {
    //     if (this._watchingGpsIsActive == false) {
    //         this._startGeolocation();
    //     }
    //     this._userTrackingIsActive = true;
    // } else {
    //     this._userMark.clear();
    //     this._userTrackingIsActive = false;
    // }
    if (value == true) {
      this._startGeolocation();
      this._userTrackingIsActive = true;
    } else {
      this._userMark.clear();
      this._userTrackingIsActive = false;
    }

};

MiniMapCtrl.prototype._startGeolocation = function() {
    this._watchingGpsIsActive = true;
    this._map.locate({
        setView : true,
        enableHighAccuracy : true
    });
};

MiniMapCtrl.prototype._stopGeolocation = function() {
    this._watchingGpsIsActive = false;
    this._userMark.clear();
    this._map.stopLocate();
};

MiniMapCtrl.prototype._onLocationFound = function(latlng, bounds, accuracy, heading){
    if (MiniMapCtrl._MAP_BOUNDS.contains(latlng) === true ) {
        var currentTime = new Date().getTime();
        if (this._lastAdquiredPositionTime &&
            (currentTime - this._lastAdquiredPositionTime < 500)) {
            return;
        }
        this._lastAdquiredPositionTime = currentTime;

        this._putMarker(latlng);

        if (this._userTrackingIsActive === true) {
            this._userMark.updatePosition(latlng, accuracy);
            if (this._map.getBounds().contains(bounds) === false) {
                this._map.setView(latlng);
            }
        }
    }
};

MiniMapCtrl.prototype._onLocationError = function(errorMessage){
    this.showMessage($.i18n("mapcontroller-gpsError"));
    this._stopGeolocation();
};



MiniMapCtrl.prototype._pictograms = {
    '-1': { image: _loadImage('images/pictograms/picto_round.png'),  color : '#558C89'},
    '1' : { image: _loadImage('images/pictograms/picto_small1.png'), color : '#cdcd00'},
    '2' : { image: _loadImage('images/pictograms/picto_small2.png'), color : '#8b4789'},
    '3' : { image: _loadImage('images/pictograms/picto_small3.png'), color : '#8B8B00'},
    '4' : { image: _loadImage('images/pictograms/picto_small4.png'), color : '#8b5a00'},
    '5' : { image: _loadImage('images/pictograms/picto_small4.png'), color : '#8b5a00'},
    '6' : { image: _loadImage('images/pictograms/picto_small6.png'), color : '#8b2500'},
    '7' : { image: _loadImage('images/pictograms/picto_small6.png'), color : '#8b2500'},
    '8' : { image: _loadImage('images/pictograms/picto_small8.png'), color : '#1C1C1C'},
    '9' : { image: _loadImage('images/pictograms/picto_small9.png'), color : '#8b5a00'},
    '10': { image: _loadImage('images/pictograms/picto_small10.png'), color : '#1C1C1C'},
    '11': { image: _loadImage('images/pictograms/picto_small11.png'), color : '#458b00'},
    '12': { image: _loadImage('images/pictograms/picto_small12.png'), color : '#00ee00'}
};


/**Draw the tiles on the map on MiniMapCtrl._FEATURES_MIN_ZOOM **/
MiniMapCtrl.prototype._drawTiles = function(canvas, tilePoint, zoom) {
        var zoomLevel = zoom ? zoom : this._map.getZoom();
        if (zoomLevel <= MiniMapCtrl._FEATURES_MIN_ZOOM) {
            this._canvasLayer.tileDrawn(canvas);
            return;
        }
        var QUERY_MARGIN = 25;
        var self = this;
        var tileSize = this._canvasLayer.options.tileSize;
        var leftTopPointInWorld= tilePoint.multiplyBy(tileSize).subtract(new L.Point(QUERY_MARGIN, QUERY_MARGIN));
        var leftTopLatLng = this._map.unproject(leftTopPointInWorld);
        var rightBottomPointInWorld= leftTopPointInWorld.add(new L.Point(tileSize+QUERY_MARGIN*2, tileSize+QUERY_MARGIN*2));
        var rightBottomLatLng = this._map.unproject(rightBottomPointInWorld);



        return this;
  };



MiniMapCtrl.prototype._showTouch = function(latlng, point) {
    var self = this;
    this._touchMarker.setLatLng(latlng);

    this._map.addLayer(this._touchMarker);
    setTimeout(function() {
        self._map.removeLayer(self._touchMarker);
    }, 250);

};

// Function that put a marker on the map and clear the precedent
MiniMapCtrl.prototype._putMarker = function(latlng) {

  // Put marker on map
  this._markerGroup.clearLayers();
  iconImage =  'images/markers/marker-orange.gif';
  var icon = new MarkIcon({'iconUrl' : iconImage});
  this._marker = new L.Marker(new L.LatLng(latlng.lat, latlng.lng),
                            {'icon': icon});
  this._markerGroup.addLayer(this._marker);

  this.cbLonLat (latlng.lat , latlng.lng); // Retturn the position to the callback

}


MiniMapCtrl.prototype.showMessage = function(msg) {
    var $div = $('#message');
    if ($div.length == 0) {
        $div = $('<div/>')
           .attr('id', 'message')
           .css({
               top : '35px',
               position: 'absolute',
               display: 'none',
               height : '1em',
               zIndex : 1E6,
               fontSize : '1.2em',
               'min-height' : '',
               padding : '1em 1em 2em 1em',
               color : 'white',
               backgroundColor : 'silver',
               textAlign : 'center'
           })
           .click(function() {
               $(this).stop().fadeOut(function() {
                   $(this).detach();
               });
           })
           .appendTo('body')
           .fadeIn()
           .delay(10*1000)
           .fadeOut(function() {
              $(this).detach();
           });
    };
    $div.html(msg).fadeIn();
};
