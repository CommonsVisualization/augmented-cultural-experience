// function createUserCtrl = function(username, userService) {
//     return new UserCtrl (username);
// };


function UserCtrl (iui, $element, userService, imageController, itineraryCtrl ){
  var self = this;
  this._iui = iui;
  this._$element = $element;
  this._userService = userService;
  this._imageController = imageController;
  this._itineraryCtrl = itineraryCtrl;


}

UserCtrl.prototype._userService = null;     // User service to make calls
UserCtrl.prototype._imageController = null; // Image controller for upload or see user photos
UserCtrl.prototype._itineraryCtrl = null;   // Controller to manage itineraries from user.
UserCtrl.prototype._iui = null;             // Iui framework to create the page
UserCtrl.prototype._$element = null;        // $element where the page will be created
UserCtrl.prototype._firstExect = true;        // Ugly thing to solve a bug

var USERINFO = null;                         // Global variable that stores the user info
// Function used to validate if we have token || user information, for minigallery purpose
var USERDATACOMPLETED = function (){
  if (USERINFO == null || getJwtToken() == null) return false;
  return true;
}

// Show the page top bar and the content
UserCtrl.prototype.show = function (){
  this._initializeContainer();
  // Check if there is user data. If not calls the backend and authenticate
  if (!this._weHaveUserInfo() || getJwtToken() == null){
    this.authenticate();

  } else {
    if (this._firstExect) { // First execution
      this._firstExect = false;
      this._createElements();

    }
  }
}

// Show page top bar
UserCtrl.prototype._initializeContainer = function() {
    this._iui.showPageById('userPage');
};

// Creates html elements from the page
UserCtrl.prototype._createElements = function() {

    this._$element.empty();
    var self=this;

      // User name
      $('<li>')
        .appendTo( this._$element  )
        .addClass ("userName")
        .attr('id', 'userName');

      // Tab menu
      $('<div>')
        .appendTo( this._$element  )
        .addClass('noPadding')
        .html(this._userTabMenu) ;

      // Tab menu tabs
      var tabs = "";
      // Content that tabs menu open
      var contents = "";

      // If user is not registered, add menu tab and sections for register or login options
      if (!this._isRegistred ()){

        var notRegistredForms = '<section  id="imNotRegistred" class="tab-content">' +this._loginUserForm +'<br>'+ this._registerUserForm+ '</section>';

        tabs = tabs + this._tab3User;
        contents = notRegistredForms;

      }
      tabs = tabs + this._tab2Gallery + this._tab4Itineraries;

      // Add all tabs menu and contents (displayed none)
      $('#userTabMenu')
        .html(tabs + contents);

      // Append gallery container
      this._imageController.appendGalleryContainer($('#userTabMenu'));
      // Append also the itinerary container
      this._itineraryCtrl.appendItinerariesContainer($('#userTabMenu'));



      // Add logic when gallery or itinerary tab is selected
      var $radio = $('[name="tabs"]').on("change", function(){
        if ($('#tab2Gallery').is(':checked') ){
          self._imageController.gallerySelectorCreation ();
        }
        else if ($('#tab4Itineraries').is(':checked') ) {
          self._itineraryCtrl.showItineraryPage ();
        }

      });

      // Add callbacks for non registred user forms
      if (!this._isRegistred ()){

        // Add the callback for the forms
        $("#registerForm").submit(function (event) {
            event.preventDefault();
            self._registerFormAction();
        });

        $("#loginForm").submit(function (event) {
            event.preventDefault();
            self._loginFormAction();
        });

        generateAccordeon(); // Start the javascript to open and close the accordions
      }

      this._fillElements (); // Fill with userdata

      i18nUpdate(); // Fill the translations

};



// ***** Page visualization and behaviour funtions
// Fill the html elements with user info
UserCtrl.prototype._fillElements = function() {
  if (this._isRegistred ()){
    $('#imNotRegistred').remove();
  }
  $('#userName')
    .html("<span data-i18n='userPage-welcome'>"+$.i18n("userPage-welcome")+"</span>" +" "+ this.getUsername() )
}

// Call to authenticate resource on the backend and then get user info.
UserCtrl.prototype.authenticate = function (logindata){
  var self = this;
  if (getJwtToken()==null){
    return self._userService.doLogin(logindata)
      .success(function (){
        self._userService.getUserInformation(self)
          .success (function (response){
            console.log("Authentication completed " , response);
            self._createElements();
          })
      })
      // If the error get is bad credential exception, append the form to make the login
      .error(function (jqXHR, textStatus, errorThrown) {
          if ($('#imNotRegistred').length == 0){
            self._$element.append('<section  id="imNotRegistred" class="tab-content">' +self._loginUserForm+ '</section>');
            i18nElementUpdate(self._$element);
            $('#imNotRegistred').find("button").eq(0).addClass("active"); // Start the accordion as active
            generateAccordeon();

            $("#loginForm").submit(function (event) {
                event.preventDefault();
                self._internetLogin();
            });
          }
      });
    }
    return self._userService.getUserInformation(self)
      .success (function (response){
        console.log("Authentication completed " , response);
        self._createElements();
      })
}

// Recharge info from user
UserCtrl.prototype._weHaveUserInfo = function (){
  var self =this;
  try {
    if (!USERINFO) {
      return false;
    }
    return true;
  } catch (err) {
    openModal ("Error: " , err);
    return false;
  }
}


// Check if the username is a mac address and return guest string if it is
UserCtrl.prototype.getUsername = function() {

  if ( !this._isRegistred() ) {
    return "<span data-i18n='userPage-guest' >"+$.i18n("userPage-guest")+"</span>";
  }
  return USERINFO.username;
}

//Check if the user is register. This mean that if his username is a mac address (So not registred) returns false
UserCtrl.prototype._isRegistred = function (){
  var regex = /^(([A-Fa-f0-9]{2}[:]){5}[A-Fa-f0-9]{2}[,]?)+$/
  if (USERINFO &&  regex.test(USERINFO.username)) {
    return false;
  }
  return true;
}



// ********* Forms and pages behaviour
// Hang comportament for user registration form that gives to user a username and password
UserCtrl.prototype._registerFormAction = function () {
        var self = this;
        var $form = $('#registerForm');
        var formData = {
            "username": $form.find('input[name="username"]').val(),
            "password": $form.find('input[name="password"]').val(),
            "email": $form.find('input[name="email"]').val()
        };

        this._userService.registerUser (USERINFO.username, formData)
          .success (function (response) {
            self._userService._removeJwtToken();
            USERINFO = null;
            self.authenticate()
              .done (function () {
                self._fillElements();
                openModal (
                  '<i class="fa fa-check-circle" aria-hidden="true"></i> ' +'<span data-i18n="userPage-modal-registred-title">'+$.i18n("userPage-modal-registred-title")+'</span>' ,
                  '<span data-i18n="userPage-modal-registred-body">'+$.i18n("userPage-modal-registred-body")+'</span>'
                );
              });

          });
};

// Action when login form is submited
UserCtrl.prototype._loginFormAction = function () {

  var self = this;
  var $form = $('#loginForm');
  var formData = {
      "username": $form.find('input[name="username"]').val(),
      "userPin": $form.find('input[name="password"]').val()
  };

  this._userService.addIdentityToUser (USERINFO, formData)
    .success (function (response) {
      self._userService._removeJwtToken();
      USERINFO = null;
      self.authenticate()
        .done (function () {
          self._fillElements();
          openModal (
            "<i class='fa fa-check-circle' aria-hidden='true'></i> " +'<span data-i18n="userPage-modal-login-title">'+$.i18n("userPage-modal-login-title")+'</span>',
            '<span data-i18n="userPage-modal-login-body">'+$.i18n("userPage-modal-login-body")+'</span>'
          );
        });

    });

}

// Action when login form from internet mode is submited
UserCtrl.prototype._internetLogin = function () {

  var self = this;
  var $form = $('#loginForm');
  var formData = {
      "username": $form.find('input[name="username"]').val(),
      "password": $form.find('input[name="password"]').val()
  };

  self.authenticate(formData);
}



// ********* Html generated for and each deppending for user.

// Menu tabs
// Note: This tabs are triggered with a pure css rules
UserCtrl.prototype._userTabMenu = '<div class="tab_container" id="userTabMenu"></div>';
UserCtrl.prototype._tab2Gallery = `
<input id="tab2Gallery" class="menuInput" type="radio" name="tabs">
<label for="tab2Gallery" class="menuLabel">
<i class="fa icon-gallery"></i>
<span data-i18n="userPage-tab-gallery"> </span></label>
`
UserCtrl.prototype._tab3User = `
<input id="tab3User" class="menuInput" type="radio" name="tabs" checked>
<label for="tab3User" class="menuLabel" ><i class="fa fa-user"></i><span data-i18n="userPage-tab-register"> </span></label>
`
UserCtrl.prototype._tab4Itineraries = `
<input id="tab4Itineraries" class="menuInput" type="radio" name="tabs">
<label for="tab4Itineraries" class="menuLabel"><i class="fa fa-map-signs"></i><span data-i18n="userPage-tab-itinerary" > Itineraris</span></label>
`


// Accordeons menu for each option for the user.
// Register the user identity
UserCtrl.prototype._registerUserForm =
`
<button class="accordion"><i class="fa fa-chevron-right" aria-hidden="true"></i>
 <span data-i18n="userPage-register-title"></span></button>
<div class="accordeonPanel">
  <form id="registerForm" class="normalForm">
  <p data-i18n="userPage-register-form-title"> </p>

    <div class="form-group">
        <input type="text" class="translate inputText" id="inputUser"
               required name="username" data-args="[placeholder]input-user">
    </div>
    <div class="form-group">
        <input type="password" class="inputText translate" id="inputPassword"
               data-args="[placeholder]input-password" required name="password" onchange="registerForm.passwordCheck.pattern = this.value;">
               <br>
         <input type="password" class="inputText translate" id="inputPasswordCheck"
                data-args="[placeholder]input-password-check"  required name="passwordCheck">
    </div>
    <br>
    <div class="explanation" data-i18n="userPage-register-mailcheck"> </div>
    <div class="form-group">
        <input type="email" class="inputText inputEmail translate" id="inputEmail"
        name="email" data-args="[placeholder]input-mail">
    </div>
    <br>
    <button type="submit" class="btn btn-default"><i class="fa fa-check" aria-hidden="true"></i><span data-i18n="userPage-register-submit"> </span></button>
  </form>
</div>
`;

UserCtrl.prototype._loginUserForm =
`
<button class="accordion">  <span data-i18n="userPage-login-title"></span>
</button>
<div class="accordeonPanel">
  <form id="loginForm" class="normalForm" >
  <p data-i18n="userPage-login-form-title"></p>

    <div class="form-group">
        <input type="text" class="inputText translate" id="inputUserLogin" data-args="[placeholder]input-user"
               required name="username">
    </div>
    <div class="form-group">
        <input type="password" class="inputText translate" id="inputPasswordLogin" data-args="[placeholder]input-password-unlock"
          required name="password" >
    </div>
    <br>

    <button type="submit" class="btn btn-default"><i class="fa fa-sign-in" aria-hidden="true"></i>
 <span data-i18n="userPage-login-submit"></span></button>
  </form>
</div>
`;
