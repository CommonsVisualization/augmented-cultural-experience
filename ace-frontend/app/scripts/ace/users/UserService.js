var TOKEN_KEY = "jwtToken";

function UserService() {
}

// This is the user type that the backend will respond to the authentication proces.
UserService._USER_TYPE = "lan-client-req";
UserService._PASSWORD = "";
UserService._TOKEN_KEY = "jwtToken";

UserService.prototype.user = null;
UserService.prototype.self = null;



// JWT functions
UserService.prototype._setJwtToken = function (token) {
    localStorage.setItem(TOKEN_KEY, token);
}

UserService.prototype._removeJwtToken = function () {
    localStorage.removeItem(TOKEN_KEY);
}


var getJwtToken = function () {
        return localStorage.getItem(TOKEN_KEY);
}

var createAuthorizationTokenHeader = function () {
        var token = getJwtToken();
        if (token) {
            return {"Authorization": token};
        } else {
            return {};
        }
}

UserService._AUTH_SERVICE_URL =  configuration.restAuthService;
UserService._USER_SERVICE_URL =  configuration.restUserService;
UserService._IDENTITY_SERVICE_URL =  configuration.restIdentityService;

// AJAX calls
UserService.prototype.doLogin = function(loginData) {
  var self = this;

  if (!loginData) {
    var loginData = {
      username: UserService._USER_TYPE,
      password: UserService._PASSWORD
    };
  }


  return $.ajax({
            url: UserService._AUTH_SERVICE_URL,
            type: "POST",
            data: JSON.stringify(loginData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            // async: false, // This makes that the code stops to wait the answer
            success: function (data, textStatus, jqXHR) {
                self._setJwtToken(data.token);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
        });
}

UserService.prototype.getUserInformation = function (user) {
  var self = this;

  return $.ajax({
            url: UserService._USER_SERVICE_URL ,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            success: function (response) {
              USERINFO = response;
            },
            error: function (jqXHR, textStatus, errorThrown) {
              defaultAjaxError (jqXHR, textStatus, errorThrown);
            }
        });
    }

UserService.prototype.registerUser = function (userToUpdate, newUserData) {
  var self = this;

  var userResource = '/'+userToUpdate;

  return $.ajax({
            url: UserService._USER_SERVICE_URL+userResource ,
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            data:JSON.stringify (newUserData),
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    openModal ("Err 401:" , jqXHR.responseJSON.exception);
                    throw new Error("Err 401: " + errorThrown);
                } else if (jqXHR.status === 409) {
                  openModal (
                    '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ' + $.i18n("userPage-modal-register-failed-title"),
                     $.i18n("userPage-modal-register-failed-body"));
                  throw new Error("Err 409: " + errorThrown);
                }else {
                  defaultAjaxError (jqXHR, textStatus, errorThrown);
                }
            }
        });
}

UserService.prototype.addIdentityToUser = function (user, loginData) {

  var userResource = '/'+user.username;

  var self = this;

  return $.ajax({
            url: UserService._IDENTITY_SERVICE_URL+userResource ,
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            data:JSON.stringify (loginData),
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    openModal (
                      '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ' +$.i18n("userPage-modal-login-failed-title"),
                       $.i18n("userPage-modal-login-failed-body"));
                    throw new Error("Err 401: " + errorThrown);
                } else if (jqXHR.status === 404) {
                  openModal (
                    '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> ' +$.i18n("userPage-modal-login-badUsername-title"),
                     $.i18n("userPage-modal-login-badUsername-body"));
                  throw new Error("Err 404: " + errorThrown);
                }else {
                  defaultAjaxError (jqXHR, textStatus, errorThrown);
                }
            }
    });
}
