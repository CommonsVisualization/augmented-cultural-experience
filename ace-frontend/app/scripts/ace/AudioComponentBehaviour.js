function AudioComponentBehaviour($element, audioService, audioResource) {
    this._$element = $element;
    this._audioService = audioService;
    if (typeof(audioResource) == "string") {
        this._audioResource = function() {
            return audioResource;
        }
    } else {
        this._audioResource = audioResource;
    }
}

AudioComponentBehaviour.prototype._$element = null;
AudioComponentBehaviour.prototype._audioService = null;
AudioComponentBehaviour.prototype._audioResource = null;

AudioComponentBehaviour.prototype._showAudioIsPlaying = null;
AudioComponentBehaviour.prototype._showAudioIsPaused =  null;


AudioComponentBehaviour.prototype.attach = function() {
    var self = this;
    this._$element
            .unbind("click")
            .click(function() {
                if (self._audioService.isPlaying() == true) {
                    self._audioService.stop();
                } else {
                    self._audioService.resourcePath = self._audioResource();
                    self._audioService.play();
                }
            });
    this._audioService.addOnStartListener(function() {
        self._showAudioIsPlaying();
    });
    this._audioService.addOnStopListener(function() {
        self._showAudioIsPaused();
    });
    if (this._audioService.isPlaying() == true) {
        this._showAudioIsPlaying();
    } else {
        this._showAudioIsPaused();
    }
}

// ***************************************************************************

function TextAudioComponentBehaviour($element, audioService, audioResource) {
    AudioComponentBehaviour.call(this, $element, audioService, audioResource);
}

TextAudioComponentBehaviour.prototype = new AudioComponentBehaviour();

TextAudioComponentBehaviour.prototype._showAudioIsPlaying = function() {
    this._$element.html($("<span/>")
      .text($.i18n("audiocomponent-stop"))
      .attr('data-i18n', "audiocomponent-stop" ));
}

TextAudioComponentBehaviour.prototype._showAudioIsPaused =  function() {
    this._$element.html($("<span/>")
      .text($.i18n("audiocomponent-play"))
      .attr('data-i18n', "audiocomponent-play" ));
}

// ***************************************************************************

function ImageAudioComponentBehaviour($element, audioService, audioResource) {
    AudioComponentBehaviour.call(this, $element, audioService, audioResource);
}

ImageAudioComponentBehaviour.prototype = new AudioComponentBehaviour();

ImageAudioComponentBehaviour.prototype._findImageElement = function() {
    var $img = null;
    if (this._$element[0].nodeName.toLowerCase() == "img") {
        $img = this;
    } else {
        $img = this._$element.find("img");
    }
    return $img;
}

ImageAudioComponentBehaviour.prototype._showAudioIsPlaying = function() {
    var $img = this._findImageElement();
    $img.attr("src", "images/icons/pause.svg");
}

ImageAudioComponentBehaviour.prototype._showAudioIsPaused =  function() {
    var $img = this._findImageElement();
    $img.attr("src", "images/icons/play.svg");
}
