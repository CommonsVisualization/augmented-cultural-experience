function UserMark(map) {
    this._map = map;
    this._layerGroup = new L.LayerGroup();
    this._latLngHistory = [];
    this._userPositionLayer= new L.Circle(new L.LatLng(0,0), 0, {
                                    opacity : 0.25,
                                    fillOpacity : 0.1,
                                    clickable : false
                                });              
                                
    this._map.addLayer(this._layerGroup);
}

UserMark.MAX_NUMBER_OF_FOOTPRINTS = 5;
UserMark.MIN_DISTANCE_BETWEEN_FOOTPRINTS = 1;

UserMark.prototype._map = null;
UserMark.prototype._layerGroup = null;
UserMark.prototype._userPositionLayer = null;
UserMark.prototype._latLngHistory;

UserMark.prototype.updatePosition = function(latlng, accuracy, heading) {
    // Update current mark
    if (this._userPositionLayer.getRadius() === 0) {
        this._layerGroup.addLayer(this._userPositionLayer);                            
    }
    this._userPositionLayer.setLatLng(latlng);
    this._userPositionLayer.setRadius(accuracy);
    // Check if a new footprint is needed
    var lastCoordIdx = this._latLngHistory.length -1;
    var previousCoords = (lastCoordIdx === -1) ? null : this._latLngHistory[lastCoordIdx];
    if ((lastCoordIdx === -1) || 
        (Math.abs(previousCoords.distanceTo(latlng)) > UserMark.MIN_DISTANCE_BETWEEN_FOOTPRINTS)) {
        latlng._footprintLayer = new L.CircleMarker(latlng, {
            stroke : true,
            radius : 2,
            fill : true,
            clickable : false
        });
        this._layerGroup.addLayer(latlng._footprintLayer);
        this._latLngHistory.push(latlng);
    }
    // Remove oldest footprint if necessary
    if (this._latLngHistory.length > UserMark.MAX_NUMBER_OF_FOOTPRINTS) {
        var coordToBeRemoved = this._latLngHistory[0];
        this._latLngHistory = this._latLngHistory.slice(1);
        this._layerGroup.removeLayer(coordToBeRemoved._footprintLayer);
    }
};

UserMark.prototype.clear = function() {
    this._layerGroup.clearLayers();
    this._latLngHistory = [];
    this._userPositionLayer.setRadius(0);    
};