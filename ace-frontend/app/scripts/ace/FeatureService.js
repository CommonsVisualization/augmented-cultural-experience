function FeatureService() {
}

FeatureService.getInstance = function() {
    if (navigator.userAgent == 'phonegap') {
        var databaseService = new LocalDatabaseService();
        var service = new FeatureServiceImplLocalDatabase(databaseService);
    } else {
        var service = new FeatureServiceImplAJAX();
    }
    return service;
}

FeatureService._FEATURES_MODIFIED_EVENT_NAME = "onChangeFeatures";
FeatureService._FEATURE_ACTIVATED_EVENT_NAME = "onActivatedFeature";

FeatureService._MAX_FEATURES = 40;

FeatureService.prototype._features = [];
FeatureService.prototype._currentFeatureIndex = 0;


FeatureService.prototype._orderFeaturesByDistance = function(features, latLng) {
    features.sort(function(f1, f2) {
        var latLng1 = new L.LatLng(f1.lat, f1.lon);
        var latLng2 = new L.LatLng(f2.lat, f2.lon);
        var d1 = Math.abs(latLng.distanceTo(latLng1));
        var d2 = Math.abs(latLng.distanceTo(latLng2));
        f1.distance = parseInt(d1*100)/100;
        f2.distance = parseInt(d2*100)/100;
        return (-1) * parseInt(d2*100-d1*100);
    });
    return features;
}


FeatureService.prototype.setActiveFeature = function(feature) {
    var idx;
    for (idx=0; idx < this._features.length; idx++) {
        if (this._features[idx] == feature) {
            break;
        }
    }
    this._currentFeatureIndex = idx;
    $(this).trigger(FeatureService._FEATURE_ACTIVATED_EVENT_NAME);
}

FeatureService.prototype.getActiveFeature = function() {
    return this._features[this._currentFeatureIndex];
}

FeatureService.prototype.getActiveFeatureOrdinal = function() {
    return this._currentFeatureIndex;
}

FeatureService.prototype.getFeatures = function() {
    return this._features;
}

FeatureService.prototype.setFeatures = function(features) {
    this._features = features;
    $(this).trigger(FeatureService._FEATURES_MODIFIED_EVENT_NAME);
}

FeatureService.prototype.addFeature = function(feature) {
    this._features.add(feature);
    $(this).trigger(FeatureService._FEATURES_MODIFIED_EVENT_NAME);
}


FeatureService.prototype.clearFeatures = function() {
    this._features = [];
    $(this).trigger(FeatureService._FEATURES_MODIFIED_EVENT_NAME);
}


FeatureService.prototype.rollFeature = function(direction, isAbsolute) {
    if (isAbsolute == true) {
        direction = direction - this._currentFeatureIndex;
    }
    this._currentFeatureIndex = (this._currentFeatureIndex + direction) % this._features.length;

    $(this).trigger(FeatureService._FEATURE_ACTIVATED_EVENT_NAME);
}

FeatureService.prototype._normalizeFeatures = function(features) {
    for (var idx=0; idx < features.length; idx++) {
        var feature = features[idx];
        var label = feature.taxon.genus + " " + feature.taxon.speciesName;
        if (feature.taxon.popularNameCA) {
            label = label + " (" + feature.taxon.popularNameCA + ")";
        }
        feature.label = label;
        feature.audioResource = !feature.taxon.wikipediaResource.ca  ?
                                null :
                                feature.taxon.wikipediaResource.ca
                                .replace(/[.:/]/g, '_');

        feature.detailsAvalaible = true;
    }
    return features;
}


function FeatureServiceImplLocalDatabase(databaseService) {
    FeatureService.call(this);
    this._databaseService = databaseService;
}

FeatureServiceImplLocalDatabase.prototype = new FeatureService();

FeatureServiceImplLocalDatabase.prototype._databaseService = null;

FeatureServiceImplLocalDatabase.prototype._dbResultToFeatures = function(results, centerLatLng, maxDistance) {
    maxDistance = maxDistance || 1E10;
    var foundFeatures = [];
    for (var idx=0; idx < results.rows.length; idx++) {
        var currentSpecimen = {
            code : results.rows.item(idx).code,
            lat : results.rows.item(idx).lat,
            lon : results.rows.item(idx).lon,
            taxon : $.parseJSON(results.rows.item(idx).description)
        }
        var featureLatLng = new L.LatLng(currentSpecimen.lat, currentSpecimen.lon);
        if (featureLatLng.distanceTo(centerLatLng) <= maxDistance) {
            foundFeatures.push(currentSpecimen);
        }
    }
    foundFeatures = this._normalizeFeatures(foundFeatures);
    foundFeatures = this._orderFeaturesByDistance(foundFeatures, centerLatLng);
    return foundFeatures;
}

FeatureServiceImplLocalDatabase.prototype.searchFeaturesByPattern = function(prefix, centerLatLng, fn) {
    var self = this;
    var sql = 'select i.codi_ind, i.lat, i.lon, t.description '
            + 'from inventari i join taxa t on i.codi_esp = t.codi_esp '
            + 'where description like ? '
            + 'order by (i.lat - ?)*(i.lat - ?) + (i.lon - ?)*(i.lon - ?) '
            + 'limit 10';
    this._databaseService.doQuery(sql, ['%' + prefix + '%', centerLatLng.lat, centerLatLng.lat, centerLatLng.lng, centerLatLng.lng], function(tx, results) {
        var foundFeatures = self._dbResultToFeatures(results, centerLatLng);
        if (fn) fn(foundFeatures);
    });
}

FeatureServiceImplLocalDatabase.prototype.searchFeaturesByLatLng = function(centerLatLng, maxDistance, fn) {
    var self = this;
    var sql = 'select i.codi_ind as code, i.lat as lat, i.lon as lon, t.description as description '
            + 'from inventari i join taxa t on i.codi_esp = t.codi_esp '
            + 'order by (i.lat - ?)*(i.lat - ?) + (i.lon - ?)*(i.lon - ?) '
            + 'limit 10';
    this._databaseService.doQuery(sql, [centerLatLng.lat, centerLatLng.lat, centerLatLng.lng, centerLatLng.lng], function(tx, results) {
        var foundFeatures = self._dbResultToFeatures(results, centerLatLng, maxDistance);
        if (fn) fn(foundFeatures);
    })
}


function FeatureServiceImplAJAX() {
    FeatureService.call(this);
}

FeatureServiceImplAJAX._PATTERN_SEARCH_WEB_SERVICE_URL =  configuration.restSearchService;
FeatureServiceImplAJAX._LATLON_SEARCH_WEB_SERVICE_URL =  configuration.restNearService;
FeatureServiceImplAJAX._BBOX_SEARCH_WEB_SERVICE_URL =  configuration.restBBoxService +
                                                       '?minLon={minLon}&minLat={minLat}&maxLon={maxLon}&maxLat={maxLat}';
FeatureServiceImplAJAX._WIKIPEDIA_RESUME_WEB_SERVICE_URL =  configuration.restWikipediaService +
                                                            '?wikipediaLink={wikipediaLink}';

FeatureServiceImplAJAX.prototype = new FeatureService();

FeatureServiceImplAJAX.prototype.searchFeaturesByPattern = function(prefix, centerLatLng, fn) {
    return this._invokeWebService(
                   FeatureServiceImplAJAX._PATTERN_SEARCH_WEB_SERVICE_URL,
                   {"pattern" : prefix,
                    "lat" : centerLatLng.lat,
                    "lon" : centerLatLng.lng},
                   centerLatLng, fn);
}

FeatureServiceImplAJAX.prototype.searchFeaturesByLatLng = function(centerLatLng, maxDistance, fn) {
    return this._invokeWebService(
                   FeatureServiceImplAJAX._LATLON_SEARCH_WEB_SERVICE_URL,
                   {"lat" : centerLatLng.lat,
                    "lon" : centerLatLng.lng,
                    "maxDistance" : maxDistance},
                   centerLatLng, fn);
}

FeatureServiceImplAJAX.prototype._invokeWebService= function(url, params, centerLatLon, fn) {
    var self = this;
    $(document).trigger("ajaxStart");
    var promise = $.get(url, params)
                   .done(function(foundFeatures) {
                        foundFeatures = self._normalizeFeatures(foundFeatures);
                        foundFeatures = self._orderFeaturesByDistance(foundFeatures, centerLatLon);
                        if (fn) fn(foundFeatures);
                   })
                   .fail(function(exc) {
                        throw exc;
                   });
    return promise;
};

// Request to REST server of all speciments inside an area
FeatureServiceImplAJAX.prototype.searchFeaturesByBBox= function(minLat, minLon, maxLat, maxLon) {
    var url = FeatureServiceImplAJAX._BBOX_SEARCH_WEB_SERVICE_URL
                .replace('{minLon}', minLon)
                .replace('{minLat}', minLat)
                .replace('{maxLon}', maxLon)
                .replace('{maxLat}', maxLat);
    var promise = $.getJSON(url);
    return promise;
};

FeatureServiceImplAJAX.prototype.getWikipediaResume= function(taxonFeature) {
    var url = FeatureServiceImplAJAX._WIKIPEDIA_RESUME_WEB_SERVICE_URL
              .replace('{wikipediaLink}', taxonFeature.wikipediaResource.ca);
    var promise = $.getJSON(url);
    return promise;
};
