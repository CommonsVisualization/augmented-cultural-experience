/** If db doesn't exist creates one for you! **/


function LocalDatabaseService() {
    console.log('Opening a database full of candy.');
    this._db = window.openDatabase('ace', '1.0', 'Specimen data.', 10*1024*1024, function() {
        console.log('I didn\'t found the database so I created one for your delight.');
    });
}

LocalDatabaseService.DATABASE_READY_EVENT_NAME  = "onDatabaseReady";
LocalDatabaseService.DATABASE_ERROR_EVENT_NAME  = "onDatabaseError";

LocalDatabaseService._EXPECTED_INV_RECORDS = 12645;

LocalDatabaseService.prototype._db = null;

LocalDatabaseService.prototype._defaultSuccessHandler = function(tx, results) {
    //console.log('Umh.. yeah, everything looks fine: ' + results.length + ' rows retreived.')
}

LocalDatabaseService.prototype._sqlErrorHandler = function(tx, error, sql) {
    console.log('Oh my god, something went horribly wrong. (' + error.code + ') ' + error.message + " (" + sql + ").");
    $(this).trigger(LocalDatabaseService.DATABASE_ERROR_EVENT_NAME);
}

LocalDatabaseService.prototype._transactionErrorHandler = function(error, sql) {
    console.log('Oh my god, something went horribly wrong. (' + error.code + ') ' + error.message + " (" + sql + ").");
    $(this).trigger(LocalDatabaseService.DATABASE_ERROR_EVENT_NAME);
}

LocalDatabaseService.prototype.doQuery = function(sql, data, success, errorHandler) {
    data = data || [];
    success = success || this._defaultSuccessHandler;
    errorHandler = errorHandler || this._sqlErrorHandler;

    this._db.transaction(function(tx) {
        console.log('Executing ' + sql + '.');
        tx.executeSql(sql, data, success, function(tx, error) { errorHandler(tx, error, sql); });
    });
}

LocalDatabaseService.prototype._createTaxaTable = function(fn) {
    var self = this;
    var sql = 'create table if not exists taxa (                '+
              '   codi_esp char(10) primary key,  '+
              '   description varchar(500)        '+
              ');'
    this._db.transaction(function(tx) {
        console.log('Creating taxa table.');
        tx.executeSql(sql, [], self._defaultSuccessHandler, function(tx, error) { self._sqlErrorHandler(tx, error, sql); });
    }, function(error) { self._transactionErrorHandler(error, sql); }, fn);
}

LocalDatabaseService.prototype._createInventariTable = function(fn) {
    var self = this;
    var sql = 'create table if not exists inventari (           '+
              '   codi_ind integer primary key,   '+
              '   codi_esp char(10),              '+
              '   lat real,                       '+
              '   lon real,                        '+
              'foreign key(codi_esp) references taxa(codi_esp))'
    this._db.transaction(function(tx) {
        console.log('Creating inventari table.');
        tx.executeSql(sql, [], self._defaultSuccessHandler, function(tx, error) { self._sqlErrorHandler(tx, error, sql); });
    }, function(error) { self._transactionErrorHandler(error, sql); }, fn);
}


LocalDatabaseService.prototype._insertInventariData = function(fn) {
    var self = this;
    this._db.transaction(function(tx) {
        console.log('Inserting inventari data.');
        ejecutarInsertsInventari(tx, self._defaultSuccessHandler, function(tx, error, sql) { self._sqlErrorHandler(tx, error, sql); });
    }, this._transactionErrorHandler, fn);
}

LocalDatabaseService.prototype._insertTaxaData = function(fn) {
    var self = this;
    this._db.transaction(function(tx) {
        console.log('Inserting taxa data.');
        ejecutarInserts(tx, self._defaultSuccessHandler, function(tx, error, sql) { self._sqlErrorHandler(tx, error, sql); });
    }, this._transactionErrorHandler, fn);
}

LocalDatabaseService.prototype._fillDatabase = function(fn) {
    var self = this;
    this._db.transaction(function(tx) {
        console.log('Checking existance of data.');
        var sql = 'select count(*) as rowCount from inventari';
        tx.executeSql(sql, [], function(tx, results) {
            var count = parseInt(results.rows.item(0).rowCount);
            if (count == LocalDatabaseService._EXPECTED_INV_RECORDS) {
                console.log('Database is full of stars! Nothing to do.');
                $(self).trigger(LocalDatabaseService.DATABASE_READY_EVENT_NAME);
            } else {
                console.log('Database is empty. Filling it with stars.');
                self._insertTaxaData(function() {
                    self._insertInventariData(function() {
                        $(self).trigger(LocalDatabaseService.DATABASE_READY_EVENT_NAME);
                    })
                })
            }
        }, function(tx, error) { self._sqlErrorHandler(tx, error, sql); })
    }, this._transactionErrorHandler, fn);                 x
}

LocalDatabaseService.prototype.initialize = function() {
    var self = this;

    self._createTaxaTable(function() {
       self._createInventariTable(function() {
           self._fillDatabase(function() {
              console.log('Wow! database initialized. Awesome.')
           });
       }) ;
    });


}
