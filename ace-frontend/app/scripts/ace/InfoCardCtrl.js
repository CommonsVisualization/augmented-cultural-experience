//globalCardCtrl used for internationalization
var globalCardCtrl;

/**  Controller that creates the page with the info of a specimen or itinerary**/
function createInfoCardCtrl(title, $element, audioService, featureList, activeIndex, featureService, imageCtrl) {
  // ADAPTATION, if is name (general itieraries) or title, in user itineraries
    if (featureList[0].name || featureList[0].title) {
      globalCardCtrl = new ItineraryInfoCardCtrl(title, $element, audioService, featureList, activeIndex, imageCtrl);
    } else {
      globalCardCtrl = new TaxonInfoCardCtrl(title, $element, audioService, featureList, activeIndex, featureService, imageCtrl);
    }
    return globalCardCtrl;
}

// ***************************************************************************

function InfoCardCtrl(title, $element, audioService, featureList, activeIndex, featureService, imageCtrl) {
    this._title = title;
    this._$element = $element;
    this._featureList = featureList;
    this._activeIndex = activeIndex || 0;
    this._audioService = audioService;
    this._featureService = featureService;
    this._imageCtrl = imageCtrl;

    if (this._featureList != undefined){ // To avoid a bug
      if (this._featureList[0].name || this._featureList[0].title) {
        this._isItineraryFeature = true;
      } else {
        this._isItineraryFeature = false;
      }
    }
}

// NOT USED?
InfoCardCtrl.create = function(title, $element, audioService, featureList, activeIndex) {
    if (featureList[0].name) {
        return new ItineraryInfoCardCtrl(title, $element, audioService, featureList, activeIndex);
    } else {
        return new TaxonInfoCardCtrl(title, $element, audioService, featureList, activeIndex);
    }
};

InfoCardCtrl._MEDIA_WEBSERVICE_URL = (navigator.userAgent == 'phonegap') ? configuration.mediaServerPhonegap : configuration.mediaServer;

/** TODO: Refactor this assign in order to provide it as a constructor parameter. */
InfoCardCtrl.prototype._iui = iui;
InfoCardCtrl.prototype._title = null;
InfoCardCtrl.prototype._$element = null;
InfoCardCtrl.prototype._audioService = null;
InfoCardCtrl.prototype._featureList = null;
InfoCardCtrl.prototype._activeIndex = null;
InfoCardCtrl.prototype._featureService = null;
InfoCardCtrl.prototype._imageCtrl = null;

InfoCardCtrl.prototype._getCardName = null;
InfoCardCtrl.prototype._getImageList = null;
InfoCardCtrl.prototype._getAudioResource = null;
InfoCardCtrl.prototype._getTextAsync = null;

InfoCardCtrl.prototype._isItineraryFeature = null;


InfoCardCtrl.prototype.show = function() {
    this._initContainer();
    this._showActiveFeature();
};

InfoCardCtrl.prototype._initContainer = function() {
    var self = this;
    if (self._featureList.length > 1) {
        this._iui.setRightButtonBehaviour('toolbar-Rbutton-next', function() {
            self._activeIndex = (self._activeIndex + 1) % (self._featureList.length);
            self._showActiveFeature();
        });
    }
};

InfoCardCtrl.prototype._showActiveFeature = function() {
    var self = this;

    var numeration = "";
    if (this._featureList.length > 0) {
        numeration = this._activeIndex+1+'/'+this._featureList.length
    }
    this._$element.empty().attr('data-title', 'toolbar-title-data,'+numeration);
    i18nAddDataArgsToElement($("#pageTitle"),'toolbar-title-data,'+numeration)

    this._$element.append($('<li/>').text(this._getCardName()));

    // Add audio
    var audioResource = this._getAudioResource();
    if (audioResource) {
        var $cmdAudioControl =
                $('<a/>')
                    .addClass('whiteButton')
                    .attr('type', 'button')
                    .attr('target', '_self')
        this._$element.append($cmdAudioControl);
        var audioBehaviour = new TextAudioComponentBehaviour(
                                $cmdAudioControl, this._audioService, audioResource);
        audioBehaviour.attach();
   }

    return this._$element;
};

// Appends a mini gallery on the info card.
InfoCardCtrl.prototype._addMiniGallery = function(feature) {
  var self = this;
  // Get the type of card (taxon or itinerary)
  var featureCode = (feature.id) ? feature.id : feature.taxon.code;
  var featureType = (feature.id) ? 'itinerary' : 'taxon';
  // Add mini gallery to upload fotos
  var galleryId = 'gal'+makeid();
  var title = "<span data-i18n='infocard-usergallery-title'>"+$.i18n("infocard-usergallery-title")+"</span>"; // Set little title to mini gallery
  var minigal = new this._imageCtrl.MiniGallery( this._imageCtrl,galleryId, title,
          function (photos, photoUploaded){ //Callback when image is added
              // Call the ajax to associate the image uploaded to a specific feature
              self._imageCtrl._imageService.asociateToFeature (featureType, featureCode, photoUploaded)
            });
  // Get all images from the feature and append it to miniGallery
  // if is a feture card from an specimen the images will be added to his **Taxon**
  self._imageCtrl._imageService.getImagesFromFeature(featureType, featureCode).success(
    function (data){
      if (data) minigal.appendImageList(data);
    }
  )
  minigal.appendMiniGallery(this._$element);

}


InfoCardCtrl.prototype._addWikipediaImages = function(images, $galleryElement, label) {
    var imageCount = 0;
    if ($galleryElement.files === undefined) {
        $galleryElement.files = [];
    }
    for (var idx=0; idx < images.length; idx++) {
        var currentImageName = images[idx].fitxer;
        var fileName = currentImageName.substring(currentImageName.lastIndexOf(':'));
        if ($.inArray(fileName, $galleryElement.files) == -1) {
            $galleryElement.files.push(fileName);
            var currentImageThumbName = this._wikipediaImagesParseUrl(images[idx].thumbnail);
            this._addImages($galleryElement, currentImageName, currentImageThumbName);
            imageCount = imageCount + 1;
        }
    }
    return imageCount;
};

// Function that modify the url of the wikimedia image to get always a more little size to save bandwth
// Wikimedia gives you the posibility to get resized images via an url parameter
// This function get this url parameter and modify it to 200 px
InfoCardCtrl.prototype._wikipediaImagesParseUrl = function (url) {
  var init = url.lastIndexOf("/")+1; //Get last block of the url
  var fin = url.substr(init).indexOf("-"); // Get XXXpx param
  var pxString = url.substr(init, fin );

  // Check if is in format XXXpx
  if (pxString.endsWith("px") &&
    /^\+?(0|[1-9]\d*)$/.test( pxString.substring(0, pxString.indexOf("px")) ) ) {
      return url.replace(pxString, "200px");
  }
  return url;
}

InfoCardCtrl.prototype._addImages = function($galleryElement, imageUrl, thumbUrl) {
    $galleryElement
        .append($('<a/>')
            .attr('href', imageUrl)
            .attr('target', '_blank')
            .append($('<img/>')
                        .attr('src', thumbUrl)
                        .attr('height', 60) ));
};


// ***************************************************************************

function TaxonInfoCardCtrl(title, $element, audioService, featureList, activeIndex, featureService, imageCtrl) {
    InfoCardCtrl.call(this, title, $element, audioService, featureList, activeIndex, featureService, imageCtrl);
}



TaxonInfoCardCtrl.prototype = new InfoCardCtrl();

TaxonInfoCardCtrl.prototype._showActiveFeature = function() {
    var self = this;
    InfoCardCtrl.prototype._showActiveFeature.call(this);
    // Add technical data
    var taxon = this._featureList[this._activeIndex].taxon;
    var $info = $('<li/>')
            .addClass('group')
            .text($.i18n("infocard-relevantInfo"))
            .attr('data-i18n', "infocard-relevantInfo" );

    this._$element.children().first().after($info);

    // Following functions added to help with internationalization
    var locale = getCurrentLocale();
    var getPopularName = function () {
      if (locale == 'ca' && taxon.popularNameCA) return taxon.popularNameCA;
      else if (locale == 'en' && taxon.popularNameEN) return taxon.popularNameEN;
      else if (locale == 'es' && taxon.popularNameES) return taxon.popularNameES;
      else return null;
    };
    var getDescription = function (){
      if (locale == 'ca' && taxon.descriptionResources.ca) return taxon.descriptionResources.ca;
      else if (locale == 'en' && taxon.descriptionResources.en) return taxon.descriptionResources.en;
      else if (locale == 'es' && taxon.descriptionResources.es) return taxon.descriptionResources.es;
      else return null;
    };
    var getWikimediaResources = function (){
      if (locale == 'ca' && taxon.wikipediaResource.ca) return taxon.wikipediaResource.ca;
      else if (locale == 'en' && taxon.wikipediaResource.en) return taxon.wikipediaResource.en;
      else if (locale == 'es' && taxon.wikipediaResource.es) return taxon.wikipediaResource.es;
      else return null;
    };

    if (taxon.family) {
        $info.after($('<li/>')
        .text($.i18n("infocard-family", taxon.family))
          .attr('data-args', "infocard-family,"+ taxon.family)
          .addClass('translate')
      );
    }
    if (taxon.worldLocation){
        $info.after($('<li/>')
        .text($.i18n("infocard-origin", taxon.worldLocation))
          .attr('data-args', "infocard-origin,"+ taxon.worldLocation)
          .addClass('translate')
      );
    }
    var popuName = getPopularName();
    if (popuName) {
        $info.after($('<li/>')
        .text($.i18n("infocard-popularName", popuName))
          .attr('data-args', "infocard-popularName,"+ popuName)
          .addClass('translate')
      );
    }
    // Add description
    var taxon = this._featureList[this._activeIndex].taxon;
    var descript = getDescription();
    if (descript) {
        var $description = $('<li/>').text(descript)
                                     .appendTo(this._$element);
        $('<a/>')
            .addClass('whiteButton')
            .attr('href', getWikimediaResources())
            .attr('target', '_blank')
            .text($.i18n("infocard-wikipedia"))
            .attr('data-i18n', "infocard-wikipedia" )
            .appendTo($description);
        if (taxon.media.length > 0) {
            var $galleryElement = $('<div/>')
                                    .addClass('gallery');
            self._addWikipediaImages(taxon.media, $galleryElement);
            self._$element
                .append($('<li/>')
                    .append($galleryElement));
        }
    }

    // Add Minigallery
    this._addMiniGallery(this._featureList[this._activeIndex]);
};

TaxonInfoCardCtrl.prototype._getCardName = function() {
    var cardName = this._featureList[this._activeIndex].label;
    return cardName;
};

TaxonInfoCardCtrl.prototype._getImageList = function() {
    return this._featureList[this._activeIndex].taxon.media;
};

TaxonInfoCardCtrl.prototype._getAudioResource = function() {
  var locale = getCurrentLocale();
  if (this._featureList[this._activeIndex].taxon.audioFileResources[locale]) {
    var audioResource = this._featureList[this._activeIndex].taxon.audioFileResources[locale];
    return audioResource;
  }
};

TaxonInfoCardCtrl.prototype.translate = function (){
  this._showActiveFeature();
}


// ***************************************************************************

function ItineraryInfoCardCtrl(title, $element, audioService, featureList, activeIndex, imageCtrl) {
    // InfoCardCtrl.call(this, title, $element, audioService, featureList, activeIndex, imageCtrl);
    InfoCardCtrl.call(this, title, $element, audioService, featureList, activeIndex, null,  imageCtrl);

}

ItineraryInfoCardCtrl.prototype = new InfoCardCtrl();
ItineraryInfoCardCtrl.prototype._itineraryService = null;


ItineraryInfoCardCtrl.prototype._showActiveFeature = function() {
    var self = this;
    InfoCardCtrl.prototype._showActiveFeature.call(this);
    var description = this._featureList[this._activeIndex].description;
    $('<li/>').text(description).appendTo(this._$element);


    // ADAPTATION
    // If is general itinerary don't get the option to minigallery
    if (this._featureList[this._activeIndex].title){
      // Add Minigallery
      this._addMiniGallery(this._featureList[this._activeIndex]);
    }


};

ItineraryInfoCardCtrl.prototype._getCardName = function() {
    var cardName = this._featureList[this._activeIndex].label;
    return cardName;
};

ItineraryInfoCardCtrl.prototype._getImageList = function() {
    return [];
};

ItineraryInfoCardCtrl.prototype._getAudioResource = function() {
  if (getCurrentLocale() == 'ca') {
    var audioResource = this._featureList[this._activeIndex].audioFile;
    return audioResource;
  }
}

// Function used for dynamic translation
ItineraryInfoCardCtrl.prototype.translate = function (){
  // Only translate the general itinerary
  if (!this._featureList[this._activeIndex].title && this._itineraryService){
    var self = this;
    this._itineraryService.loadItinerary("itiaud", function (iti) {
      self._featureList = iti.features;
      self._showActiveFeature();
    } );
  }
}

// Used to add dinamically to the object an itinerary service for dinamic translation
// To get each translated itinerary you need to do a single GET, so it needs to acces to itineraryService
ItineraryInfoCardCtrl.prototype.setItineraryService = function (itiService){
  this._itineraryService = itiService;
}
