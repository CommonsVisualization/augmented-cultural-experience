function AudioService() {
}

AudioService.createInstance = function() {
    var service = (navigator.userAgent == 'phonegap') ?
                  new AudioServiceImplPhonegap() : new AudioServiceImplHTML5();
    return service;
};

AudioService._PLAY_EVENT_NAME = "AudioServiceOnPlay";
AudioService._STOP_EVENT_NAME = "AudioServiceOnStop";


AudioService.prototype.resourcePath = null;

AudioService.prototype.play = null;
AudioService.prototype.stop = null;
AudioService.prototype.isPlaying = null;

AudioService.prototype.addOnStartListener = function(listener) {
    $(this).on(AudioService._PLAY_EVENT_NAME, listener);               
}

AudioService.prototype.addOnStopListener = function(listener) {
    $(this).on(AudioService._STOP_EVENT_NAME, listener);               
}


function AudioServiceImplPhonegap() {
}

AudioServiceImplPhonegap._AUDIO_WEBSERVICE_URL = "http://localhost:9999/";

AudioServiceImplPhonegap.prototype = new AudioService();
AudioServiceImplPhonegap.prototype.constructor = AudioServiceImplPhonegap;

AudioServiceImplPhonegap.prototype._mediaPlayer = null;

AudioServiceImplPhonegap.prototype.play = function(resourcePath, onEndedFn) {
    resourcePath = resourcePath || this.resourcePath;
    var self = this;
    if (resourcePath) {
        if (this.isPlaying() == true) {
            console.debug('There was another active song.');
            this.stop();
        }
        this.resourcePath = resourcePath;
    }
    if ((resourcePath != null) && (this._mediaPlayer == null)) {
        this._mediaPlayer = new Media(AudioServiceImplPhonegap._AUDIO_WEBSERVICE_URL + resourcePath
        , function() {
            console.debug('All played.');
            self.stop();
        }, function(error) {
            console.warn('Something went wrong while playing ' + resourcePath + ': ' + error + '.');
            self.stop();
        });
        $(this).on(AudioService._STOP_EVENT_NAME, function(evt) {
            console.debug('Finishing the job.')
             $(this).unbind(evt);
             if (onEndedFn) onEndedFn();
        });
        console.debug('Rocknroll! ' + resourcePath);
        this._mediaPlayer.play();
        $(this).trigger(AudioService._PLAY_EVENT_NAME);
    }
}

AudioServiceImplPhonegap.prototype.stop = function() {
    if (this._mediaPlayer != null) {
        console.debug('Stopping mediaplayer!');
        this._mediaPlayer.stop();
        this._mediaPlayer = null;
        $(this).trigger(AudioService._STOP_EVENT_NAME);
    }
}

AudioServiceImplPhonegap.prototype.isPlaying = function() {
    return this._mediaPlayer != null;
}

function AudioServiceImplHTML5() {
}

AudioServiceImplHTML5._AUDIO_WEBSERVICE_URL = configuration.mediaServer;

            
AudioServiceImplHTML5.prototype = new AudioService();

AudioServiceImplHTML5.prototype._$audioElement = null;


AudioServiceImplHTML5.prototype._prepare = function() {
    var self = this;
    this._$audioElement = $("<audio/>")
        //.attr("volume", 1.0)
        .data("audioResource", this.resourcePath)
        .on("doStart", function() {
            var resourcePath = self.resourcePath;
            var len = resourcePath.length;
            if ((len < "?.mp3") || (resourcePath.charAt(len - ".mp3".length) != ".")) {
                resourcePath += ".mp3";
            }
            resourcePath = AudioServiceImplHTML5._AUDIO_WEBSERVICE_URL + resourcePath;
            var filePathMp3 = resourcePath.substring(0, resourcePath.lastIndexOf(".")) + ".mp3";
            var filePathOgg = resourcePath.substring(0, resourcePath.lastIndexOf(".")) + ".ogg";
            $(this).children("source").detach();
            $(this).append($("<source/>")
                .attr("src", filePathMp3)
                .attr("type", "audio/mp3"))
            $(this).append($("<source/>")
                .attr("src", filePathOgg)
                .attr("type", "audio/ogg"))
            this.play();
        })
        .on("ended", function() {
            this.pause();
            $(self).trigger(AudioService._STOP_EVENT_NAME);
            if (self._$audioElement != null) {
                self._$audioElement.remove();
                self._$audioElement = null;
            }
        })
        .appendTo(document.body)
        .hide();        
}

AudioServiceImplHTML5.prototype.play = function(resourcePath, onEndedFn) {
    if (resourcePath) {
        if (this.isPlaying() == true) {
            this.stop();
        }
        this.resourcePath = resourcePath;
    }
    if ((this.resourcePath != null) && (this._$audioElement == null)) {
        this._prepare();
        this._$audioElement.trigger("doStart");
        $(this).on(AudioService._STOP_EVENT_NAME, function(evt) {
             $(this).unbind(evt);
             if (onEndedFn) onEndedFn();
        });
        $(this).trigger(AudioService._PLAY_EVENT_NAME);
    }
}

AudioServiceImplHTML5.prototype.stop = function() {
    if (this._$audioElement != null) {
        if (this._$audioElement[0].ended != true) {
            this._$audioElement.trigger("ended");
        }
        $(this).trigger(AudioService._STOP_EVENT_NAME);
        if (this._$audioElement != null) {
            this._$audioElement.detach();
            this._$audioElement = null;
        }
    }
}

AudioServiceImplHTML5.prototype.isPlaying = function() {
    return this._$audioElement != null;
}

