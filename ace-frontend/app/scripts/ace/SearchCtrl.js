function SearchCtrl(iui, map, featureService) {
    var self = this;
    this._iui = iui;
    this._map = map;
    this._service = featureService;
    this._$searchDialog = $("#searchDialog");
    this._$textBox = this._$searchDialog.find("input");
    this._$textBox.off('keyup').on('keyup', function(evt) {
       if (evt.keyCode === 13 /* Enter */) {
           self.executeSearch();
       }
       evt.preventDefault();
    });
    this._initialize();
}

SearchCtrl.prototype._iui = null;
SearchCtrl.prototype._map = null;
SearchCtrl.prototype._$searchDialog = null;
SearchCtrl.prototype._$textBox = null;
SearchCtrl.prototype._service = null;

SearchCtrl.prototype._initialize = function() {
    var self = this;
    this._iui.setRightButtonBehaviour("toolbar-Rbutton-execute",
                                      function() {self.executeSearch();});
    this._iui.showPageById('searchDialog');
};

SearchCtrl.prototype.clearResults = function() {
    this._$searchDialog.children("li:not(.group)").remove();
};

SearchCtrl.prototype._showError = function(errorMessage) {
    this.clearResults();
    this._addResultItem(errorMessage);
};

SearchCtrl.prototype._addResultItem = function(resultContent) {
    this._$searchDialog.append($("<li/>").append(resultContent));
};

SearchCtrl.prototype.executeSearch = function() {
    var self = this;
    try {
        var prefix = this._$textBox.val().trim();
        if (prefix.length !== 0) {
            this.clearResults();
            this._addResultItem($.i18n("searchPage-searching"));
            var center = this._map.getCenter();
            this._service.searchFeaturesByPattern(prefix, center,
                function(foundFeatures) {
                   self._showResults(foundFeatures);
            });
        }
    } catch (exc) {
        this._showError(exc.toString());
    }
};

SearchCtrl.prototype._showResults = function(features) {
    var self = this;
    this.clearResults();
    if (features.length === 0) {
        this._addResultItem("<span data-i18n='searchPage-noResults'>"+$.i18n("searchPage-noResults")+"</span>");
    } else {
        for (var idx=0; idx < features.length; idx++) {
            var feature = features[idx];
            var label = feature.label + " (" + feature.distance + "m)";

            var $content = $("<a/>")
                             .text(label)
                             .data("feature", feature)
                             .click(function() {
                                 self._service.setFeatures(features);
                                 var feature = $(this).data("feature");
                                 self._activateFeature(feature);});
            this._addResultItem($content);
        }
    }
};

SearchCtrl.prototype._activateFeature = function(feature) {
    this._service.setActiveFeature(feature);
    this._iui.back();
};
