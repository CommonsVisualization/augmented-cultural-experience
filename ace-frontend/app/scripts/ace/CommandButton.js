function CommandButton(text, localeKey, imgSrc, fnContext, fnClick, fnArgsArray) {
    this._text = text || this._text;
    this._localeKey = localeKey || this._localeKey;
    this._imgSrc = imgSrc || this._imgSrc;
    this._fnContext = fnContext || this._fnContext;
    this._fnClick = fnClick || this._fnClick;
    this._fnArgsArray = fnArgsArray || this._fnArgsArray;

    this._initialize();
}

CommandButton._lastEvtTimeStamp = null;

CommandButton.prototype._text = null;
CommandButton.prototype._localeKey = null; // Used for translation
CommandButton.prototype._imgSrc = null;
CommandButton.prototype._fnContext = null;
CommandButton.prototype._fnClick = null;
CommandButton.prototype._fnArgsArray = null;

CommandButton.prototype.$element = null;

CommandButton.prototype._initialize = function() {
  var label = $('<label/>')
            .text(this._text)
            .attr('data-i18n', this._localeKey);

    this.$element =  $('<button/>')
                       .addClass('icon')
                       .append($('<img/>')
                                 .attr('alt', this._text)
                                 .attr('src', this._imgSrc)
                                 .css('width', '50px'))
                       .append(label);
    this._initializeClick();
    return this.$element;
};

CommandButton.prototype._clickFx = function(callback) {
    // fast fade-in/fade-out.
    var $element = this.$element;
    var originalOpacity = $element.css('opacity');
    $element.animate(
        {'opacity' : '1'}, 100,
        function() {
            $element.animate(
                {'opacity' : originalOpacity}, 100,
                callback
            );
        });
};

function isChrome() {
  var isChromium = window.chrome,
    winNav = window.navigator,
    vendorName = winNav.vendor,
    isOpera = winNav.userAgent.indexOf("OPR") > -1,
    isIEedge = winNav.userAgent.indexOf("Edge") > -1,
    isIOSChrome = winNav.userAgent.match("CriOS");

  if(isIOSChrome){
    return true;
  } else if(isChromium !== null && isChromium !== undefined && vendorName === "Google Inc." && isOpera == false && isIEedge == false) {
    return true;
  } else {
    return false;
  }
}

CommandButton.prototype._onClick = function(evt) {
    evt.preventDefault();


    // This lines causes  a bug on firefox Android when goes from user page to map, you can't click to features.
    // hack to avoid double click on Android with Chrome.
    if ((CommandButton._lastEvtTimeStamp && (evt.timeStamp - CommandButton._lastEvtTimeStamp < 1000)) && isChrome()) {
        return;
    }
    CommandButton._lastEvtTimeStamp = evt.timeStamp;
    var self = this;
    self._clickFx(function() {
        self._fnClick.apply(self._fnContext, self._fnArgsArray);
    });
};

CommandButton.prototype._initializeClick = function() {
    var self = this;
    this.$element.click(function(evt){
        self._onClick(evt);
        if (evt.stopPropagation) evt.stopPropagation();
    });
};

function ToggleCommandButton(text, localKey, imgSrc, fnContext, fnClick, fnArgsArray,
                             activeText, activeLocalKey,activeImgSrc, activeFnClick, activeFnArgsArray,
                             status) {
   CommandButton.call(this, text, localKey,imgSrc, fnContext, fnClick);
   this._activeText = activeText || this._activeText;
   this._activeLocaleKey = activeLocalKey || this._activeLocaleKey;
   this._activeImgSrc = activeImgSrc || this._activeImgSrc;
   this._activeFnClick = activeFnClick || this._activeFnClick;
   this._activeFnArgsArray = activeFnArgsArray || this._activeFnArgsArray;
   this._active = status || this._active;
   this._updateView();
}

ToggleCommandButton.prototype = new CommandButton();
ToggleCommandButton.prototype._activeLocaleKey = null; // Used for translation
ToggleCommandButton.prototype._activeText  = null;
ToggleCommandButton.prototype._activeImgSrc = null;
ToggleCommandButton.prototype._activeFnClick  = null;
ToggleCommandButton.prototype._activeFnArgsArray = null;
ToggleCommandButton.prototype._active = false;

ToggleCommandButton.prototype.setActive = function(newStatus) {
    this._active = newStatus;
    this._updateView();
};

ToggleCommandButton.prototype._updateView = function() {
    if (this._active == false) {
        this.$element.children('img').attr('src', this._imgSrc);
        this.$element.children('label').text(getLocalWord(this._localeKey));
    } else {
        this.$element.children('img').attr('src', this._activeImgSrc);
        this.$element.children('label').text(getLocalWord(this._activeLocaleKey));
    }
};

ToggleCommandButton.prototype._onClick = function(evt) {
    evt.preventDefault();
    var self = this;
    var success = true;
    if (self._active == true) {
        if (self._activeFnClick) {
            success = self._activeFnClick.apply(self._fnContext, self._activeFnArgsArray) !== false;
        }
        if (success == true) {
            self._active = false;
        }
    } else {
        if (self._activeFnClick) {
            success = self._fnClick.apply(self._fnContext, self._fnArgsArray) !== false;
        }
        if (success == true) {
            self._active = true;
        }
    }
    self._updateView();
};

ToggleCommandButton.prototype._initializeClick = function()  {
    var self = this;
    this.$element.click(function(evt) {
        self._onClick(evt);
        if (evt.stopPropagation) evt.stopPropagation();
        if (evt.preventDefault) evt.preventDefault();
    });
};
