

window.applyListenerDecorator = function(eventName, eventSource) {
    if (eventSource.events == undefined) {
        eventSource.events = {};
    }
    eventSource.events[eventName] = {};
    var listeners = eventSource.events[eventName];

    var eventNameSuffix = eventName.charAt(0).toUpperCase() + eventName.substring(1);
    eventSource["add" + eventNameSuffix + "Listener"] = function(listener, key) {
        if (key == undefined) {
            key = listener;
        }
        if (listeners[key] != undefined) {
            throw ("assert: listener already added " + key + ".");
        } else {   
            listeners[key] = listener;
        }
    }

    eventSource["has" + eventNameSuffix + "Listener"] = function(listenerOrKey) {
        return listeners[listenerOrKey] != undefined;
    }
    
    eventSource["remove" + eventNameSuffix + "Listener"] = function(listenerOrKey) {
        var key = null;
        for (var currentListenerKey in listeners) {
            if ((listenerOrKey == currentListenerKey)
                || (listeners[currentListenerKey] == listenerOrKey)) {
                key = currentListenerKey;
                break;
            }
        }
        if (key == null) {
            throw "assert: Removing unknown listener (" + listenerOrKey + ").";
        }
        delete listeners[key];
    }

    eventSource["fire" + eventNameSuffix + "Event"] = function(evt) {
        for (var currentListenerKey in listeners) {
            listeners[currentListenerKey](evt);
        }
    }

}


