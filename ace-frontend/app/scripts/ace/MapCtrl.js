/** Controlls the map creation (caracteristics, Behaviors...), buttons Behaviors..
- Buttons on the map
- Leaflet configuration
**/

// Create the buttons on the bottom of the map
var FeatureControl = L.Control.extend({
    options: {
            position: 'bottomright',
            /** Map controller. */
            ctrl    : null
    },

    _cmdSearch : null,
    _cmdClearSearch : null,
    _cmdRollFeature : null,
    _cmdGeolocationButton : null,
    _cmdToggleItinerary : null,
    _cmdAudioToogleButton: null,
    _cmdUserButton: null,

    onAdd: function (map) {
            this._map = map;
            var container = this._createComponents();
            return container;
    },


    _createComponents: function () {
        var self = this;
        var ctrl = this.options.ctrl;
        var className = 'buttonPanel';
        var container = L.DomUtil.create('div', className);
        var $container = $(container);
        if (false) window.setInterval(function() {
            if ($container.is(':visible') === true) {
                var parentHeight = $('#map').height();
                var containerY = $container.offset().top;
                var containerHeight = $container.height();
                console.log('* ' + containerY + ', ' + containerHeight + ', ' + parentHeight);
                if (containerY + containerHeight + 10> parentHeight) {
                    $container.css({
                       top : parentHeight - containerHeight - 10
                    });
                }
            }
        }, 500);

        var userLocale='mapmenu-user';
        this._cmdUserButton = new CommandButton(getLocalWord(userLocale),userLocale, 'images/icons/user.svg',
                                        this.options.ctrl, ctrl.openUserPage);
        $(container).append(this._cmdUserButton.$element);

        var guideLocale='mapmenu-guide';
        var freeLocale='mapmenu-free';
        this._cmdToggleItinerary = new ToggleCommandButton(
                getLocalWord(guideLocale),guideLocale, 'images/icons/headphone.svg', ctrl, ctrl._toggleItinerary, null,
                getLocalWord(freeLocale),freeLocale,  'images/icons/map.svg', ctrl._toggleItinerary, null,
                false);
        $(container).append(this._cmdToggleItinerary.$element);

        var gpsLocale='mapmenu-gps';
        this._cmdGeolocationButton = new CommandButton(getLocalWord(gpsLocale),gpsLocale,
                    'images/icons/android-locate.svg', ctrl, function() {
                    ctrl._setUserTracking(true);
                });
        $(container).append(this._cmdGeolocationButton.$element);

        var searchLocale='mapmenu-search';
        this._cmdSearch = new CommandButton(getLocalWord(searchLocale),searchLocale,
                                        'images/icons/search.svg',
                                        this.options.ctrl, ctrl.openSearchDialog);
        $(container).append(this._cmdSearch.$element);

        var clearLocale='mapmenu-clean';
        this._cmdClearSearch = new CommandButton(getLocalWord(clearLocale),clearLocale, 'images/icons/close-circled.svg',
                                        this.options.ctrl, ctrl.clearMapFeatures);
        this._cmdClearSearch.$element.hide();
        $(container).append(this._cmdClearSearch.$element);

        var nextLocale='mapmenu-next';
        this._cmdRollFeature = new CommandButton(getLocalWord(nextLocale),nextLocale,  'images/icons/arrow-down-a.svg',
                                        this.options.ctrl, ctrl.rollFeature);
        this._cmdRollFeature.$element.hide();
        $(container).append(this._cmdRollFeature.$element);

        var audioService = ctrl._audioService;
        var audioLocale='mapmenu-audio';
        var muteLocale='mapmenu-mute';
        this._cmdAudioToogleButton = new ToggleCommandButton(
                getLocalWord(audioLocale),audioLocale,'images/icons/play.svg', ctrl, ctrl._toggleAudio, null,
                getLocalWord(muteLocale),muteLocale,'images/icons/pause.svg', ctrl._toggleAudio, null,
                false);
        audioService.addOnStartListener(function() {
            self._cmdAudioToogleButton.setActive(true);
        });
        audioService.addOnStopListener(function() {
            self._cmdAudioToogleButton.setActive(false);
        });
        this._cmdAudioToogleButton.$element.hide();
        $(container).append(this._cmdAudioToogleButton.$element);

        return container;
    },

    setAudioButtonVisibility : function(visibility) {
        if (visibility) {
            this._cmdAudioToogleButton.$element.show();
        } else {
            this._cmdAudioToogleButton.$element.hide();
        }
    }


});

var MarkIcon = L.Icon.extend({
    options : {
        'iconUrl'    : 'images/markers/marker-blue.gif',
        'iconSize'   : new L.Point(17, 19),
        'iconAnchor' : new L.Point(0, 19),
        'popupAnchor': new L.Point(9, -15)
        //shadowUrl: '../docs/images/leaf-shadow.png',
        //shadowSize: new L.Point(68, 95),
    }
});

var TouchIcon = L.Icon.extend({
    options : {
        'iconUrl'    : 'images/touch.png',
        //'iconUrl'    : '../images/touch.png', !!!!!!!!!!!!!!!!!!!
        'iconSize'   : new L.Point(40, 40),
        'iconAnchor' : new L.Point(20, 20)
        //shadowUrl: '../docs/images/leaf-shadow.png',
        //shadowSize: new L.Point(68, 95),
    }
});


L.Popup.include({
    getSource : function() {
        return this._source;
    }
});


function MapCtrl(iui, $mapElement, featureService, audioService, itineraryService, userCtrl, imageCtrl) {
    var self = this;
    this._iui = iui || this._iui;
    this._$mapElement = $mapElement || this._$mapElement;
    this._featureService = featureService || this._featureService;
    this._audioService = audioService || this._audioService;
    this._itineraryService = itineraryService || this._itineraryService;
    this._userCtrl = userCtrl || this._userCtrl;
    this._imageCtrl = imageCtrl || this._imageCtrl;

    self._executeWhenMapIsVisible(self, function() {self._map.invalidateSize();}, [], true);

    $(this._featureService).on(FeatureService._FEATURES_MODIFIED_EVENT_NAME, function() {
        self._executeWhenMapIsVisible(self, self._onFeaturesModified);
    });
    $(this._featureService).on(FeatureService._FEATURE_ACTIVATED_EVENT_NAME, function() {
        self._executeWhenMapIsVisible(self, self._onFeatureActivated);
    });
    $(this._itineraryService).on(ItineraryService._ITINERARY_MODIFIED_EVENT_NAME, function() {
        self._executeWhenMapIsVisible(self, self._onItineraryModified);
    });
    $(this._itineraryService).on(ItineraryService._FEATURE_ACTIVATED_EVENT_NAME, function() {
        self._executeWhenMapIsVisible(self, self._onItineraryFeatureActivated);
    });
}

MapCtrl._INITIAL_LATLON = new L.LatLng(41.3621, 2.15808);
MapCtrl._MAP_BOUNDS = new L.LatLngBounds(
                        new L.LatLng(41.35951, 2.15510),
                        new L.LatLng(41.36510, 2.16140)
                      );
MapCtrl._INITIAL_ZOOM = 17;
MapCtrl._FEATURES_MIN_ZOOM = 19;
MapCtrl._ITINERARY_FEATURE_AUTODISPLAY_DISTANCE = 10;

MapCtrl.prototype._iui = null;
MapCtrl.prototype._map = null;
MapCtrl.prototype._$mapElement = null;
MapCtrl.prototype._canvasLayer = null;

MapCtrl.prototype._featureService = null;
MapCtrl.prototype._itineraryService = null;
MapCtrl.prototype._audioService = null;

MapCtrl.prototype._userCtrl = null;
MapCtrl.prototype._imageCtrl = null;


MapCtrl.prototype._featureGroup = null;
MapCtrl.prototype._itineraryGroup = null;

MapCtrl.prototype._watchingGpsIsActive = false;
MapCtrl.prototype._userTrackingIsActive= false;
MapCtrl.prototype._userMark = null;

MapCtrl.prototype._lastAdquiredPositionTime = null;

MapCtrl.prototype._touchMarker = new L.Marker(new L.LatLng(0,0),
                                        {'icon': new TouchIcon(),
                                         'clickable' : false});

MapCtrl.prototype._featureControl = null;

MapCtrl.prototype._loadingControl = null;

MapCtrl.prototype.initialized = false;

MapCtrl.prototype.initialize = function() {
    var self = this;
    this._$mapElement.css('height', '100%');
    this._map = new L.Map(this._$mapElement[0],
                {crs : L.CRS.EPSG4326,
                    attributionControl :false,
                 zoomControl : false,
                 maxZoom : 22,
                 maxBounds : MapCtrl._MAP_BOUNDS,
                 doubleClickZoom : false,
                 keyboard : true,
                 keyboardPanOffset : 250
                }).on('click', function(evt) {
                    self._showTouch(evt.latlng, evt.layerPoint);
                    self.showFeaturesNearLatLon(evt.latlng);
                    if (evt.stopPropagation) evt.stopPropagation();
                }).on('dragend', function(evt) {
                    self._setUserTracking(false);
                }).on('popupopen', function(evt) {
                    var marker = evt.popup.getSource();
                    var feature = marker.options.feature;
                    if (feature.audioResource) {
                        self._onPopupShowed(feature);
                    }
                    if (evt.stopPropagation) evt.stopPropagation();
                });
    // Using geojson implies drawing vectors and that pane is usually
    // over the tile pane with the canvas.
    this._map.getPanes()['tilePane'].style.zIndex = 5;
    var colorsPromise = $.getJSON('colors.json');
    var fitoPromise = $.getJSON('fitoepisodis.json');
    var furniturePromise = $.getJSON('baranes.json');
    $.when(colorsPromise, fitoPromise, furniturePromise)
     .then(function(colorsResponse, fitoepisodisResponse, furnitureResponse) {
        self._fitoColors = colorsResponse[0];
        L.geoJson(fitoepisodisResponse[0], {
            minZoom : MapCtrl._INITIAL_ZOOM,
            maxZoom : 24,
            touchZoom : true,
            attributionControl : false,
            style: function (feature) {
                var color = self._fitoColors[feature.properties.ID];
                var style = {
                       "clickable": false,
                       "color": color ? color.stroke : 'darkgray',
                       "fillColor": color ? color.fill : 'lightgray',
                       "weight": 3.0,
                       "opacity": 0.8,
                       "fillOpacity": 0.8
                   };
                   return style;
            }
        }).addTo(self._map);
        L.geoJson(furnitureResponse[0], {
            minZoom : MapCtrl._INITIAL_ZOOM,
            maxZoom : 24,
            touchZoom : true,
            attributionControl : false,
            style: function (feature) {
                var style = {
                       "clickable": false,
                       "color": 'darkgray',
                       "weight": 3.5,
                       "opacity": 0.7
                   };
                   return style;
            }
        }).addTo(self._map);
    });
    self._canvasLayer = L.tileLayer.canvas({
        minZoom : MapCtrl._INITIAL_ZOOM,
        maxZoom : 22,
        async   : true
    });
    self._canvasLayer.drawTile = function(canvas, tilePoint, zoom) {
        self._drawTiles(canvas, tilePoint, zoom);
    };
    self._canvasLayer.addTo(self._map);

    var zoomControl = new L.Control.Zoom({'position' : 'topleft'});
    this._featureControl = new FeatureControl({'ctrl' : this});
    this._loadingControl = new LoadingControl({'text' : getLocalWord("leaflet-loadingControl"), 'localeKey' : "leaflet-loadingControl" });


    this._featureGroup = new L.LayerGroup();
    this._featureGroup.addTo(this._map);
    this._itineraryGroup = new L.LayerGroup();
    this._itineraryGroup.addTo(this._map);

    this._map.addControl(zoomControl)
        .addControl(this._featureControl)
        .addControl(this._loadingControl)
        .setView(MapCtrl._INITIAL_LATLON, MapCtrl._INITIAL_ZOOM);

    this._map.on('locationfound', function(evt) {
        self._onLocationFound(evt.latlng, evt.bounds, evt.accuracy, evt.heading);
    });
    this._map.on('locationerror', function(exc) {
        self._onLocationError(exc.message);
    });


    this._$mapElement.data('map', this._map);

    this._userMark = new UserMark(this._map);
    this._setUserTracking(false);
    this._startGeolocation();


    this.initialized = true;
};

MapCtrl.prototype.show = function() {
    var self = this;
    this._iui.showPage(this._$mapElement, undefined, function() {
        if (self.initialized == false) {
            self.initialize();
        }
    })
};


MapCtrl.prototype._executeWhenMapIsVisible = function(context, fn, args, keepAlive) {
    if (this._$mapElement.is(':visible') == true) {
        fn.apply(context, args);
    } else {
        // show the popup when map page is visible.
        this._$mapElement.on(iui.PAGE_SHOWED_EVENT_NAME,
                             function(evt) {
                                fn.apply(context, args);
                                if (keepAlive != true) {
                                    $(this).unbind(evt);
                                }
                             });
    }
};


MapCtrl.prototype._pauseAudio = function() {
    this._audioService.stop();
};

MapCtrl.prototype._toggleAudio = function() {
    if (this._audioService.isPlaying() == true) {
        this._audioService.stop();
    } else {
        this._audioService.play();
    }
};

MapCtrl.prototype._setUserTracking = function(value) {
    if (value == true) {
        if (this._watchingGpsIsActive == false) {
            this._startGeolocation();
        }
        this._userTrackingIsActive = true;
    } else {
        this._userMark.clear();
        this._userTrackingIsActive = false;
    }
};

MapCtrl.prototype._startGeolocation = function() {
    this._watchingGpsIsActive = true;
    this._map.locate({
        watch : true,
        setView : false,
        enableHighAccuracy : true,
        timeout : 1000*60*5
    });
};

MapCtrl.prototype._stopGeolocation = function() {
    this._watchingGpsIsActive = false;
    this._userMark.clear();
    this._map.stopLocate();
};

MapCtrl.prototype._onLocationFound = function(latlng, bounds, accuracy, heading){
    if (MapCtrl._MAP_BOUNDS.contains(latlng) === true ) {
        var currentTime = new Date().getTime();
        if (this._lastAdquiredPositionTime &&
            (currentTime - this._lastAdquiredPositionTime < 500)) {
            return;
        }
        this._lastAdquiredPositionTime = currentTime;

        if (this._itineraryService.getActiveFeature() !== null) {
            this._showNearestLatLonItineraryFeature(latlng);
        }
        if (this._userTrackingIsActive === true) {
            this._userMark.updatePosition(latlng, accuracy);
            if (this._map.getBounds().contains(bounds) === false) {
                this._map.setView(latlng);
            }
        }
    }
};

MapCtrl.prototype._onLocationError = function(errorMessage){
    this.showMessage("mapcontroller-gpsError");
    this._stopGeolocation();
};

MapCtrl.prototype._showNearestLatLonItineraryFeature = function(latLng) {
    var features = this._itineraryService.getItineraryFeatures();
    var minDistance = 1E6;
    var nearestFeature = null;
    for (var idx=0; idx < features.length; idx++) {
        var feature = features[idx];
        var featureLatLng = new L.LatLng(feature.lat, feature.lon);
        var featureDistance = Math.abs(featureLatLng.distanceTo(latLng));
        if (featureDistance < minDistance) {
            nearestFeature = feature;
            minDistance = featureDistance;
        }
    }
    if (minDistance < MapCtrl._ITINERARY_FEATURE_AUTODISPLAY_DISTANCE) {
        if (this._itineraryService.getActiveFeature() != nearestFeature) {
            this._itineraryService.setActiveFeature(nearestFeature);
            // TODO: ARREGLA EL BEEP! this._audioService.play('airbeep.mp3');
        }
    }
};

/* TODO: convert images in a sprite sheet. */
var _loadImage = function(url) {
    var img = new Image();
    img.src=url;
    return img;
};

MapCtrl.prototype._pictograms = {
    '-1': { image: _loadImage('images/pictograms/picto_round.png'),  color : '#558C89'},
    '1' : { image: _loadImage('images/pictograms/picto_small1.png'), color : '#cdcd00'},
    '2' : { image: _loadImage('images/pictograms/picto_small2.png'), color : '#8b4789'},
    '3' : { image: _loadImage('images/pictograms/picto_small3.png'), color : '#8B8B00'},
    '4' : { image: _loadImage('images/pictograms/picto_small4.png'), color : '#8b5a00'},
    '5' : { image: _loadImage('images/pictograms/picto_small4.png'), color : '#8b5a00'},
    '6' : { image: _loadImage('images/pictograms/picto_small6.png'), color : '#8b2500'},
    '7' : { image: _loadImage('images/pictograms/picto_small6.png'), color : '#8b2500'},
    '8' : { image: _loadImage('images/pictograms/picto_small8.png'), color : '#1C1C1C'},
    '9' : { image: _loadImage('images/pictograms/picto_small9.png'), color : '#8b5a00'},
    '10': { image: _loadImage('images/pictograms/picto_small10.png'), color : '#1C1C1C'},
    '11': { image: _loadImage('images/pictograms/picto_small11.png'), color : '#458b00'},
    '12': { image: _loadImage('images/pictograms/picto_small12.png'), color : '#00ee00'}
};

// Testing all resources are been loaded properly
// MapCtrl.locationsArray= new Array ();
// MapCtrl.repeated= new Array ();
// var contains = function(needle) {
//     // Per spec, the way to identify NaN is that it is not equal to itself
//     var findNaN = needle !== needle;
//     var indexOf;
//
//     if(!findNaN && typeof Array.prototype.indexOf === 'function') {
//         indexOf = Array.prototype.indexOf;
//     } else {
//         indexOf = function(needle) {
//             var i = -1, index = -1;
//
//             for(i = 0; i < this.length; i++) {
//                 var item = this[i];
//
//                 if((findNaN && item !== item) || item === needle) {
//                     index = i;
//                     break;
//                 }
//             }
//             return index;
//         };
//     }
//     return indexOf.call(this, needle) > -1;
// };
// MapCtrl.n = 0;


/**Draw the tiles on the map on MapCtrl._FEATURES_MIN_ZOOM **/
MapCtrl.prototype._drawTiles = function(canvas, tilePoint, zoom) {
        var zoomLevel = zoom ? zoom : this._map.getZoom();
        if (zoomLevel <= MapCtrl._FEATURES_MIN_ZOOM) {
            this._canvasLayer.tileDrawn(canvas);
            return;
        }
        var QUERY_MARGIN = 25;
        var self = this;
        var tileSize = this._canvasLayer.options.tileSize;
        var leftTopPointInWorld= tilePoint.multiplyBy(tileSize).subtract(new L.Point(QUERY_MARGIN, QUERY_MARGIN));
        var leftTopLatLng = this._map.unproject(leftTopPointInWorld);
        var rightBottomPointInWorld= leftTopPointInWorld.add(new L.Point(tileSize+QUERY_MARGIN*2, tileSize+QUERY_MARGIN*2));
        var rightBottomLatLng = this._map.unproject(rightBottomPointInWorld);

        // var n =  MapCtrl.n;
        // MapCtrl.n =  MapCtrl.n + 1;
        // console.log(n);
        // console.log(rightBottomLatLng.lat+","+leftTopLatLng.lng+"\n"+leftTopLatLng.lat+","+rightBottomLatLng.lng);

        this._featureService.searchFeaturesByBBox(
                             rightBottomLatLng.lat, leftTopLatLng.lng,
                             leftTopLatLng.lat, rightBottomLatLng.lng)
        .success(function(featureReferences)  {

          // console.log("num "+n+"\nlength: "+featureReferences.length);
          // MapCtrl.n = MapCtrl.n +1;

            var ctx = canvas.getContext('2d');
            for (var i = 0; i < featureReferences.length; i++) {

                var coordinateLatLng = new L.LatLng(featureReferences[i][0], featureReferences[i][1]);
                var port = featureReferences[i][2];
                var coordinatePointInWorld = self._map.project(coordinateLatLng);
                var coordinatePointInCanvas = new L.Point(Math.round(coordinatePointInWorld.x - leftTopPointInWorld.x - QUERY_MARGIN ),
                                                          Math.round(coordinatePointInWorld.y - leftTopPointInWorld.y - QUERY_MARGIN));
                var iconWidth = self._pictograms[port].image.width;
                var iconHeight = self._pictograms[port].image.height;

                // Test totals loaded
                // var needle = featureReferences[i][0]+","+ featureReferences[i][1];
                // if (contains.call(MapCtrl.locationsArray, needle)) {
                //   MapCtrl.repeated.push(needle);
                // } else {
                //     MapCtrl.locationsArray.push(needle);
                // }
                // console.log("Total Mapped: " , MapCtrl.locationsArray.length);
                // console.log("Repeated: " , MapCtrl.repeated.length);


                // Draw the picogram specific width
                if (zoomLevel == 21) {
                    iconWidth = iconWidth * 0.6;
                    iconHeight = iconHeight * 0.6;
                }

                // Draw pictogram image proportional to the zoom
                if (zoomLevel >= 21) {
                    ctx.drawImage(self._pictograms[port].image,
                                        coordinatePointInCanvas.x-iconWidth/2,
                                        coordinatePointInCanvas.y-iconHeight/2, iconWidth, iconHeight);
                }
                // Draw a point
                else {
                    ctx.beginPath();
                    ctx.strokeStyle = self._pictograms[port].color;
                    ctx.fillStyle = self._pictograms[port].color;
                    ctx.arc(coordinatePointInCanvas.x, coordinatePointInCanvas.y, 2, 0, 2 * Math.PI, false);
                    ctx.stroke();
                    ctx.fill();
                }

            }


            self._canvasLayer.tileDrawn(canvas);
        });

        return this;
  };

// Shows specific itinerary object on map
MapCtrl.prototype.showItinerary = function(itinerary) {
  this._featureControl._cmdToggleItinerary.setActive(true); //Update itinerary icon on map controllers
  this._map.setZoom(MapCtrl._INITIAL_ZOOM);
  this._itineraryService.setActiveItinerary(itinerary);
  this.show();
}


MapCtrl.prototype._toggleItinerary = function( ) {
    if(this._itineraryService.getActiveItinerary() == null) {
        this._map.setZoom(MapCtrl._INITIAL_ZOOM);
        this._itineraryService.loadItinerary('itiaud');
    } else {
        this._itineraryService.clearItinerary();
    }
};

MapCtrl.prototype.goBackToMenu = function() {
    var self = this;

    this._audioService.stop();
    iui.setRightButtonBehaviour(null);
    iui.showPageById("menu");
};

MapCtrl.prototype.openSearchDialog = function() {
    var searchCtrl = new SearchCtrl(this._iui, this._map, this._featureService);
};

MapCtrl.prototype.openUserPage = function (){
  this._userCtrl.show();
}

MapCtrl.prototype._showFeatures = function(features, group, iconImageName, iconIndex) {
    group.clearLayers();
    for (var idx=0; idx < features.length; idx++) {
        var currentFeature = features[idx];
        var iconImage = 'images/markers/{0}{1}.gif';
        if (iconImageName) {
            iconImage = iconImage.replace('{0}', iconImageName);
        } else {
            iconImage = iconImage.replace('{0}', 'marker-orange');
        }
        if (iconIndex == true) {
            iconImage = iconImage.replace('{1}', idx+1);
        } else {
            iconImage = iconImage.replace('{1}', '');
        }
        var icon = new MarkIcon({'iconUrl' : iconImage});
        var marker = new L.Marker(new L.LatLng(currentFeature.lat, currentFeature.lon),
                                  {'icon': icon, 'feature': currentFeature});
        var popupContent = null;

        // ADAPTATION: This if is an adaptation for user itineraries
        if (!currentFeature.label){
          currentFeature.label = currentFeature.title;
          currentFeature.detailsAvalaible = true;
        }

        if (currentFeature.detailsAvalaible == true) {
            var cmdInfo = new CommandButton(currentFeature.label, "",
                                            'images/50px-Info-contrast-text-x-generic.svg.png',
                                            this, this._openActiveFeatureCard, [features, idx]);
            popupContent = cmdInfo.$element[0];
        } else {
            popupContent = currentFeature.label;
        }
        marker.bindPopup(popupContent, {closeButton : false});
        currentFeature.marker = marker;
        group.addLayer(marker);
    }
};

MapCtrl.prototype._showRoute = function(pathRouteString) {
    var points = [];
    var coordPairs = pathRouteString.split(',');
    for (var idx=0; idx < coordPairs.length; idx++) {
        latLngValues = coordPairs[idx].trim().split(' ');
        points.push(new L.LatLng(latLngValues[0], latLngValues[1]));
    }
    var line = new L.Polyline(points, {'weight' : 3, 'color' : 'red', 'opacity' :  0.3});
    this._itineraryGroup.addLayer(line);
};

MapCtrl.prototype.showFeaturesNearLatLon = function(latLng) {
    var self = this;
    this._featureService.searchFeaturesByLatLng(latLng, 2.0, function(foundFeatures) {
        self._featureService.setFeatures(foundFeatures);
        if (foundFeatures.length > 0) {
            self._featureService.setActiveFeature(foundFeatures[0]);
        }
    })
};

MapCtrl.prototype._openActiveFeatureCard = function(features, activeFeatureOrdinal) {
    /* TODO: Refactor this var, maybe as an optional param. */
    var $infoCard = $('#infoCard');
    var cardCtrl = createInfoCardCtrl(
                'Dades', $infoCard,
                this._audioService, features, activeFeatureOrdinal, this._featureService, this._imageCtrl);
    cardCtrl.show();
    // If is an itinerary set the itinerary service for dynamic translation
    if (cardCtrl.setItineraryService) cardCtrl.setItineraryService (this._itineraryService);
    // TODO: next sentence should become part of cardCtrl.show(), isn't it?
    this._iui.showPage($infoCard);

};

MapCtrl.prototype.clearMapFeatures = function() {
    this._featureService.clearFeatures();
    this._audioService.stop();
};

MapCtrl.prototype.rollFeature = function() {
    this._featureService.rollFeature(+1);
};

MapCtrl.prototype._onItineraryFeatureActivated = function(evt) {
    var features = this._itineraryService.getItineraryFeatures();
    var activeFeature = this._itineraryService.getActiveFeature();
    this._showFeaturePopup(features, activeFeature);
    this._updateAudioButton(activeFeature);
};

MapCtrl.prototype._onFeatureActivated = function(evt) {
    var features = this._featureService.getFeatures();
    var activeFeature = this._featureService.getActiveFeature();
    this._showFeaturePopup(features, activeFeature);
    this._updateFeatureControl(features, activeFeature);
    this._updateAudioButton(activeFeature);
};

MapCtrl.prototype._showFeaturePopup = function(features, activeFeature) {
    if (activeFeature) {
        if (activeFeature.marker) {
            activeFeature.marker.openPopup();
        }
    }
};

MapCtrl.prototype._onPopupShowed = function(feature) {
    if (feature.audioResource) {
        this._audioService.resourcePath = feature.audioResource;
    }
    this._updateAudioButton(feature);
};

MapCtrl.prototype._onFeaturesModified = function() {
    var features = this._featureService.getFeatures();
    var activeFeature = this._featureService.getActiveFeature();
    this._showFeatures(features, this._featureGroup)
    this._updateFeatureControl(features, activeFeature);
    this._updateAudioButton(activeFeature);
};

MapCtrl.prototype._updateFeatureControl = function(features, activeFeature) {
    if (features.length == 0) {
        this._featureControl._cmdSearch.$element.show();
        this._featureControl._cmdClearSearch.$element.hide();
        this._featureControl._cmdRollFeature.$element.hide();
    } else {
        this._featureControl._cmdSearch.$element.hide();
        this._featureControl._cmdClearSearch.$element.show();
        if (features.length == 1) {
            this._featureControl._cmdRollFeature.$element.hide();
        } else {
            this._featureControl._cmdRollFeature.$element.show();
        }
    }
};

MapCtrl.prototype._updateAudioButton = function(activeFeature) {
    if (activeFeature && activeFeature.audioResource) {
        this._featureControl.setAudioButtonVisibility(true);
    } else {
        this._featureControl.setAudioButtonVisibility(false);
    }
};

MapCtrl.prototype._onItineraryModified = function() {
    var itinerary = this._itineraryService.getActiveItinerary();
    if (itinerary != null) {
        var features = this._itineraryService.getItineraryFeatures();
        this._showFeatures(features, this._itineraryGroup, 'lightblue', true)
        // ADAPTATION
        if (itinerary.pathRouteString) {
          this._showRoute(itinerary.pathRouteString);
        }
    } else {
        this._itineraryGroup.clearLayers();
    }
};

MapCtrl.prototype._showTouch = function(latlng, point) {
    var self = this;
    this._touchMarker.setLatLng(latlng);

    this._map.addLayer(this._touchMarker);
    setTimeout(function() {
        self._map.removeLayer(self._touchMarker);
    }, 250);

};

MapCtrl.prototype.showMessage = function(msg) {
    var $div = $('#message');
    if ($div.length == 0) {
        $div = $('<div/>')
           .attr('id', 'message')
           .attr('data-i18n', msg)
           .css({
               top : '35px',
               position: 'absolute',
               display: 'none',
               height : '1em',
               zIndex : 1E6,
               fontSize : '1.2em',
               'min-height' : '',
               padding : '1em 1em 2em 1em',
               color : 'white',
               backgroundColor : 'silver',
               textAlign : 'center'
           })
           .click(function() {
               $(this).stop().fadeOut(function() {
                   $(this).detach();
               });
           })
           .appendTo('body')
           .fadeIn()
           .delay(10*1000)
           .fadeOut(function() {
              $(this).detach();
           });
    };
    $div.html($.i18n(msg)).fadeIn();
};
