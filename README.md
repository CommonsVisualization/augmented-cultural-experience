# What is ACE?

ACE (Augmented Cultural Experience) is a software that **maps a botanical garden** and provides to users and workers the capacity to visualize from their own smartphones, tablet or any device with a browser the characteristics of all the plants with text, photos and audios.

All the singles are geolocated and represented in a map that is also used as guide for do itineraries for the garden. All users can contribute to the application map with their own photos and itineraries.

- Current Version: [1.2.1](CHANGELOG.md)

# How it works?

We wanted to do a transparent authentication, with the less user interaction to start using the application. Also we planned for first that _ace_ should work ideally on lan. This is because this software will work _in situ_, what does it mean? This mean that each implementation is deployed on a physical space, and the user will be there to use the application on all its features.  

Also a user could have more than one device (called _identities_) associated to her account (called _user_).

The user access to the application on the lan network, authenticates against the backend and can start to use the application:

- See and navigate over the map.
- Be geolocated and tracked (**only runing over https!**).
- Consult single individuals position and its taxon information (photo/audio/text).
- See default garden itinerary.
- Login or signup to the application.
- Create user itineraries.
- Upload photos associated to a taxon, user-itinerary point or to your own gallery.

So lets see the authentication procedure:

## From LAN
#### First time access

1. The final user connects to the local network with his device, accessing to the application frontend server.
1. The frontend sends a token /auth petition to the backend server.
1. The server get the MAC of the user and check on the identity table if is the first connection time. If it is creates an identity and a user. Using ARP command the backend get user MAC address to use it as default username and identity. The user table stores the user info, and the identity table the MAC of the user different devices.
1. Each time that a user access with this device will automatically login, using the MAC of the device.
1. If user wants to register, will be request for a username and pin code to use it to login with other device. Anyway, if a registered user use the same device to access the app, it login transparently using his MAC address.

#### Access with other device

1. The workflow is the same as above.
1. The user can register his identity to an existing user using the username and pin.
1. So after register this new identity (connected with the MAC address of his device) to an existing user, the login will be transparent to him: each time that he will access with one of his registered MAC address will automatically login to his user.

Note: this security is breakable with an ARP spoofing, you could login as other user identity. But for us, is important to do this transparent login to improve the user experience. A possible solution could be to get "user agent" from the clients.

Note2: For the moment we have **dependencies with ARP package** to get the MAC.

## From Internet

On the frontend, if after try to get `/auth` we get a response error, it will show a only login form on the users page. This login form submits a basic authentication with username and userpin for a pre-registered user (this mean that the user should be in the place and sign up the application before).

On the backend the way to detect that a request come from Internet is if the requested MAC address is blacklisted or the MAC address length is 0 and the user password length is bigger than 0. Else, it throws an authentication error.

If the user and userpin password are corrects, it perform the jwt authentication with the system password (or users default password) to return the token to the user.

# Install

First of all just clone this repo, this will download the backend and frontend parts:

```bash
git clone https://gitlab.com/CommonsVisualization/augmented-cultural-experience && cd augmented-cultural-experience/
```

## Backend
(`ace-backend/`)

### Install database

1. Install all backend dependencies:

  ```bash
  sudo apt install postgresql-9.6 openjdk-8-jdk maven net-tools
  ```
**Important notes about deprecated packages:**
    
* For new linux distros **postgres 10 is not compatible**. See #62 to how to install `postgres-9.6` package.
* After reading the dependencies you will see that we need to install the **deprecated net-tools package**. This is because we get the user mac address using arp command, on lan transparent authentication.  We apologize about that, we are working to use the most modern `ip` command. See the workarround or integration solutions on Debian on #32
    
2. Change postgres user password using psql shell:

	```
	sudo -u postgres psql postgres
    \password postgres
    \q
	```

3. Prepare the database

	1. Access as user postgres (in order to use psql shell): `sudo -i -u postgres`
	2. Create database: `createdb -T template0 acedb`
	3. Inside the psql shell, create a role "ace" and a role "worker" (note that workers can't see or modify the tables with users data):

    ```
    psql
    CREATE ROLE ace;
    CREATE ROLE worker;
    CREATE ROLE jbotanic_remot;
    CREATE ROLE jbotanic;
    \q
    ```

4. Download latest database sample from: https://gitlab.com/CommonsVisualization/databases_of_the_commons

    ```
    wget https://gitlab.com/CommonsVisualization/databases_of_the_commons/raw/master/acedb-jardi_botanic_barcelona-plants-identities-postgresql9.5.6-20171117.sql -O jbotanic.sql && psql acedb < jbotanic.sql
    ```

5. Now its time to import the database, first of all cd into .dump file path. Make sure that in the dump the Owner of the databases is the same as the role that we created before. Now import db using:

    `psql acedb < database-name.dump`

### [Download and install maven](https://maven.apache.org/)

- Maven 3.0.5 and 3.3.9
- Java JDK1.7 minimum

    __Maven manual installation:__
    1. Download Maven & Unzip
    2. `ln -s /mavenPath/bin/mvn /bin/mvn`
    3. `mvn -v` #To test if it works

	_Note: Read [Maven getting started](https://maven.apache.org/guides/getting-started/)_

### Generate .jar

Set your JAVA_HOME variable (if is not set). After, go to ace-backend directory and use maven to build:

```bash
mvn -DskipTests=true -f ace-backend/pom.xml clean install
```

### Create necessary directories

The backend files (such images) are stored by default on `/var/ace` directory, so create them and give it necessary permissions to write on them. You can change directories on application.yml on the project before compile.

## Frontend

(`ace-frontend/`)

__Frontend javascript dependencies installation:__

1. npm, nodejs
  ```
  sudo apt install npm nodejs
  ln -s /usr/bin/nodejs /usr/bin/node  # To solve "/usr/bin/env: 'node': No such file or directory" error on Debian
  ```
  Note: if you are using Debian but not Sid (neither Debian Jessie) npm and nodejs could not be in your `sources.list `repositories, add them or [follow installation from nodejs website](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions).*

2. Install Node dependencies, Gulp, Bower and Bower dependencies.

	```bash
	sudo npm install --global gulp-cli bower
	npm install --prefix ace-frontend
	bower --config.cwd=ace-frontend/ install
	```

After installation test it using:

```bash
gulp -f ace-frontend/gulpfile.js serve
```


### Install audios

We generated taxon description audio files using TTS software. You can download the generated audios for Catalan, Spanish and English [here](https://gitlab.com/CommonsVisualization/databases_of_the_commons). Copy or `ln -s` the audio directory to application frontend working dir:

```bash
sudo ln -s /home/ace/CommonsVisualization/databases_of_the_commons/audio/ /var/www/html/audio
```

For more info about how this audio files are generated check [this](https://gitlab.com/CommonsVisualization/augmented-cultural-experience/issues/48).

# Running
## Backend

Using this command you should run the REST server on default 8080 port:

```bash
/usr/bin/java -jar  /path/to/api.jar \
            --spring.datasource.username=ace \
            --spring.datasource.password=dbpassword \
            --logging.file=/var/log/ace-backend.log \
            --server.ssl.key-store-password=supersecret \
            --debug;
```
**Note:** After v1.1, ssl support is enabled. So you will need to have a _java key store_ with a valid ssl key. If this is miss configured, the geolocation will not work. Mor information of how to get a certificate at the end of this document. You can disable ssl adding to he command: `--server.ssl.enabled=false`

##### Run it at startup:
An option could be to store the previous command on a script, give it execution permissions and add it on `/etc/rc.local` using the follwing line. The sleep time is needed to let postgres database start.

```bash
((sleep 10; sudo -u ace /home/kon/api.start)&)
```

## Frontend

There are various gulp commands to look. Run its from `ace-frontend/`:

* `gulp serve` : This start a web server on port 9000 for http and 9001 for https.
* `gulp build` : Run this for build the code on `ace-frontend/dist` with minified code and images.
* `gulp deploy` : will deploy the `ace-frontend/dist` directory on `/var/www/html`. Check writing permissions of the destination directory to make it work or run it as sudo.

Configure the parameters on `ace-frontend/gulpfile.js`.

# SSL support

After version 1.1, ssl support is implemented on frontend and backend. **If the SSL is not configured the geolocation will not work!**

* To get a valid certificate you can use [Lets Encrypt](https://letsencrypt.org/) service, and automatize the certificate renewal with [cert-bot](https://certbot.eff.org/). For example:

```
sudo certbot certonly --webroot -w /var/www/html -d jardibotanic.guifi.net
```

* To use it on backend you will need to add the key to the tomcat server using a [java keystore](https://en.wikipedia.org/wiki/Keystore). Watch the step by step guide that we follow to import it on: https://gitlab.com/CommonsVisualization/augmented-cultural-experience/issues/16#note_69077884 . After the configuration is loaded, run the backend using a command like:

```bash
/usr/bin/java -jar /path/to/api.jar --spring.datasource.username=dbusername --spring.datasource.password=dbpassword --debug --logging.file=/var/log/ace-backend.log --server.ssl.key-store-password=supersecret
```
* You can disable ssl adding to he command: `--server.ssl.enabled=false`.

* To use it on frontend configure your web server to use the keys. Instead if you are using gulp, search on `ace-frontend/gulpfile.js` the variables `key` and `cert` to change the path of the keys.

* Look at the use cases of having both, http and https enabled on https://gitlab.com/CommonsVisualization/augmented-cultural-experience/issues/51#note_75399690




[1]: https://maven.apache.org/

[3]: https://maven.apache.org/guides/getting-started/

[2]: https://www.postgresql.org/

[5]: https://netbeans.org/downloads/

[6]: https://ask.fedoraproject.org/en/question/94538/netbeans-jdk-is-missing-and-is-required-to-run-some-modules/

[4]: https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md
[5]: https://gitlab.com/CommonsVisualization/augmented-cultural-experience/issues/48
[6]: https://gitlab.com/CommonsVisualization/databases_of_the_commons
