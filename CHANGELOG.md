# Changelog
All notable changes to this project will be documented in this file.

## [1.2.1] - 2018-12-31
### Changed
- Fix token expiration issue. #65
- Fix blacklisted identity crash. #65

## [1.2] - 2018-06-28
### Added
- [Internet mode](https://gitlab.com/CommonsVisualization/augmented-cultural-experience/commits/feature-internet-access). Now if the user comes from outside the Lan is possible to log-in with a previous created user.
- Version number on frontend footer with [`gulp-inject-version`](https://www.npmjs.com/package/gulp-inject-version).

### Changed
- Bower dependencies. Some libraries (jquery & leaflet) are moved from local repository to bower dependency. So now is needed to do **bower install** before use the application. The bower dependencies are wired to index.html with gulp function: `gulp wiredep`

## [1.1] - 2018-06-01
### Added
- SSL support on frontend and backend.
- CHANGELOG.md file.

### Changed
- Better log and debugging when identity is created.
- Get wikimedia images with maximum width of 200px a37e0d8.
- Refactor i18nGlobal.js 5ea832a.
- Fix map show before translations loaded 092b0aa.
- Other minor bugs fixes.
- Geolocation bug fixed


### Removed
- Clean commented and unnecessary code 23a71e3.

## [1.0] - 2018-04-19  [FIRST STABLE RELEASE]
### Added
- Add user system auth with JwT on backend.
- Users can create itineraries publics or privates.
- Users can upload photos (public and private), associated to a taxon or to a user itinerary.
- Users can see other users public data.
- Multilingual support to any language
- Translations for: English, Spanish, Catalan.
- Transparent login on user system on lan based on MAC address.
- Users can login with multiple devices.
- Users can sign up to not use the application as guests.
- Implemented opensource TTS on the description of each taxon.

### Changed
- Some UI improvements and bugfixes.


## [0.9] - 2017-05-03 [DEVELOPMENT]
### Added
- Refactor of source code inherited from Barcelona Botanical Garden.
- Free Software License AfferoGPL v.3.
- Support for Postgres database.
- Interactive leaflet map with geolocated speciments of the botanic garden.
- Frontend-backend with MVC based application.
- Geolocation on frontend user device.
- Search string for specimen.
- Download photo thumbnails from wikipedia.
- View information of an specimen: name, regular name, description, origin...
- IUI as frontend framework.
- General itinerary with audio guide and points of interest.
- Special database structure to identify specimens by family and taxon.
- Search specimen by name, or aproximate geolocation (nearest on the user point).
