    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.jwt.controller;

import com.ace.authentication.jwt.JwtAuthenticationRequest;
import com.ace.authentication.jwt.JwtTokenUtil;
import com.ace.authentication.jwt.JwtUser;
import com.ace.authentication.jwt.service.JwtAuthenticationResponse;
import com.ace.authentication.model.User;
import com.ace.authentication.service.IdentityService;
import com.ace.authentication.service.IdentityUserService;
import com.ace.authentication.service.UserService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bolet
 */
@RestController
public class AuthenticationRestController {
    
    @Value("${ace.auth.lan-user}")
    private String LAN_USER;
    
    @Value("${ace.auth.internet-user}")
    private String INTERNET_USER;
    
    @Value("${ace.auth.localhost-mac}")
    private String LOCALHOST_MAC;
    
    @Value("${ace.auth.users-default-password}")
    private String USERS_DEFAULT_PASSWORD;

    @Value("${jwt.header}") 
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;
    
    @Autowired
    UserService userService; 
    
    @Autowired
    IdentityService identityService;
    
    @Autowired
    IdentityUserService identityUserService;
    
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     *  Authenticate a user and give a token to her. Whe have two behaviours: LAN user and INTERNET user. 
     * The first one is identified by MAC address, and give the token using that. 
     * The second needs to send their credentials (username and pin) to request the token.
     * 
     * @param authenticationRequest Username and password. 
     * @param device 
     * @param request
     * @return
     * @throws AuthenticationException
     * @throws Exception 
     */
    @RequestMapping(value = "${jwt.route.authentication.path}", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest, Device device, HttpServletRequest request) throws AuthenticationException, Exception {
        
        // LAN:
        // We want to get a transparent authentication on LAN, so if the user identity doesn't exist we will create it and return her a token
        // Normally, we will recive a /auth petition, and we are going to take his Identity from her mac: getting her ip addres and consulting her mac using "arp" command.
        // So, if we get the MAC is a lan user and will be his identity name.
        // INTERNET: 
        // Otherwise, if we don't have the mac, just don't give the token until the authentication is succeed via the website login form.
        // We can only authenticate internet users that are previously registered on lan.
        
        // 0- Get the mac. 
        //  a) Get the ip address of the petition
        //  b) Get the mac using ip address and consulting arp table in your system. Tested using linux!
        String identityMac = getMac (request.getRemoteAddr());  // Ip address of the client or last proxy that sent the request (for example gateway)
        
        // The username on body request will be the type of user.
        if ( identityMac.length() > 0  && !identityService.isBlacklisted( identityMac )) {
            
            log.info("Tryng to auth user with MAC: " + identityMac);
            
            // Now check if the user identity exists, if don't exists, create it! (both, user and identity)
            if ( !identityService.identityExists( identityMac )) {
    //            userService.registerNewUser(new User ( authenticationRequest.getUsername(), authenticationRequest.getPassword() ) );   
                User tmpUser =  new User ( identityMac, USERS_DEFAULT_PASSWORD ) ;
                if (!identityUserService.registerUserIdentity(tmpUser) ){
                    return new ResponseEntity<String>("{\"success\" : \"false\", \"message\": \"Not possible to create identity for user\", \"user\":  \""+ tmpUser.getUsername() +"\", \"mac\": \""+identityMac+"\"}" ,HttpStatus.EXPECTATION_FAILED);
                }
                
            } 
                        
            // After that the user and identity should be created
            // So lets give them the token
            // First get the user that have this identity:
            authenticationRequest.setUsername(
                    identityUserService.getUserFromIdentity( 
                            identityMac 
                    ).getUsername()
            );
            authenticationRequest.setPassword(USERS_DEFAULT_PASSWORD);            
        }
        
        // If we don't have mac is an "internet user" or a user from outside the lan, so we need to authenticate her.
        // The user must to send a password to try the authentication. Otherwhise just return an error.
        else if ( (identityMac.length() == 0 || identityService.isBlacklisted( identityMac )) && authenticationRequest.getPassword().length() > 0) { 
//        else if ( identityMac.length() == 0  ) { 

            // Special commands for internet users
            log.info("Checking identification for internet user:  " + authenticationRequest.getUsername() );
            // Try to get the user, if not will throw an exception
            User user = userService.findByUsername( authenticationRequest.getUsername() );  
            // Check if the pin is from the user
            if (userService.pinValidation(user, authenticationRequest.getPassword())) {
                authenticationRequest.setPassword(USERS_DEFAULT_PASSWORD);
            } else {
                throw new org.springframework.security.core.userdetails.UsernameNotFoundException("Unauthorized");
            }
        } 
        // Error if is not connected from lan or the password lenght is 0.
        else { 
            throw new org.springframework.security.authentication.BadCredentialsException("Unauthorized");
        }

        return performSecurity(authenticationRequest.getUsername(),
                                authenticationRequest.getPassword(), device);
    }
    
    private  ResponseEntity performSecurity (String username, String password, Device device){
        
        // Perform the security directly to the user
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        username,
                        password
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Reload password post-security so we can generate token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        final String token = jwtTokenUtil.generateToken(userDetails, device);
        
        // Return the token
        return ResponseEntity.ok(new JwtAuthenticationResponse(token));        
    }

    @RequestMapping(value = "${jwt.route.authentication.refresh}", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }
    
    
    Pattern macpt = null;
    
    /**
     * Function that consults arp table to get mac of a ip. Only works on linux dispositives. Tested on debian based.
     * @param ip The ip to check. If its 127.0.0.1 check variable LOCALHOST_MAC
     * @return mac
     */
    private String getMac(String ip) {
        
        log.info("Looking for mac address for client ip: " + ip);
        
        // Localhost are not in arp table!
        if (ip.equals( "127.0.0.1" ) || ip.equals("0:0:0:0:0:0:0:1")) { // This is for localhost request. 0.0.0.0.0... for chromium requests on localhost.  
//            return LOCALHOST_MAC;
            return "";
        }

        // Find OS and set command according to OS
//        String OS = System.getProperty("os.name").toLowerCase();
//
        String[] cmd;
//        if (OS.contains("win")) {
//            // Windows
//            macpt = Pattern
//                    .compile("[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+");
//            String[] a = { "arp", "-a", ip };
//            cmd = a;
//        } else {
            // Mac OS X, Linux
            macpt = Pattern
                    .compile("[0-9a-f]+:[0-9a-f]+:[0-9a-f]+:[0-9a-f]+:[0-9a-f]+:[0-9a-f]+");
            String[] a = { "arp", ip };
            cmd = a;
//        }

        try {
            // Run command
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            // read output with BufferedReader
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line = reader.readLine();

            // Loop trough lines
            while (line != null) {
//                System.out.println("@@ Log , " + line);
                Matcher m = macpt.matcher(line);

                // when Matcher finds a Line then return it as result
                if (m.find()) {
//                    System.out.println("Found");
                    log.info("MAC: " + m.group(0));
                    return m.group(0);
                }

                line = reader.readLine();
            }

        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Return empty string if no MAC is found
        return "";
    }

}
