/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.controller;

import com.ace.authentication.jwt.JwtTokenUtil;
import com.ace.authentication.model.Identity;
import com.ace.authentication.model.UpdatedUser;
import com.ace.authentication.model.User;
import com.ace.authentication.service.IdentityService;
import com.ace.authentication.service.IdentityUserService;
import com.ace.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author bolet
 */
@RestController
public class IdentityController {
    
    @Value("${jwt.header}")
    private String tokenHeader;
    
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    @Autowired
    UserService userService;
    
    @Autowired 
    IdentityService identityService;
    
    @Autowired
    IdentityUserService identityUserService;
    
    /**
     * Add identity to a user. 
     * @param identity The identity that will be in  a new user
     * @param user The unauthenticated user credentials
     * @param token The actual token of the identity. 
     * @return 
     */
    @RequestMapping(value = "/identity/{identity}", method = RequestMethod.PUT)
    public ResponseEntity<?> createUser(
            @PathVariable("identity") String id, 
            @RequestBody UpdatedUser user, 
            @RequestHeader("${jwt.header}") String token)  {
        
        // TODO: Its posible to, with a registred user, add itself to the identity of other? Yes if you have her pin.

        // Get the users
        User tokenUser = userService.findByUsername( jwtTokenUtil.getUsernameFromToken(token) );     // If token user is an admin user can edit other users
        boolean isAdminToken = userService.isAdminUser(tokenUser);
        User userToUpdate = userService.findByUsername(user.getUsername());
        Identity identity = identityService.findByIdentity(id);
        
        // Check if identity exists and doesn't have associated user (the token username is not the identiti name) or is admin token.
        // Check if username and userpin are valids, perform security.
        if (identity == null  || userToUpdate == null ){
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        } else if ( (!tokenUser.getUsername().equals(id) && !isAdminToken) || !userService.pinValidation(userToUpdate, user.getUserPin()) ){
            return new ResponseEntity<User>(HttpStatus.UNAUTHORIZED);
        }
        
        // Try to add the identity
        if (identityUserService.addIdentityToUser(userToUpdate, identity ) ){
            return new ResponseEntity<String>("{ }", HttpStatus.OK );
        } else {
            return new ResponseEntity<String>("{ }",  HttpStatus.NOT_MODIFIED);
        }
    }
}
