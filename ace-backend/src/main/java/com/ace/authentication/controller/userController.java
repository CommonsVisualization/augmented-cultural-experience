/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.controller;

import com.ace.authentication.jwt.JwtTokenUtil;
import com.ace.authentication.jwt.JwtUser;
import com.ace.authentication.model.AuthorityName;
import com.ace.authentication.model.Identity;
import com.ace.authentication.model.UpdatedUser;
import com.ace.authentication.model.User;
import com.ace.authentication.service.IdentityService;
import com.ace.authentication.service.IdentityUserService;
import com.ace.authentication.service.UserService;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * Controller for register users to the databse. 
 * @author bolet
 */
@RestController
public class userController  {
    
    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;
    
    @Autowired
    UserService userService;  //Service which will do all data retrieval/manipulation work
    
    @Autowired 
    IdentityService identityService;
    
    @Autowired
    IdentityUserService identityUserService;
    
    // Create user
    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> createUser(@RequestBody User user,    UriComponentsBuilder ucBuilder) throws Exception {
        System.out.println("Creating User " + user.getUsername());
        
        // Search the if the requested username are an existing identity
        // If there are an identity with this username means that the user exists.
        
        if (identityService.identityExists(user.getUsername())) {
            System.out.println("A User with name " + user.getUsername() + " already exist");
            return new ResponseEntity<String>( "A User with name " + user.getUsername() + " already exist",HttpStatus.CONFLICT);
        }

        // Else if the Identity doesn't exists creates an identity an his associated user.
        identityUserService.registerUserIdentity(user);

        
  
//        This piece will redirect you to /user/id if succes.
//        HttpHeaders headers = new HttpHeaders();
//        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
//        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
        
        return new ResponseEntity<String>("User Created!", HttpStatus.CREATED);
    }
    
    
    // Get all info of an user
    @RequestMapping(value = "user", method = RequestMethod.GET)
    public JwtUser getAuthenticatedUser(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
        return user;
    }
    
    /**
     * Delete a user
     * @param username Username to delete
     * @param token Token, only for admins
     * @return 
     */
    @RequestMapping(value = "/user/{username}", method = RequestMethod.DELETE)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteUser(@PathVariable("username") String username, @RequestHeader("${jwt.header}") String token)  {
        
        // Get the users
        User tokenUser = userService.findByUsername( jwtTokenUtil.getUsernameFromToken(token) );     // If token user is an admin user can edit other users
        User currentUser = userService.findByUsername(username);
        
        if (currentUser == null || tokenUser==null ) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        
//        if (userService.deleteUser(currentUser)){
//            return new ResponseEntity<String>( HttpStatus.OK );
//        } 

        identityUserService.deleteUser(currentUser);
        return new ResponseEntity<String>(HttpStatus.OK );
        
        
    }
    
    /**
     * Update a user. The token verifies if the user to update is the same that this to update or if is an admin who is going to update. The password is the passwordUser in entity.
     * @param username The path variable. Is the user to update
     * @param userUpdated This are the username or password that should be updated for the user. Json in the body with params. 
     * @param token In the token qe get the info of who want to do the operation, only works with the same user or an admin user.
     * @return 
     */
    @RequestMapping(value = "/user/{username}", method = RequestMethod.PUT)
    public ResponseEntity<JwtUser> updateUser(
            @PathVariable("username") String username, 
            @RequestBody UpdatedUser userUpdated, 
            @RequestHeader("${jwt.header}") String token) throws Exception  {
        
        // Get the users
        User tokenUser = userService.findByUsername( jwtTokenUtil.getUsernameFromToken(token) );     // If token user is an admin user can edit other users
        User currentUser = userService.findByUsername(username);
        
          
        if (currentUser==null || tokenUser==null) {
            System.out.println("User with username " + username + " not found");
            return new ResponseEntity<JwtUser>(HttpStatus.NOT_FOUND);
        } 
        
        // If the token user and user to change are not the same, or the token user is not admin return
        else if (currentUser.getUsername() != tokenUser.getUsername() && !userService.isAdminUser(tokenUser) ) {
            return new ResponseEntity<JwtUser>(HttpStatus.UNAUTHORIZED);
        } 
        // Check username doesn't exists
        else if (userService.usernameExists( userUpdated.getUsername() )){
            return new ResponseEntity<JwtUser>(HttpStatus.CONFLICT);
        }
        
        currentUser.setUsername(userUpdated.getUsername());
        currentUser.setUserPin(userUpdated.getPassword());
        if(userUpdated.getEmail()!=null){
            System.out.println("Email: " +   userUpdated.getEmail());
            currentUser.setEmail(userUpdated.getEmail());
        }
        
        return new ResponseEntity<JwtUser>( (JwtUser) userDetailsService.loadUserByUsername( 
                userService.updateUserWithUserPin(currentUser).getUsername()), HttpStatus.OK);
    }
}
