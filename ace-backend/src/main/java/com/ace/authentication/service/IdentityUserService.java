/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.service;

import com.ace.authentication.model.Identity;
import com.ace.authentication.model.User;
import com.ace.usersfeatures.service.ImagesService;
import com.ace.usersfeatures.service.UserItineraryService;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Class that manage the operations that are both from identity and users.
 * @author bolet
 */
@Service("identityUserService")
public class IdentityUserService {
    
    @Autowired
    UserService userService;
    
    @Autowired 
    IdentityService identityService;
    
    @Autowired
    private ImagesService imagesService;
    
    @Autowired
    private UserItineraryService userItineraryService;
    
    /**
     * Register a new user and asociate him to his identity (username). 
     * For example, new user without identities: first register this user, after that his entity and then make the relation between them.
     * @param user
     * @return
     * @throws Exception 
     */
    public Boolean registerUserIdentity (User user) {
        try {
            // 1- Create user
            User newUser = userService.registerNewUserHashingPassword(user);

            // 2- Create identities with user id
            Set identities = identityService.registerIdentity(newUser);

            // 3- Set identities to the user
            userService.setUserIdentities(newUser, identities);            
            return true;
            
        } catch  (Exception e){
            e.printStackTrace();
            return false;
        }        
    }
    
    public User getUserFromIdentity (String id) throws Exception{        
        return identityService.findByIdentity( id ).getUser();
    }
    
    /**
     * Add new identity to existing user
     * @param user
     * @return 
     */
    public boolean addIdentityToUser (User user, Identity identity) {
        try {
            // Change images and itineraries ownership
            imagesService.changeAllUserImagesOwnership(identity.getUser(), user);
            userItineraryService.changeAllUserItinerariesOwnership(identity.getUser(), user);
            
            // Add user to identity 
            identityService.changeUser(identity, user);
            
            // Add identity to the existing user
            userService.addIdentity(user, identity);
            
            // Delete old identity user
            User idUser = userService.findByUsername(identity.getIdentity());
            if (idUser != null){
                userService.deleteUser(idUser);
            }
            return true;
            
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Deleta a user and his identities
     * @param user 
     */
    public boolean deleteUser (User user){
        try {
            // 1- Delete entities from user
            identityService.deleteIdentity( user.getIdentities() );
            // 2- Delete user
            userService.deleteUser(user);
            return true;
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
