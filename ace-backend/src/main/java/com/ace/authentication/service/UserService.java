/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.service;

import com.ace.authentication.model.Authority;
import com.ace.authentication.model.AuthorityName;
import com.ace.authentication.model.Identity;
import com.ace.authentication.model.User;
import com.ace.authentication.repository.AuthorityRepository;
import com.ace.authentication.repository.IdentityRepository;
import com.ace.authentication.repository.UserRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bolet
 */
@Service("userService")
public class UserService  {
    
    private static final AuthorityName DEFAULT_USER_ROLE  = AuthorityName.ROLE_USER;
    
    @Autowired
    private UserRepository userRepository; 
    
    @Autowired
    private AuthorityRepository authorityRepository; 
    
    @Autowired
    private IdentityRepository identityRepository; 
    
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
    @Transactional
    private User saveUserHashingPassword (User user){
        user.setPassword( setCryptedPassword( user.getPassword() )  );
        return userRepository.save (user);
    }
    
    @Transactional
    private User updateUser (User user){
        return userRepository.save (user);
    }
    
    @Transactional
    public boolean deleteUser (User user){
        userRepository.delete(user);
        if ( usernameExists( user.getUsername() )){
            return false;
        }
        return true;
    }
     
    /**
     * Save a new user. Only needed the username and unencrypted password
     * @param user
     * @return
     * @throws Exception 
     */
    public User registerNewUserHashingPassword(User user) throws Exception {
        if (usernameExists(user.getUsername())) {  
            throw new Exception(
              "There is an account with that email adress: "
              +  user.getUsername());
        }
        
        return saveUserHashingPassword(initializeUser(user));
    }
    
    /**
     * Update a user. Password in plain text and stores it hashed
     * @param user
     * @return 
     */
    public User updateUserHashingPassword(User user) throws Exception {
        if (usernameExists(user.getUsername())) {  
            throw new Exception(
              "There is an account with that email adress: "
              +  user.getUsername());
        }
        return saveUserHashingPassword(user);
    }
    
    /**
     * Update a user. The password will be the passwordUser in entity. 
     * @param user
     * @return 
     */
    public User updateUserWithUserPin(User user) throws Exception {
        if (usernameExists(user.getUsername())) {  
            throw new Exception(
              "There is an account with this username: "
              +  user.getUsername());
        }
        user.setUserPin( setCryptedPassword( user.getUserPin() )  );
        return updateUser(user);
    }
    
    /**
     * Add a set of identities to a user. 
     * @param user
     * @param identities
     * @return 
     */
    public User setUserIdentities (User user, Set <Identity> identities){
        user.setIdentities(identities);
        return updateUser( user );
    }
    
    /**
     * Add a single identity to a user.
     * @param user
     * @param identity
     * @return 
     */
    public User addIdentity (User user, Identity identity){
        user.getIdentities().add(identity);
        return updateUser( user );
    }
    
    // #### Utils ####
    
    /**
     * Add parameters like Role or timestamp to new user. 
     * @param user
     * @return 
     */
    private User initializeUser(User user) {
        List<Authority> authorities = new ArrayList<Authority>();
        authorities.add(authorityRepository.findByName(DEFAULT_USER_ROLE)); // By default a user will have ROLE_USER
        user.setAuthorities(authorities); 
        user.setEnabled(true);
        user.setLastPasswordResetDate( new Date() );
        return user;
    }
    
    /**
     * Return a crypted password with bcrypt
     * @param password
     * @return 
     */
    private String setCryptedPassword (String password){
        return passwordEncoder.encode( password );
    }
    
    /**
     * Get if a user with username exists.
     * @param username
     * @return 
     */
    public boolean usernameExists (String username) {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            return true;
        }
        return false;
    }
    
    /**
     * Return a user found by username.
     * @param username
     * @return 
     */
    public User findByUsername (String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }
    
    public User addAuthority (User user, AuthorityName auth){
        user.getAuthorities().add( authorityRepository.findByName(auth) );
        return updateUser(user);
    }
       
    public boolean isAdminUser (User user){   
        return  user.getAuthorities().contains( authorityRepository.findByName(AuthorityName.ROLE_ADMIN) );
    }
    
    public User findById (Long id) {
        User user = userRepository.findById(id);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }
    
    /**
     * Check if a pin is from a user. 
     * @param user User who check
     * @param pin The pin to check
     * @return 
     */
    public boolean pinValidation (User user, String pin){
        return passwordEncoder.matches(pin, user.getUserPin() );
    }
}