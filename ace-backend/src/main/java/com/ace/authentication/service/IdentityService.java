/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.service;

import com.ace.authentication.model.Identity;
import com.ace.authentication.model.IdentityRole;
import com.ace.authentication.model.User;
import com.ace.authentication.repository.IdentityRepository;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bolet
 */
@Service("identityService")
public class IdentityService {
    
    private static final IdentityRole DEFAULT_IDENTITY = IdentityRole.CLIENT_HW;
            
    @Autowired
    private IdentityRepository identityRepository; 
    
    /**
     * Register a new identity previously created.
     * @param identity
     * @return 
     */
    @Transactional
    private Identity registerIdentity (Identity identity){
        if ( identityExists(identity.getIdentity()) )
            try {
                throw new Exception("Identity already exists!") ;
        } catch (Exception ex) {
            Logger.getLogger(IdentityService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return identityRepository.save (identity);
    }
    
    @Transactional
    private Identity updateIdentity (Identity identity){
        return identityRepository.save (identity);
    }
    
    @Transactional
    private void deleteIdentity (Identity identity){
        identityRepository.delete(identity);
    }
    
    /**
     * Delete a Set of dientities
     * @param identities 
     */
    public void deleteIdentity (Set<Identity> identities){
        for ( Identity id : identities ){
            deleteIdentity(id);
        }
    }
    
    /**
     * Register new identity for a new registred user. The username is the new identity
     * @param user The user who is the owner of the identity
     * @return 
     */
    public Set <Identity> registerIdentity (User user){
        Set identities = new HashSet <Identity> ();
        Identity id = registerIdentity ( new Identity ( user.getUsername(), user, DEFAULT_IDENTITY) );
        identities.add( identityRepository.save(id) );
        return identities;
    }
    
    /**
     * Register new identity for a new registred user. The username is the new identity. Setting the role
     * @param user  The user who is the owner of the identity
     * @param identityRole
     * @return 
     */
    public Set <Identity> registerIdentity (User user, IdentityRole identityRole){
        Set identities = new HashSet <Identity> ();
        Identity id = registerIdentity ( new Identity ( user.getUsername(), user, identityRole) );
        identities.add( identityRepository.save(id) );
        return identities;
    }
    
    /**
     * Register new identity for existing user.
     * @param user The user who is the owner of the identity
     * @return 
     */
    public Identity registerIdentity (User user, String identity) {
        return registerIdentity ( new Identity ( identity, user, DEFAULT_IDENTITY ) );

    }
    
    public Identity changeUser (Identity id, User user) {
        id.setUser(user);
        return updateIdentity( id );
    }
    
    public Identity findByIdentity (String identityName) {
        Identity identity = identityRepository.findByIdentity(identityName);
        
        if (identity == null) {
            try {
                throw new Exception("Identity not found");
            } catch (Exception ex) {
                Logger.getLogger(IdentityService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    
        return identity;
    }
    
    public Boolean identityExists (String identityName) {
        
        Identity identity = identityRepository.findByIdentity(identityName);
        
        if (identity != null) {
            return true;
        }
        return false;
    }
    
    public Boolean isBlacklisted (Identity id){
        if (id.getRole() == IdentityRole.BLACKLISTED) 
            return true;  
        
        return false;
    }
    
     public Boolean isBlacklisted (String identity){
        try {
            if (identityRepository.findByIdentity(identity).getRole() == IdentityRole.BLACKLISTED ) {
                return true;  }
        } catch (Exception ex) {
            Logger.getLogger(IdentityService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
        
    }

    
}
