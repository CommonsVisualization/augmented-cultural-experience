/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.model;

/**
 * Intermediate class used before update a user. Used to help to isolate the User class.
 * @author bolet
 */
public class UpdatedUser {
    
    private String username;
    private String password;
    private String userPin;
    private String email;

    public UpdatedUser() {
    }

    public UpdatedUser(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public UpdatedUser(String username, String password) {
        this.username = username;
        this.password = password;
    }        

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserPin() {
        return userPin;
    }

    public void setUserPin(String userPin) {
        this.userPin = userPin;
    }

    @Override
    public String toString() {
        return "UpdatedUser{" + "username=" + username + ", password=" + password + ", userPin=" + userPin + ", email=" + email + '}';
    }
}
