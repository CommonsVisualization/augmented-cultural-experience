/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This are class stores the identities stored in the DB. 
 * For example an user can have two dispositives (two identities, usually the MAC address). 
 * @author bolet
 */
@Entity
@Table(name = "a_identity")
public class Identity {
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "authority_seq")
    @SequenceGenerator(name = "authority_seq", sequenceName = "authority_seq", allocationSize = 1)
    private Long id;
    
    @Column(name = "identity", length = 50, unique = true)
    @NotNull
    @Size(min = 4, max = 50)
    private String identity;
    
    // Role of the identity: blacklisted, infrastructure, hardware client..
    @Column(name = "role", length = 50)
    @NotNull
    @Enumerated(EnumType.STRING)
    private IdentityRole role;
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    
    public User getUser() {
            return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IdentityRole getName() {
        return role;
    }

    public void setName(IdentityRole role) {
        this.role = role;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public IdentityRole getRole() {
        return role;
    }

    public void setRole(IdentityRole role) {
        this.role = role;
    }

    public Identity() {
    }

    public Identity(String identity,  User user, IdentityRole role) {
        this.identity = identity;
        this.role = role;
        this.user = user;
    }

    @Override
    public String toString() {
        return "Identity{" + "id=" + id + ", identity=" + identity + ", role=" + role + '}';
    }

    
    
    
    
    
    
    
}
