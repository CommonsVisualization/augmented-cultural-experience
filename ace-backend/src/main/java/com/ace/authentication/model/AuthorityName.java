/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.model;

/**
 *
 * @author bolet
 */
public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}