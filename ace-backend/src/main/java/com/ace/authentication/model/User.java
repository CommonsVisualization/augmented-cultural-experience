/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.model;

import com.ace.usersfeatures.model.Images;
import com.ace.usersfeatures.model.UserItinerary;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * DTO for users of the application.
 * @author bolet
 */
@Entity
@Table(name = "a_user")
@JsonSerialize
public class User  {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "user_seq")
    @SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 1)
    private Long id;

    @Column(name = "username", length = 50, unique = true)
    @NotNull
    @Size(min = 1, max = 50)
    private String username;

    @Column(name = "email", length = 50, unique = true)
    private String email;

    @Column(name = "password", length = 100)
    @Size(min = 0, max = 100)
    private String password;
    
    // This user pin is a custom password for each user, used for log in in other machines
    @Column(name = "user_pin", length = 100)
    @Size(min = 0, max = 100)
    private String userPin;

    @Column(name = "enabled")
    @NotNull
    private Boolean enabled;

    @Column(name = "last_password_reset_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPasswordResetDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "a_user_authority",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "authority_id", referencedColumnName = "id")})
    private List<Authority> authorities;
    
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Identity> identities;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<Images> images;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<UserItinerary> itineraries;
    
    @JsonIgnore
    public Set<Identity> getIdentities() {
            return this.identities;
    }

    public void setIdentities(Set<Identity> identities) {
        this.identities = identities;
    }
    
    @JsonIgnore
    public Set<Images> getImages() {
        return images;
    }

    public void setImages(Set<Images> images) {
        this.images = images;
    }
    
    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    @JsonIgnore
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    @JsonIgnore
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
    @JsonIgnore
    public List<Authority> getAuthorities() {
        return authorities;
    }
    
    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    @JsonIgnore
    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }
    
    @JsonIgnore
    public String getUserPin() {
        return userPin;
    }

    public void setUserPin(String userPin) {
        this.userPin = userPin;
    }
    
    @JsonIgnore
    public Set<UserItinerary> getItineraries() {
        return itineraries;
    }

    public void setItineraries(Set<UserItinerary> itineraries) {
        this.itineraries = itineraries;
    }
    
//    Needed for load the Spring MVC 
    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(Long id, String username, String password, Boolean enabled, Date lastPasswordResetDate, List<Authority> authorities, Set<Identity> identities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.lastPasswordResetDate = lastPasswordResetDate;
        this.authorities = authorities;
        this.identities = identities;
    }
    
    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", password=" + password + ", enabled=" + enabled + ", lastPasswordResetDate=" + lastPasswordResetDate + ", authorities=" + authorities + ", identities=" + identities + '}';
    }    
}
