/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.repository;

import com.ace.authentication.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bolet
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findById(Long id);
}
