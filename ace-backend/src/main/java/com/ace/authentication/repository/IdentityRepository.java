/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.authentication.repository;


import com.ace.authentication.model.Identity;
import com.ace.authentication.model.IdentityRole;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bolet
 */
public interface IdentityRepository  extends JpaRepository<Identity, Long>{
    Identity findByRole(IdentityRole name);
    Identity findByIdentity (String dientity);
}
