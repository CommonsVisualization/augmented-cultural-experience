package com.ace.authentication.repository;

import com.ace.authentication.model.Authority;
import com.ace.authentication.model.AuthorityName;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 *
 * @author bolet
 */
public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Authority findByName(AuthorityName name);
    
}
