/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.controller;

import com.ace.authentication.jwt.JwtTokenUtil;
import com.ace.authentication.model.User;
import com.ace.authentication.service.UserService;
import com.ace.usersfeatures.model.Images;
import com.ace.usersfeatures.service.ImagesService;
import com.ace.usersfeatures.service.UserItineraryService;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.IOUtils;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import com.ace.usersfeatures.service.TaxonService;

/**
 *
 * @author bolet
 */
@RestController
public class ImagesController {
    
    
    
    @Value("${jwt.header}")
    private String tokenHeader;
    
    @Value("${ace.uploads}")
    private String uploads;
    
    @Autowired
    ImagesService imagesService;
    @Autowired
    UserService userService;
    @Autowired
    private UserItineraryService userItineraryService;
    @Autowired
    private TaxonService taxonService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserDetailsService userDetailsService;
    
    
    // get public images json
    @RequestMapping(value = "/img", method = RequestMethod.GET)
    public ResponseEntity<?> getImages(
//            @RequestHeader("${jwt.header}") String token
    ){
        
        return  new ResponseEntity<List<Images>>(imagesService.getPublicImages(), HttpStatus.OK);      

    }   
    
    
    // get private images json
    @RequestMapping(value = "/img/private", method = RequestMethod.GET)
    public ResponseEntity<?> getImages(
            @RequestHeader("${jwt.header}") String token
    ){
        User user =  userService.findByUsername(jwtTokenUtil.getUsernameFromToken(token));
        
        return  new ResponseEntity<List<Images>>(imagesService.getPrivateImages(user), HttpStatus.OK);      
//        return  new ResponseEntity<>(HttpStatus.OK);      


    }   
    
    /**
     * Get a public image. Get a image that is marked as public
     * @param filename
     * @return 
     */
    @RequestMapping(value = "/img/{token}/public/{filename}", method = RequestMethod.GET)
    public ResponseEntity<?> getPublicImage(
            @PathVariable("filename") String filename
    ){
        HttpHeaders headers = new HttpHeaders();

        InputStream is;
        byte[] media = null;
        try {
            is = imagesService.getImageBytes("public/"+filename);
            media = IOUtils.toByteArray(is);

        } catch (IOException ex) {
            Logger.getLogger(ImagesController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);

        }


        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
        return responseEntity;


    }  
    
    /**
     * Get a private image. Get the bytes from an image that is stored as private. Checks if the token corresponds to the image user owner.
     * @param filename The name of the image
     * @param token The token of the user that is trying to acces to it
     * @param username the username of the image
     * @return 
     */
    @RequestMapping(value = "/img/{token}/private/{username}/{filename}", method = RequestMethod.GET)
    public ResponseEntity<?> getPrivateImage(
            @PathVariable("filename") String filename,
            @PathVariable("token") String token,
            @PathVariable("username") String username
    ){  
        
        String imagePath = "private/"+username+"/"+filename;
        // Check if token is valid
        // Check if is valid image path (check if the token user is the owner of the image)
        User tokenUser = userService.findByUsername(jwtTokenUtil.getUsernameFromToken(token));
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(tokenUser.getUsername());
        if (!jwtTokenUtil.validateToken(token, userDetails) || 
                !imagesService.isImageFromUser(tokenUser, imagePath)){
            return  new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        
        
        // Return the image
        HttpHeaders headers = new HttpHeaders();

        InputStream is;
        byte[] media = null;
        try {
            is = imagesService.getImageBytes(imagePath);
            media = IOUtils.toByteArray(is);

        } catch (IOException ex) {
            Logger.getLogger(ImagesController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);

        }


        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
        return responseEntity;

    } 
    
    /**
     * Upload an image. 
     * @param file Multipart containing the blob of image
     * @param image The data of the image such as title, isPublic...
     * @param token Auth token of the user
     * @return 
     */
    @RequestMapping(value = "/img", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<?> uploadImage(
            @RequestPart("file") MultipartFile file,
            @RequestPart("image") Images image,
            @RequestHeader("${jwt.header}") String token
    ){
        
        if(!file.getContentType().contains("image") && imagesService.isImage(file)){
            return  new ResponseEntity<User>(HttpStatus.NOT_ACCEPTABLE);
        } 
        
        
        User tokenUser = userService.findByUsername( jwtTokenUtil.getUsernameFromToken(token) );     // If token user is an admin user can edit other users       
        
        if (imagesService.store(file, tokenUser, image)) {            
            return  new ResponseEntity<Images>(image, HttpStatus.OK);      

        } 
        return  new ResponseEntity<String>("Can't store the image",HttpStatus.INTERNAL_SERVER_ERROR);
        
    }
    
    
    /**
     * Delete an image. 
     * @param image Image to delete
     * @param token Auth token
     * @return 
     */
    @RequestMapping(value = "/img", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteImage(
            @RequestBody  Images image,
            @RequestHeader("${jwt.header}") String token)  {

        // Check if token is valid
        // Check if is valid image (check if the token user is the owner of the image)
        User tokenUser = userService.findByUsername(jwtTokenUtil.getUsernameFromToken(token));
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(tokenUser.getUsername());
        if (!jwtTokenUtil.validateToken(token, userDetails) || 
                !imagesService.isImageFromUser(tokenUser, image)){
            return  new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        
        
        
        Images im = imagesService.getImage(image.getPath());
        imagesService.deleteImageAndFile(im);
        return  new ResponseEntity<String>("{}",HttpStatus.OK);
    }
    
    /**
     * Asociate an image to a feature. 
     * @param featureType Could be "itinerary" || "specimen"
     * @param featureId The feature Id 
     * @param image Data if the image that you want to associate to the feature
     * @param token
     * @return 
     */
    @RequestMapping(value = "/img/{featureType}/{featureId}", method = RequestMethod.POST)
    public ResponseEntity<?> asociateToFeature(
            @PathVariable("featureType") String featureType,
            @PathVariable("featureId") String featureId,
            @RequestBody  Images image,
            @RequestHeader("${jwt.header}") String token)  {
        
        if (featureType.equals("itinerary") ){            
            try{
                userItineraryService.addImageToFeature(userItineraryService.getFeatureById(Long.valueOf(featureId)), image);
                return new ResponseEntity<String>("{}",HttpStatus.OK);

            } catch (Exception e){
                return  new ResponseEntity<String>("{}",HttpStatus.NOT_FOUND);
            }
        } else if (featureType.equals("taxon")){
            try{
                taxonService.setImageToTaxon(image, taxonService.getByCode( featureId ) );
                return new ResponseEntity<String>("{}",HttpStatus.OK);
            } catch (Exception e){
                return  new ResponseEntity<String>("{}",HttpStatus.NOT_FOUND);
            }
        } else {
            return  new ResponseEntity<String>("{}",HttpStatus.NOT_MODIFIED);
        }
    }
    
    @RequestMapping(value = "/img/{featureType}/{featureId}", method = RequestMethod.GET)
    public ResponseEntity<?> getImagesFromFeature(
            @PathVariable("featureType") String featureType,
            @PathVariable("featureId") String featureId,
            @RequestHeader(value = "${jwt.header}", required = false) String token  
    ){
        
        List<Images> imageList = null;
        
        if (token != null) {
            User tokenUser = userService.findByUsername(jwtTokenUtil.getUsernameFromToken(token));
            if (featureType.equals("itinerary") ){  
                imageList = userItineraryService.getImagesFromFeature(userItineraryService.getFeatureById(Long.valueOf(featureId)), tokenUser);
            } else if (featureType.equals("taxon")){
                imageList = taxonService.getImagesFromTaxon(taxonService.getByCode( featureId ), tokenUser);
            }             
        } else {
            if (featureType.equals("itinerary") ){  
                imageList = userItineraryService.getImagesFromFeature(userItineraryService.getFeatureById(Long.valueOf(featureId)));
            } else if (featureType.equals("taxon")){
                imageList = taxonService.getImagesFromTaxon(taxonService.getByCode( featureId ));
            }    
        }
        
        if (imageList != null) return new ResponseEntity<List<Images>> (imageList, HttpStatus.OK);
        
        return  new ResponseEntity<String>("{}",HttpStatus.NOT_MODIFIED);

    }
    
    
}
