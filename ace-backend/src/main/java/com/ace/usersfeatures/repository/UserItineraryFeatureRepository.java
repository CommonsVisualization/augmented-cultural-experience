/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.repository;

import com.ace.usersfeatures.model.UserItineraryFeature;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bolet
 */
public interface UserItineraryFeatureRepository  extends JpaRepository<UserItineraryFeature, Long>{
    UserItineraryFeature findById(Long id);
}
