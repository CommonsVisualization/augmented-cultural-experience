/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.repository;

import com.ace.botserver.model.Taxon;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ciberado
 */
public interface TaxonRepository extends JpaRepository<Taxon, String>{
    Taxon findByCode(String code);
}
