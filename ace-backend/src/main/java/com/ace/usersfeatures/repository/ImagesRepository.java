/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.repository;

import com.ace.authentication.model.User;
import com.ace.botserver.model.Taxon;
import com.ace.usersfeatures.model.Images;
import com.ace.usersfeatures.model.UserItineraryFeature;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bolet
 */
public interface ImagesRepository extends JpaRepository<Images, Long>{
    
    Images findById(Long id);
    Images findByPath (String path);
    List<Images> findByUserOrderByCreationDateDesc (User user);
    List<Images> findByIsPublicOrderByCreationDateDesc(Boolean ispublic);
    
    /**
     * Get all public images from an itinerary feature where the user is not.
     * @param ispublic
     * @param feat
     * @param u
     * @return 
     */
    List<Images> findByIsPublicAndFeaturesAndUserNotOrderByCreationDateDesc(Boolean ispublic, UserItineraryFeature feat, User u);
    List<Images> findDistinctImagesByIsPublicAndTaxonsAndUserNotOrderByCreationDateDesc(Boolean ispublic, Taxon taxon, User u);
    /**
     * Get all images from user on an itinerary feature  
     * @param feat
     * @param u
     * @return 
     */
    List<Images> findByFeaturesAndUserOrderByCreationDateDesc( UserItineraryFeature feat, User u);
    List<Images> findDistinctImagesByUserAndTaxonsOrderByCreationDateDesc( User u, Taxon taxon);

//    List<Images> findByUserAndTaxonsOrderByCreationDateDesc( User u, Taxon taxon);
    
    /**
     * Get all public images from an itinerary
     * @param ispublic
     * @param feat
     * @return 
     */
    List<Images> findDistinctImagesByIsPublicAndFeaturesOrderByCreationDateDesc(Boolean ispublic, UserItineraryFeature feat);
    List<Images> findDistinctImagesByIsPublicAndTaxonsOrderByCreationDateDesc(Boolean ispublic, Taxon taxon);

}
