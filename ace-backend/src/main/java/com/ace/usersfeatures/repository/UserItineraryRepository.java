/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.repository;

import com.ace.authentication.model.User;
import com.ace.usersfeatures.model.UserItinerary;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author bolet
 */
public interface UserItineraryRepository extends JpaRepository<UserItinerary, Long>{
    UserItinerary findById(Long id);
    List<UserItinerary> findByUserOrderByCreationDateDesc (User user);
    List<UserItinerary> findByIsPublicAndUserNotOrderByCreationDateDesc(Boolean ispublic, User user);
}
