/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author bolet
 */
@Service("storageService")
public class StorageService {
    
    @Value("${ace.uploads}")
    private String uploadPath;

    private void store(MultipartFile file, String path) throws Exception {  
        File rootLocation = new File(uploadPath);
        
        File f = new File(rootLocation, path);

        if (! f.getParentFile().exists() ){
            f.getParentFile().mkdirs();        
        }


        try {
            if (file.isEmpty()) {
                throw new Exception("Failed to store empty file " + file.getOriginalFilename());
            }
            Files.copy(file.getInputStream(), f.toPath() );

        } catch (IOException e) {
            throw new Exception("Failed to store file " + file.getOriginalFilename(), e);
        }
    }
    
    public void storeImage(MultipartFile file, String filename) throws Exception {
        
        store(file, filename);
    }

    public InputStream getImage(String path)   {
        try {
            File rootLocation = new File(uploadPath);
            File f = new File(rootLocation, path);
            return (InputStream ) new FileInputStream(f);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StorageService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }
    
    public void deleteFile (String path){
        File rootLocation = new File(uploadPath);

        File file = new File(rootLocation,path);
        file.delete();
        
    }
   
    public void init() throws Exception {
        Path rootLocation = Paths.get(uploadPath);

        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new Exception("Could not initialize storage", e);
        }
    }
}
