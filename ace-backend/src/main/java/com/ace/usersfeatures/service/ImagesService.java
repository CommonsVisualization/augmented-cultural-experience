/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.service;

import com.ace.authentication.model.User;
import com.ace.usersfeatures.model.Images;
import com.ace.usersfeatures.repository.ImagesRepository;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import com.ace.usersfeatures.repository.TaxonRepository;
import javax.imageio.ImageIO;

/**
 *
 * @author bolet
 */
@Service("imagesService")
public class ImagesService {
    
    @Autowired
    StorageService storageService; 
    
    @Autowired
    ImagesRepository imagesRepository;
    
    @Autowired
    private UserItineraryService userItineraryService;
    @Autowired
    private TaxonRepository featureInfoService;
    
    @Transactional
    private Images storeOnDb (Images image){
        return imagesRepository.save (image);
    } 
    
    @Transactional
    private void deleteImage (Images image){
        imagesRepository.delete(image);
    }
    
    public void deleteImageAndFile (Images image){
        imagesRepository.delete(image);
        storageService.deleteFile(image.getPath());
    }
    
    /**
     * Store an image in database and also the file on disc. 
     * @param file The file
     * @param owner the owner of the image
     * @param image the data to database
     * @return 
     */
    public boolean store (MultipartFile file, User owner, Images image) {
        image.setCreationDate(new Date());
        image.setUser(owner);
        image.setPath(generatePathName(image));
        
        try {
            storeOnDb(image);
            storageService.storeImage(file, image.getPath());
        } catch (Exception ex) {
            Logger.getLogger(ImagesService.class.getName()).log(Level.SEVERE, null, ex);
            deleteImage(image);
            return false;
        }
        return true;
    }
    
    /**
     * Change the ownership of an image
     * @param image image object to change the ownership
     * @param owner the new owner 
     */
    public void changeImageOwner (Images image, User owner) {
        image.setUser(owner);
        storeOnDb(image);
    }
    
    /**
     * Change the ownership of all images of an user
     * @param actualOwner The owner of the image list
     * @param destOwner The destinatary of the image list 
     */
    public void changeAllUserImagesOwnership (User actualOwner, User destOwner){
        List<Images> imgList = getPrivateImages (actualOwner);
        if (imgList!=null) {
            for (Images i : imgList) {
                changeImageOwner(i, destOwner);
            }
        }       
    }
    
    /**
     * Get all images marked as public in the database
     * @return 
     */
    public List<Images> getPublicImages (){
        return imagesRepository.findByIsPublicOrderByCreationDateDesc(true);
        
    }
    
    public List<Images> getPrivateImages(User u) {
        return imagesRepository.findByUserOrderByCreationDateDesc(u);
    }
    
    public Images getImage (String path){
        return imagesRepository.findByPath(path);
    }
    
    public boolean isImageFromUser (User u, String path){
        if (imagesRepository.findByPath(path).getUser().getId() == u.getId() ) return true;
        return false;
    }
    
    public boolean isImageFromUser (User u, Images i){
        return isImageFromUser(u, i.getPath());
    }

    public InputStream getImageBytes(String path)  {
        return storageService.getImage (path);
    }
    
    
    /**
     * Generate the complet path for file. This could be public or private folders + filename
     * @param im File name info
     * @return By default, public case: public/{filename} . Private case: private/{username}/{filename}
     */
    private String generatePathName (Images im){
        if (im.getIsPublic()) {
            return "public"+File.separator+generateFileName (im);

        }
        return "private"+File.separator+ im.getUser().getUsername()+File.separator +generateFileName (im);
    }
    
    /**
     * Return the file name deppending of the image info.
     * @param im image info
     * @return By default return f-{username}-{dateTime} as file name
     */
    private String generateFileName (Images im){
        return "f-"+ im.getUser().getUsername() +"-"+String.valueOf(im.getCreationDate().getTime()) ;
    }
    
    /**
     * Check if a file is an image. Using ImageIO.read()
     * @param file
     * @return 
     */
    public static boolean isImage(File file) {
        /*
        // Solution 1
        String mimeType = new MimetypesFileTypeMap().getContentType(file);
        log.info("MimeType Type = " + mimeType);
        String type = mimeType.split("/")[0].toLowerCase();
        log.info("File Type = " + type);
        return type.equals("image");
        */
        boolean b = false;
        try {
            b = (ImageIO.read(file) != null);
        } catch (IOException e) {
            System.err.println("Image cannot be found");
        }
        return b;
    }
    
    public static boolean isImage(MultipartFile multipart) {
        try {
            File convFile = new File( multipart.getOriginalFilename());
            multipart.transferTo(convFile);
            return isImage(convFile);
        } catch (IOException | IllegalStateException ex) {
            Logger.getLogger(ImagesService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
