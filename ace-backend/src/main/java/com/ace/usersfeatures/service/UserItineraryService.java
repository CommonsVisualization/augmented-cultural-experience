/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.service;

import com.ace.authentication.model.User;
import com.ace.usersfeatures.model.Images;
import com.ace.usersfeatures.model.UserItinerary;
import com.ace.usersfeatures.model.UserItineraryFeature;
import com.ace.usersfeatures.repository.ImagesRepository;
import com.ace.usersfeatures.repository.UserItineraryFeatureRepository;
import com.ace.usersfeatures.repository.UserItineraryRepository;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author bolet
 */
@Service("itineraryService")
public class UserItineraryService {
    
    @Autowired
    UserItineraryRepository userItineraryRepository; 
    
    @Autowired
    UserItineraryFeatureRepository userItineraryFeatureRepository; 
    
    @Autowired
    ImagesRepository imagesRepository;
    
    @Transactional
    private UserItinerary storeItineraryOnDb (UserItinerary itinerary){
        return userItineraryRepository.save (itinerary);
    } 
    
    @Transactional
    private UserItineraryFeature storeFeatureOnDb (UserItineraryFeature feature){
        return userItineraryFeatureRepository.save (feature);
    } 
    
    @Transactional
    private void deleteItinerary (UserItinerary iti){
        userItineraryRepository.delete(iti);
    }
    
    @Transactional
    private void deleteFeature (UserItineraryFeature feat){
        userItineraryFeatureRepository.delete(feat);
    }
    
    /**
     * Stores User Itinerary and his features. Also connect the features with his associated images
     * @param itinerary Complet Itinerary object with Itinerary, Feature and referenced images
     * @return 
     */
    public boolean store (UserItinerary itinerary)  {
        
        // 1- Store the itinerary 
        itinerary.setCreationDate(new Date());
        UserItinerary iti = storeItineraryOnDb(itinerary);

        // 2- Store the features (the images should be previously stored)
        for (UserItineraryFeature fe :  itinerary.getFeatures() ){
            fe.setItinerary(iti);
            // Associate each image to this feature
            if(!fe.getImages().isEmpty()){
                for (Images im :  fe.getImages() ){
                    Images i = imagesRepository.findByPath(im.getPath() );
                    i.addFeature(fe);
//                    fe.addImages(i);
                }
            }
            storeFeatureOnDb(fe);
        }
        return true;
    }
    
    /**
     * Return all itineraries from a user
     * @param user
     * @return 
     */
    public List<UserItinerary> getByUser (User user)  {
        return userItineraryRepository.findByUserOrderByCreationDateDesc(user);
    }
    
    public List<UserItinerary> getByIsPublicWithoutUser (User user )  {
        return userItineraryRepository.findByIsPublicAndUserNotOrderByCreationDateDesc(Boolean.TRUE, user);
    }
    
    
    public void changeItineraryOwner (UserItinerary iti, User owner) {
        iti.setUser(owner);
        storeItineraryOnDb(iti);
    }
    
    /**
     * Change the ownership of all the itineraries of a user. Get all itineraries and change his ownership individually.
     * @param actualOwner
     * @param destOwner 
     */
    public void changeAllUserItinerariesOwnership(User actualOwner, User destOwner){
        List<UserItinerary> itineraries = getByUser(actualOwner);
        if (itineraries!=null) {
            for (UserItinerary i : itineraries) {
                changeItineraryOwner(i, destOwner);
            }
        }  
    }
    
    /**
     * Delete an itinerary and his features. Not the images associated to them.
     * @param iti
     * @return 
     */
    public boolean delete (UserItinerary iti )  {
        try {
            // 1 - Delete all features
            for (UserItineraryFeature feat : iti.getFeatures()){
                deleteFeature(feat);
            }
            // 2 - Delete itinerary
            deleteItinerary(iti);
            return true;
            } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public UserItinerary getById (Long id)  {
        return userItineraryRepository.findById(id);
    }
    
    public UserItineraryFeature getFeatureById (Long id)  {
        return userItineraryFeatureRepository.findById(id);
    }
    
    /**
     * Associate an image to a feature..
     * @param feature 
     * @param image 
     */
    public void addImageToFeature (UserItineraryFeature feature, Images image)  {
        // Get the image and the especific feature
        Images im = imagesRepository.findByPath(image.getPath());
        // Create the bidirectional relation
        im.addFeature(feature);
        feature.addImages(image);
        
        // Store it into DB, this should be suficient to create the relation on DB
        storeFeatureOnDb(feature);
    }
    

    /**
     * Get images from feature. Get all public images and the requested user private images.
     * @param feat
     * @param u
     * @return 
     */
    public List<Images> getImagesFromFeature(UserItineraryFeature feat, User u){
        
        List<Images> publicImages = imagesRepository.findByIsPublicAndFeaturesAndUserNotOrderByCreationDateDesc(Boolean.TRUE, feat, u);
        List<Images> userImages = imagesRepository.findByFeaturesAndUserOrderByCreationDateDesc(feat, u);
        userImages.addAll(publicImages);
        return userImages;

    }
    
    /**
     * Get all public images from a feature
     * @param feat
     * @return 
     */
    public List<Images> getImagesFromFeature(UserItineraryFeature feat){
        return imagesRepository.findDistinctImagesByIsPublicAndFeaturesOrderByCreationDateDesc(Boolean.TRUE, feat);
    }
    
    /**
     * Check if a user is the owner of an itinerary.
     * @param u User
     * @param i Itinerary
     * @return 
     */
    public boolean isUserOwnerOfItinerary (User u, UserItinerary i){
        if (i.getUser().getId().equals(u.getId())) return true;
        else return false;
    }
    
}
