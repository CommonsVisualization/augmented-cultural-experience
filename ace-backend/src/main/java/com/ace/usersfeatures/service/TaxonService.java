/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.service;

import com.ace.authentication.model.User;
import com.ace.botserver.model.Taxon;
import com.ace.usersfeatures.model.Images;
import com.ace.usersfeatures.repository.ImagesRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ace.usersfeatures.repository.TaxonRepository;

/**
 *
 * @author bolet
 */
@Service("specimenService")
public class TaxonService {
    
    @Autowired
    TaxonRepository taxonRepository;
    @Autowired
    ImagesRepository imagesRepository;
    
    @Transactional
    public Taxon storeTaxonOnDb (Taxon t){
        return taxonRepository.save(t);
    }
    
    public void setImageToTaxon (Images image, Taxon t){
        // Get the image and the especific Specimen
        Images im = imagesRepository.findByPath(image.getPath());
        
        // Create the bidirectional relation
        im.addTaxon(t);
        t.addImages(image);
        
        // Store it into DB, this should be suficient to create the relation on DB
        storeTaxonOnDb(t);
    }
    
    /**
     * Get all public images from taxon
     * @param taxon
     * @return 
     */
    public List<Images>  getImagesFromTaxon (Taxon taxon){
        return imagesRepository.findDistinctImagesByIsPublicAndTaxonsOrderByCreationDateDesc(Boolean.TRUE, taxon);
    }

    /**
     * Get all public images of taxon and private ones of the especific user.
     * @param taxon
     * @param u
     * @return 
     */
    public List<Images>  getImagesFromTaxon (Taxon taxon, User u){
        
        List<Images> publicImages = imagesRepository.findDistinctImagesByIsPublicAndTaxonsAndUserNotOrderByCreationDateDesc(Boolean.TRUE, taxon, u);
        List<Images> userImages = imagesRepository.findDistinctImagesByUserAndTaxonsOrderByCreationDateDesc(u,taxon);

        userImages.addAll(publicImages);
        return userImages;
        
    }

    public Taxon getByCode (String code)  {
        return taxonRepository.findByCode(code);
    }
}
