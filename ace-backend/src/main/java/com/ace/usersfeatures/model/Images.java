/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.model;

import com.ace.authentication.model.User;
import com.ace.botserver.model.Taxon;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author bolet
 */
@Entity
@Table(name = "u_image")
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonSerialize
public class Images {
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "authority_seq")
    @SequenceGenerator(name = "authority_seq", sequenceName = "authority_seq", allocationSize = 1)
    private Long id;
    
    @Column(name = "path", length = 100, unique = true)
    @NotNull
    @Size(min = 4, max = 100)
    private String path;
    
    @Column(name = "title", length = 50)
    @NotNull
    @Size(min = 0, max = 50)
    private String title;
    
    @Column(name = "description", length = 200)
    @Size(min = 0, max = 200)
    private String description;
    
    @Column(name = "tags", length = 200)
    @Size(min = 0, max = 200)
    private String tags;
    
    @Column(name = "thumbnail", length = 100)
    @Size(min = 4, max = 100)
    private String thumbnail;
    
    @Column(name = "public", length = 100)
    @NotNull
    private Boolean isPublic;
    
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    
    
    @Column(name="lat")
    private double lat;
    
    @Column(name="lon")
    private double lon;
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "u_image_u_itinerary_elems",
            joinColumns = {@JoinColumn(name = "photo_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "elem_id", referencedColumnName = "id")})
    private Set<UserItineraryFeature> features;
    
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "u_image_t_taxon",
            joinColumns = {@JoinColumn(name = "photo_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "taxon_id", referencedColumnName = "codi_esp")})
    private Set<Taxon> taxons;
    
    @JsonIgnore
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    @JsonIgnore
    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    @JsonIgnore
    public String getThumbnail() {
        return thumbnail;
    }
    
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
    
    @JsonIgnore
    public Set<Taxon> getTaxons() {
        return taxons;
    }

    public void setTaxons(Set<Taxon> taxons) {
        this.taxons = taxons;
    }
    
    @JsonIgnore
    public Set<UserItineraryFeature> getFeatures() {
        return features;
    }

    public void setFeatures(Set<UserItineraryFeature> features) {
        this.features = features;
    }
    
    public void addFeature(UserItineraryFeature feature) {
        this.features.add(feature);
    }
    
    public void addTaxon(Taxon taxon) {
        this.taxons.add(taxon);
    }
    
    public Images() {
    }

    public Images(Long id, String path, String title, String description, String tags, String thumbnail, Boolean isPublic, User user) {
        this.id = id;
        this.path = path;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.thumbnail = thumbnail;
        this.isPublic = isPublic;
        this.user = user;
    }

    public Images(Long id, String path, String title, String description, String tags, String thumbnail, Boolean isPublic, Date creationDate, double lat, double lon, User user) {
        this.id = id;
        this.path = path;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.thumbnail = thumbnail;
        this.isPublic = isPublic;
        this.creationDate = creationDate;
        this.lat = lat;
        this.lon = lon;
        this.user = user;
    }
    
    @Override
    @JsonIgnore
    public String toString() {
        return "Images{" + "id=" + id + ", path=" + path + ", title=" + title + ", description=" + description + ", tags=" + tags + ", thumbnail=" + thumbnail + ", isPublic=" + isPublic + ", creationDate=" + creationDate + '}';
    }
    
    
    
    
    
    
    
    
    
    
    
}
