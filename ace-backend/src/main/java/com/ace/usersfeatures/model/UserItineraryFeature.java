/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author bolet
 */
@Entity
@Table(name = "u_itinerary_elems")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@JsonSerialize
public class UserItineraryFeature  {
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "authority_seq")
    @SequenceGenerator(name = "authority_seq", sequenceName = "authority_seq", allocationSize = 1)
    private Long id;
    
    @Column(name = "title", length = 50)
    @NotNull
    @Size(min = 0, max = 50)
    private String title;
    
    
    @Column(name = "description", length = 200)
    @Size(min = 0, max = 200)
    private String description;
    
    @ManyToMany(mappedBy = "features", fetch = FetchType.LAZY)
    private List<Images> images;
    
    @Column(name="lat")
    private double lat;
    
    @Column(name="lon")
    private double lon;
    
    @Column(name="feature_order")
    private Integer featureOrder;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "itinerary_id")
    private UserItinerary itinerary;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public Integer getFeatureOrder() {
        return featureOrder;
    }

    public void setFeatureOrder(Integer featureOrder) {
        this.featureOrder = featureOrder;
    }
    
    @JsonIgnore
    public UserItinerary getItinerary() {
        return itinerary;
    }

    public void setItinerary(UserItinerary itinerary) {
        this.itinerary = itinerary;
    }

    public List<Images> getImages() {
        return images;
    }

    public void setImages(List<Images> images) {
        this.images = images;
    }
    
    public void addImages(Images images) {
        this.images.add(images);
    }

    public UserItineraryFeature() {
    }

    @Override
    public String toString() {
        return "UserItineraryFeatures{" + "id=" + id + ", title=" + title + ", description=" + description + ", images=" + images + ", lat=" + lat + ", lon=" + lon + ", featureOrder=" + featureOrder + ", itinerary=" + itinerary.getId() + '}';
    }
    
    /**
     * Used to remove all the relation with the images, without delete the images, when the feature is going to be deleted.
     */
    @PreRemove
    private void removeImagesFromFeatures() {
        for (Images im : images) {
            im.getFeatures().remove(this);
        }
    }
}
