/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.usersfeatures.model;

import com.ace.authentication.model.User;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class are specific for user itineraries. The global itinearies are mapped by Itinerary class.
 * @author bolet
 */    
@Entity
@Table(name = "u_itinerary")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@JsonSerialize
public class UserItinerary {
    
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "authority_seq")
    @SequenceGenerator(name = "authority_seq", sequenceName = "authority_seq", allocationSize = 1)
    private Long id;
    
    @Column(name = "name", length = 50)
    @NotNull
    @Size(min = 0, max = 50)
    private String name;
    
    @Column(name = "public", length = 100)
    @NotNull
    private Boolean isPublic;    
    
    @Column(name = "description", length = 100)
    private String description;
    
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "itinerary")
    private Set<UserItineraryFeature> features;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<UserItineraryFeature> getFeatures() {
        return features;
    }

    public void setFeatures(Set<UserItineraryFeature> features) {
        this.features = features;
    }
    
    public UserItinerary() {
    }

    @Override
    public String toString() {
        return "UserItinerary{" + "id=" + id + ", name=" + name + ", isPublic=" + isPublic + ", description=" + description + ", creationDate=" + creationDate + ", user=" + user.getUsername() + ", features=" + features + '}';
    }
    
}
