/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ace.botserver.datapublishing;

import com.ace.botserver.model.Specimen;
import com.ace.botserver.model.WikipediaPageResume;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ciberado
 */
@RestController
@RequestMapping("/features")
public class FeatureInfoCtrl {

    @Autowired
    private FeatureInfoService featureInfoService;
    
    @Autowired 
    private WikipediaService wikipediaService;
 
    @RequestMapping(value="/search", method= RequestMethod.GET)
    public List<Specimen> getFeaturesWithPattern(
            @RequestParam String pattern,
            @RequestParam double lat, @RequestParam double lon) 
    throws IOException {       
        List<Specimen> specimens = featureInfoService.getSpecimens(pattern, lat, lon);
        return specimens;       
    }

    /**
     * Search points near a especific coordenades. (For exemple on qhen clic a point on the map)
     * @param lat
     * @param lon
     * @param maxDistance
     * @return
     * @throws IOException 
     */
    @RequestMapping(value="/near", method= RequestMethod.GET)
    public List<Specimen> getFeaturesNearLatLon(
            @RequestParam double lat, @RequestParam double lon,
            @RequestParam(defaultValue="-1.0") double maxDistance) 
    throws IOException {               
        List<Specimen> specimens = featureInfoService.getSpecimens(lat, lon, maxDistance);
        return specimens;       
    }

    /**
     * Search points inside coordenades range. (for example on map zoom)
     * @param minLat
     * @param minLon
     * @param maxLat
     * @param maxLon
     * @return
     * @throws IOException 
     */
    /* TODO: reimplementar utilizando dto. */
    @RequestMapping(value="/bbox", method= RequestMethod.GET)
    public List<List<Object>> getFeaturesReferencesInBBox(
            @RequestParam double minLat, @RequestParam double minLon,
            @RequestParam double maxLat, @RequestParam double maxLon) 
    throws IOException {       
        List<List<Object>> result = new ArrayList<>();
        List<Object> references = featureInfoService.getSpecimenReferences(minLat, minLon, maxLat, maxLon);
        for (Object reference : references) {
            Object[] rowArray = (Object[]) reference;
            Double lat = (Double) rowArray[0];
            Double lon = (Double) rowArray[1];
            String port = (String) rowArray[2];
            int portCode = getPortCodeFor(port);
            List<Object> item = new ArrayList<>();
            item.add(lat);
            item.add(lon);
            item.add(portCode);
            result.add(item);
        }
        return result;
    }

    private int getPortCodeFor(String port) {
        int result = -1;
        if (port != null){ 
            port = port.toLowerCase();
            port = port.trim(); // Erase space because in the db liana is "Liana " or in bulbosa is "Bulbosa "
        }
        if ("herbàcia".equals(port) == true ) result = 1;
        else if ("bulbosa".equals(port) == true ) result = 2;
        else if ("matoll".equals(port) == true ) result = 3;
        else if ("arbust".equals(port) == true ) result = 4;
        else if ("arbust perenne".equals(port) == true ) result = 4;
        else if ("arbust caduc".equals(port) == true ) result = 5;
        else if ("arbre".equals(port) == true ) result = 6;
        else if ("arbre perenne".equals(port) == true ) result = 6;
        else if ("arbre caduc".equals(port) == true ) result = 7;
        else if ("liana".equals(port) == true ) result = 8;
        else if ("crassa".equals(port) == true ) result = 9;
        else if ("palmera".equals(port) == true ) result = 12;
        
        return result;
    }

    // NOT USED??
    @RequestMapping(value="/wikipedia", method= RequestMethod.GET)
    public WikipediaPageResume getFeaturesDescriptionFromWikipedia(
            @RequestParam String wikipediaLink) 
    throws IOException {
        WikipediaPageResume resume = wikipediaService.getResume(wikipediaLink);
        return resume;
    }

              
}
