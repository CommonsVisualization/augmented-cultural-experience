/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ace.botserver.datapublishing;

import com.ace.authentication.jwt.JwtTokenUtil;
import com.ace.authentication.model.User;
import com.ace.authentication.service.UserService;
import com.ace.botserver.model.Itinerary;
import com.ace.usersfeatures.model.UserItinerary;
import com.ace.usersfeatures.service.UserItineraryService;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ciberado
 */
@RestController
@RequestMapping("/itineraries")
public class ItineraryCtrl {

    @Autowired
    private ItineraryService itineraryService;
    
    // Service to manage the itineraries created by the users
    @Autowired
    private UserItineraryService userItineraryService;
    
    @Autowired
    UserService userService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    
    @RequestMapping("/{id}")
    public ResponseEntity<?> getItinerary(@PathVariable("id") String itineraryId,
                            @RequestParam("locale") Locale userLocale,
                            @RequestHeader(value = "${jwt.header}", required = false) String token
                            ) 
    throws IOException {   
        
        // ADAPTATION:
        // 1- Check if is a general Itinerary
        Set<Itinerary> generalItineraries = 
                itineraryService.getItineraryReferences();
        
        for (Itinerary iti : generalItineraries){
            if (iti.getCode().equals(itineraryId)){
                return new ResponseEntity<ItineraryDTO>(returnGeneralItinerary(iti, userLocale),HttpStatus.OK); 
            }
        }
        
        // 2- If is not a General Itinerary check on users itineraries
        if (token != null){
            User tokenUser = userService.findByUsername( jwtTokenUtil.getUsernameFromToken(token) );     // If token user is an admin user can edit other users       
            UserItinerary iti = userItineraryService.getById(Long.valueOf(itineraryId));

            if ( iti.getIsPublic() || iti.getUser().getId().equals(tokenUser.getId()) ){
                return new ResponseEntity<UserItinerary>(iti,HttpStatus.OK);
            }
        }
        return new ResponseEntity<String>("{}",HttpStatus.NOT_FOUND);
    }
    
    /**
     * Return the ItineraryDTO of a general Itinerary. 
     * @param itinerary
     * @param userLocale
     * @return 
     */
    private ItineraryDTO returnGeneralItinerary (Itinerary itinerary, Locale userLocale){
            ItineraryDTO itineraryDTO = new ItineraryDTO(itinerary, userLocale, true);

            return itineraryDTO;
    };
    
    /**
     * Get all General Itineraries. This itineraries are created hardcoded on db, not for the users. 
     * @param request
     * @param response
     * @param userLocale
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Set<ItineraryDTO> getItinerary(HttpServletRequest request,
                             HttpServletResponse response, 
                             Locale userLocale) 
    throws IOException {        
        userLocale = new Locale("ca", "ES");
        Set<Itinerary> itineraries = 
                itineraryService.getItineraryReferences();
        Set<ItineraryDTO> references = 
                new LinkedHashSet<ItineraryDTO>();
        for (Itinerary itinerary : itineraries) {
            references.add(new ItineraryDTO(itinerary, userLocale));
        }
        return references;        
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public ResponseEntity<?> addItinerary(
            @RequestHeader("${jwt.header}") String token,
            @RequestBody UserItinerary  itinerary            
    ){
        User tokenUser = userService.findByUsername( jwtTokenUtil.getUsernameFromToken(token) );     // If token user is an admin user can edit other users       
        
        itinerary.setUser(tokenUser);
        userItineraryService.store(itinerary);
        
        return  new ResponseEntity<String>("{}",HttpStatus.OK);
    }
    
    /**
     * Returns all itineraries of a user. If the user is not the jwtuser returns only public itineraries.
     * @param username
     * @param token
     * @return 
     */
    @RequestMapping(value = "/user/{user}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserItineraries(
            @PathVariable("user") String username,
            @RequestHeader("${jwt.header}") String token     
    ){

        User tokenUser = userService.findByUsername( jwtTokenUtil.getUsernameFromToken(token) );
        User reqUser = userService.findByUsername(username);
        
        // If requested user is token user
        if (tokenUser.getUsername().equals(username)) {
            
            List <UserItinerary> itiList = userItineraryService.getByUser (tokenUser);
            return  new ResponseEntity< List <UserItinerary> >( itiList, HttpStatus.OK);
            
        } else {
            // TODO: implement get public itineraries for a user
            return  new ResponseEntity<String>("{ 'status' : 'Not implemented yet'} ",HttpStatus.NOT_IMPLEMENTED); 
        }
        
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUserItinerary(
            @PathVariable("id") String id,
            @RequestHeader("${jwt.header}") String token     
    ){
        User tokenUser = userService.findByUsername( jwtTokenUtil.getUsernameFromToken(token) );
        UserItinerary iti = userItineraryService.getById(Long.valueOf(id));
        
        // Check if the user that want to delete the itinerary is the owner
        if (userItineraryService.isUserOwnerOfItinerary(tokenUser, iti)) {
            if (userItineraryService.delete(iti)) {
                return  new ResponseEntity<String>("{}",HttpStatus.OK); 
            } else {
                return  new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
            }
            
        } else return  new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    
    /**
     * Get public itineraries less the user that do the request.
     * @param token the user token that do the request
     * @return 
     */
    @RequestMapping(value = "/public/", method = RequestMethod.GET)
    public ResponseEntity<?> getPublicItinerariesWithoutUser(
            @RequestHeader("${jwt.header}") String token     
    ){
        User tokenUser = userService.findByUsername( jwtTokenUtil.getUsernameFromToken(token) );
        
        List <UserItinerary> itiList = userItineraryService.getByIsPublicWithoutUser (tokenUser);
         
        return  new ResponseEntity<List <UserItinerary>>(itiList,HttpStatus.OK); 
    }
}
