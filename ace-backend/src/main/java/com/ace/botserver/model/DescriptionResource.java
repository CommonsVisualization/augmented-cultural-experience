/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.botserver.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author ciberado
 */
@Embeddable
public class DescriptionResource implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "ta_desc_text")
    String description;
    @Column(name = "ta_desc_audio")
    String audio;
    @Column(name = "ta_desc_etimologia")
    private String etimologia;
    @Column(name = "ta_desc_wikipedia")
    String wikipedia;

    public DescriptionResource() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getEtimologia() {
        return etimologia;
    }

    public void setEtimologia(String etimologia) {
        this.etimologia = etimologia;
    }

    public String getWikipedia() {
        return wikipedia;
    }

    public void setWikipedia(String wikipedia) {
        this.wikipedia = wikipedia;
    }
    
}
