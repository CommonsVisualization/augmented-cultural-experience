/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace.botserver.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author ciberado
 */
@Embeddable
public class MediaResource implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Column(name = "ta_me_fitxer")
    String fitxer;
    @Column(name = "ta_me_thumb")
    String thumbnail;

    public MediaResource() {
    }

    public String getFitxer() {
        return fitxer;
    }

    public void setFitxer(String fitxer) {
        this.fitxer = fitxer;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.fitxer);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MediaResource other = (MediaResource) obj;
        if (!Objects.equals(this.thumbnail, other.thumbnail)) {
            return false;
        }
        return true;
    }

    
}
