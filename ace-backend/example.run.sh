#!/bin/bash

ACEBACKEND="target/apiace-1.2-SNAPSHOT.jar"
DBUSER="postgres"
DBPASSWORD="xxxxxx"
LOG="/var/log/ace-backend.log"
SSLPASS="xxxxxx"

/usr/bin/java -jar  $ACEBACKEND \
            --spring.datasource.username=$DBUSER \
            --spring.datasource.password=$DBPASSWORD \
            --logging.file=$LOG \
            --server.ssl.key-store-password=$SSLPASS \
            --debug;

